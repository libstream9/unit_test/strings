#include <stream9/strings/query/empty.hpp>

#include <string>
#include <utility>

#include <ranges>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;
namespace rng = std::ranges;

namespace testing {

BOOST_AUTO_TEST_SUITE(primitive_)

BOOST_AUTO_TEST_SUITE(empty_)

    template<typename S>
    inline constexpr bool is_valid = requires (S s) {
        str::empty(s);
    };

    BOOST_AUTO_TEST_CASE(valid_types_)
    {
        static_assert(is_valid<char*>);
        static_assert(is_valid<char const*>);
        static_assert(is_valid<char(&)[4]>);
        static_assert(is_valid<char const (&)[4]>);
        static_assert(is_valid<std::string&>);
        static_assert(is_valid<std::string const&>);
        static_assert(is_valid<std::string&&>);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        char arr[] = "ABC";
        char* s = arr;

        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        char arr[] = "";
        char* s = arr;

        BOOST_TEST(str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(const_pointer_)
    {
        char const arr[] = "ABC";
        char const* s = arr;

        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_const_pointer_)
    {
        char const arr[] = "";
        char const* s = arr;

        BOOST_TEST(str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s[] = "ABC";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_array_)
    {
        char s[] = "";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(const_array_)
    {
        char const s[] = "ABC";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_const_array_)
    {
        char const s[] = "";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(range_)
    {
        std::string s = "ABC";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_range_)
    {
        std::string s;

        BOOST_TEST(rng::empty(s));
        BOOST_TEST(str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(const_range_)
    {
        std::string const s = "ABC";

        BOOST_TEST(!rng::empty(s));
        BOOST_TEST(!str::empty(s));
    }

    BOOST_AUTO_TEST_CASE(empty_const_range_)
    {
        std::string const s;

        BOOST_TEST(rng::empty(s));
        BOOST_TEST(str::empty(s));
    }

BOOST_AUTO_TEST_SUITE_END() // empty_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
