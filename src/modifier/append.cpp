#include <stream9/strings/modifier/append.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(append_)

    template<typename S>
    concept can_append_ch =
        requires (S&& s) {
            str::append(s, str::str_char_t<S>());
        };

    template<typename S>
    concept can_append_str =
        requires (S&& s) {
            str::append(s, s);
        };

    template<typename S>
    concept invocable =
           can_append_ch<S>
        && can_append_str<S>;

    template<typename S>
    concept uninvocable =
           (!can_append_ch<S>)
        && (!can_append_str<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    BOOST_AUTO_TEST_CASE(char_)
    {
        std::string s1 = "foo";

        str::append(s1, 'X');

        BOOST_TEST(s1 == "fooX");
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "foo";

        str::append(s1, "bar");

        BOOST_TEST(s1 == "foobar");
    }

BOOST_AUTO_TEST_SUITE_END() // append_

} // namespace testing
