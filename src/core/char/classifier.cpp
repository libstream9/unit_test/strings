#include <stream9/strings/core/char/classifier.hpp>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(predicate_)

    BOOST_AUTO_TEST_CASE(is_classified_)
    {
        str::is_classified<char> pred;

        BOOST_TEST(pred(std::ctype_base::space, '\t'));
        BOOST_TEST(!pred(std::ctype_base::blank, '\n'));
        BOOST_TEST(!pred(std::ctype_base::alpha, '0'));
    }

    BOOST_AUTO_TEST_CASE(is_space_)
    {
        str::is_space<char> pred;

        BOOST_TEST(pred(' '));
        BOOST_TEST(!pred('Z'));
    }

    BOOST_AUTO_TEST_CASE(is_alnum_)
    {
        str::is_alnum<char> pred;

        BOOST_TEST(pred('A'));
        BOOST_TEST(!pred(' '));
    }

    BOOST_AUTO_TEST_CASE(is_alpha_)
    {
        str::is_alpha<char> pred;

        BOOST_TEST(pred('A'));
        BOOST_TEST(!pred('0'));
    }

    BOOST_AUTO_TEST_CASE(is_cntrl_)
    {
        str::is_cntrl<char> pred;

        BOOST_TEST(pred('\t'));
        BOOST_TEST(!pred('0'));
    }

    BOOST_AUTO_TEST_CASE(is_digit_)
    {
        str::is_digit<char> pred;

        BOOST_TEST(pred('5'));
        BOOST_TEST(!pred('z'));
    }

    BOOST_AUTO_TEST_CASE(is_graph_)
    {
        str::is_graph<char> pred;

        BOOST_TEST(pred('A'));
        BOOST_TEST(!pred('\b'));
    }

    BOOST_AUTO_TEST_CASE(is_lower_)
    {
        str::is_lower<char> pred;

        BOOST_TEST(pred('a'));
        BOOST_TEST(!pred('A'));
    }

    BOOST_AUTO_TEST_CASE(is_print_)
    {
        str::is_print<char> pred;

        BOOST_TEST(pred(' '));
        BOOST_TEST(!pred('\b'));
    }

    BOOST_AUTO_TEST_CASE(is_punct_)
    {
        str::is_punct<char> pred;

        BOOST_TEST(pred('.'));
        BOOST_TEST(!pred('0'));
    }

    BOOST_AUTO_TEST_CASE(is_upper_)
    {
        str::is_upper<char> pred;

        BOOST_TEST(pred('A'));
        BOOST_TEST(!pred('a'));
    }

    BOOST_AUTO_TEST_CASE(is_xdigit_)
    {
        str::is_xdigit<char> pred;

        BOOST_TEST(pred('F'));
        BOOST_TEST(!pred('G'));
    }

    BOOST_AUTO_TEST_CASE(is_blank_)
    {
        str::is_blank<char> pred;

        BOOST_TEST(pred(' '));
        BOOST_TEST(!pred('\n'));
    }

    BOOST_AUTO_TEST_CASE(is_any_of_)
    {
        str::is_any_of fn { ".,;" };

        BOOST_TEST(fn(','));
        BOOST_TEST(!fn('A'));
    }

BOOST_AUTO_TEST_SUITE_END() // predicate_

} // namespace testing
