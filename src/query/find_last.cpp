#include <stream9/strings/query/find_last.hpp>

#include "../make.hpp"

#include <algorithm>
#include <forward_list>
#include <functional>
#include <iterator>
#include <list>
#include <ranges>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

using namespace std::literals;

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(find_last_)

    BOOST_AUTO_TEST_CASE(by_character_)
    {
        // literal / array
        auto const s1 = "ABBC";
        auto const r1 = str::find_last(s1, 'B');
        BOOST_TEST(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[2]));

        auto const r1x = str::find_last(s1, 'X');
        BOOST_TEST(r1x.empty());

        // pointer
        auto* const s2 = "XYYZ";
        auto const r2 = str::find_last(s2, 'Y');
        BOOST_TEST(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[2]));

        auto const r2x = str::find_last(s2, '0');
        BOOST_TEST(r2x.empty());

        // string
        std::string s3 = "1223";
        auto const r3 = str::find_last(s3, '2');
        BOOST_TEST(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[2]));

        auto const r3x = str::find_last(s3, '5');
        BOOST_TEST(r3x.empty());

        // list (bidirectional range)
        std::list<char> const s4 = { '1', '2', '2', '3' };
        auto const r4 = str::find_last(s4, '2');
        BOOST_TEST(!r4.empty());
        BOOST_TEST((r4.begin() == rng::next(rng::begin(s4), 2)));

        auto const r4x = str::find_last(s4, 'X');
        BOOST_TEST(r4x.empty());
    }

    template<typename S1>
    void
    test_substring(S1&& s1)
    {
        // literal / array
        auto const r1 = str::find_last(s1, "bar");
        BOOST_TEST((r1.begin() == rng::next(str::begin(s1), 8)));
        BOOST_TEST((r1.end() == rng::next(str::begin(s1), 11)));

        auto const r1x = str::find_last(s1, "XXX");
        BOOST_TEST(r1x.empty());

        // pointer
        auto* const s2 = "bar";
        auto const r2 = str::find_last(s1, s2);
        BOOST_TEST((r2.begin() == rng::next(str::begin(s1), 8)));
        BOOST_TEST((r2.end() == rng::next(str::begin(s1), 11)));

        auto* const s2x = "XXX";
        auto const r2x = str::find_last(s1, s2x);
        BOOST_TEST(r2x.empty());

        // string (random access string)
        std::string const s3 = "bar";
        auto const r3 = str::find_last(s1, s3);
        BOOST_TEST((r3.begin() == rng::next(str::begin(s1), 8)));
        BOOST_TEST((r3.end() == rng::next(str::begin(s1), 11)));

        std::string const s3x = "XXX";
        auto const r3x = str::find_last(s1, s3x);
        BOOST_TEST(r3x.empty());

        // list (bidirectional)
        std::list<char> const s4 = { 'b', 'a', 'r' };
        auto const r4 = str::find_last(s1, s4);
        BOOST_TEST((r4.begin() == rng::next(str::begin(s1), 8)));
        BOOST_TEST((r4.end() == rng::next(str::begin(s1), 11)));

        std::list<char> const s4x = { 'X', 'X', 'X' };
        auto const r4x = str::find_last(s1, s4x);
        BOOST_TEST(r4x.empty());
    }

    BOOST_AUTO_TEST_CASE(from_literal_array_)
    {
        auto const s = "foo bar bar baz";

        BOOST_TEST_INFO("literal and array");
        test_substring(s);
    }

    BOOST_AUTO_TEST_CASE(from_pointer_)
    {
        auto* const s = "foo bar bar baz";

        BOOST_TEST_INFO("pointer");
        test_substring(s);
    }

    BOOST_AUTO_TEST_CASE(from_string_)
    {
        std::string const s = "foo bar bar baz";

        BOOST_TEST_INFO("std::string");
        test_substring(s);
    }

    BOOST_AUTO_TEST_CASE(from_list_)
    {
        std::list<char> const s {
            'f', 'o', 'o', ' ', 'b', 'a', 'r', ' ',
            'b', 'a', 'r', ' ', 'b', 'a', 'z'
        };

        BOOST_TEST_INFO("std::list");
        test_substring(s);
    }

BOOST_AUTO_TEST_SUITE_END() // find_last_

BOOST_AUTO_TEST_SUITE(ifind_last_)

    BOOST_AUTO_TEST_CASE(char_)
    {
        // literal / array
        char s1[] = "ABBC";
        auto const r1 = str::ifind_last(s1, 'b');
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[2]));

        // pointer
        auto* const s2 = "abbc";
        auto const r2 = str::ifind_last(s2, 'B');
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[2]));

        // string
        std::string s3 = "abbc";
        auto const r3 = str::ifind_last(s3, 'B');
        BOOST_REQUIRE(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[2]));
    }

    BOOST_AUTO_TEST_CASE(substring_)
    {
        std::string const s = "foo bar bar";

        // literal / array
        auto const r1 = str::ifind_last(s, "bar");
        BOOST_TEST(std::distance(s.begin(), r1.begin()) == 8);
        BOOST_TEST(std::distance(s.begin(), r1.end()) == 11);

        // pointer
        auto* const s2 = "bar";
        auto const r2 = str::ifind_last(s, s2);
        BOOST_TEST(std::distance(s.begin(), r2.begin()) == 8);
        BOOST_TEST(std::distance(s.begin(), r2.end()) == 11);

        // string
        std::string const s3 = "bar";
        auto const r3 = str::ifind_last(s, s3);
        BOOST_TEST(std::distance(s.begin(), r3.begin()) == 8);
        BOOST_TEST(std::distance(s.begin(), r3.end()) == 11);
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_last_

} // namespace testing
