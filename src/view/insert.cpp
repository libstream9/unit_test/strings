#include <stream9/strings/view/insert.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(insert_)

    BOOST_AUTO_TEST_CASE(character_by_iterator_)
    {
        std::string const s1 = "bar";

        auto v1 = str::views::insert(s1, s1.end(), 'X');
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "barX");

        auto v2 = str::views::insert(s1, s1.begin(), 'X');
        BOOST_TEST(v2 == "Xbar");

        auto v3 = str::views::insert(s1, s1.begin() + 2, 'X');
        BOOST_TEST(v3 == "baXr");
    }

    BOOST_AUTO_TEST_CASE(string_by_iterator_)
    {
        std::string const s1 = "bar";

        auto v1 = str::views::insert(s1, s1.end(), "foo");
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "barfoo");

        auto v2 = str::views::insert(s1, s1.begin(), "foo");
        BOOST_TEST(v2 == "foobar");

        auto v3 = str::views::insert(s1, s1.begin() + 2, "foo");
        BOOST_TEST(v3 == "bafoor");
    }

    BOOST_AUTO_TEST_CASE(character_by_index_)
    {
        std::string const s1 = "bar";

        auto v1 = str::views::insert(s1, 3, 'X');
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "barX");

        auto v2 = str::views::insert(s1, 0, 'X');
        BOOST_TEST(v2 == "Xbar");

        auto v3 = str::views::insert(s1, 2, 'X');
        BOOST_TEST(v3 == "baXr");
    }

    BOOST_AUTO_TEST_CASE(string_by_index_)
    {
        std::string const s1 = "bar";

        auto v1 = str::views::insert(s1, 3, "foo");
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "barfoo");

        auto v2 = str::views::insert(s1, 0, "foo");
        BOOST_TEST(v2 == "foobar");

        auto v3 = str::views::insert(s1, 2, "foo");
        BOOST_TEST(v3 == "bafoor");
    }

    BOOST_AUTO_TEST_CASE(empty_handling_1_)
    {
        std::string s1 = "bar";

        auto v1 = str::views::insert(s1, 3, "");
        BOOST_TEST(v1 == "bar");

        auto v2 = str::views::insert(s1, 0, "");
        BOOST_TEST(v2 == "bar");

        auto v3 = str::views::insert(s1, 2, "");
        BOOST_TEST(v3 == "bar");
    }

    BOOST_AUTO_TEST_CASE(empty_handling_2_)
    {
        std::string s1 = "";

        auto v1 = str::views::insert(s1, 0, "XYZ");
        BOOST_TEST(v1 == "XYZ");

        auto v2 = str::views::insert(s1, 0, "");
        BOOST_TEST(v2 == "");
    }

BOOST_AUTO_TEST_SUITE_END() // insert_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
