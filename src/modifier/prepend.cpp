#include <stream9/strings/prepend.hpp>

#include "../namespace.hpp"

#include <stream9/string.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;

BOOST_AUTO_TEST_SUITE(prepend_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        string s;

        str::prepend(s, 'c');
        str::prepend(s, 'b');
        str::prepend(s, 'a');

        BOOST_TEST(s == "abc");
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        string s;

        str::prepend(s, "56");
        str::prepend(s, "34");
        str::prepend(s, "12");

        BOOST_TEST(s == "123456");
    }

BOOST_AUTO_TEST_SUITE_END() // prepend_

} // namespace testing
