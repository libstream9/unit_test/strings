#include <stream9/strings/query/ends_with.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(ends_with_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::ends_with(s1, s2));

        std::string const s3 = "baR";
        BOOST_TEST(!str::ends_with(s1, s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4));

        std::string s5 = "";
        BOOST_TEST(str::ends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_pointer_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::ends_with(s1, s2.data()));
        BOOST_TEST(str::ends_with(s1.data(), s2));

        std::string const s3 = "baR";
        BOOST_TEST(!str::ends_with(s1, s3.data()));
        BOOST_TEST(!str::ends_with(s1.data(), s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4.data()));
        BOOST_TEST(!str::ends_with(s1.data(), s4));

        std::string s5 = "";
        BOOST_TEST(str::ends_with(s1, s5.data()));
        BOOST_TEST(str::ends_with(s1.data(), s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_array_)
    {
        std::string s1 = "foobar";

        char s2[] = "bar";
        BOOST_TEST(str::ends_with(s1, s2));

        char const s3[] = "baR";
        BOOST_TEST(!str::ends_with(s1, s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4));

        char s5[] = "";
        BOOST_TEST(str::ends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(array_with_range_)
    {
        char const s1[] = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::ends_with(s1, s2));

        std::string const s3 = "baR";
        BOOST_TEST(!str::ends_with(s1, s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4));

        std::string s5 = "";
        BOOST_TEST(str::ends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_pointer_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::ends_with(s1.data(), s2.data()));

        std::string const s3 = "baR";
        BOOST_TEST(!str::ends_with(s1.data(), s3.data()));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1.data(), s4.data()));

        std::string s5 = "";
        BOOST_TEST(str::ends_with(s1.data(), s5.data()));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_array_)
    {
        std::string s1 = "foobar";

        char s2[] = "bar";
        BOOST_TEST(str::ends_with(s1.data(), s2));

        char const s3[] = "baR";
        BOOST_TEST(!str::ends_with(s1.data(), s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1.data(), s4));

        char const s5[] = "";
        BOOST_TEST(str::ends_with(s1.data(), s5));
    }

    BOOST_AUTO_TEST_CASE(array_with_pointer_)
    {
        char const s1[] = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::ends_with(s1, s2.data()));

        std::string const s3 = "baR";
        BOOST_TEST(!str::ends_with(s1, s3.data()));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4.data()));

        std::string s5 = "";
        BOOST_TEST(str::ends_with(s1, s5.data()));
    }

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        char const s1[] = "foobar";

        char s2[] = "bar";
        BOOST_TEST(str::ends_with(s1, s2));

        char const s3[] = "baR";
        BOOST_TEST(!str::ends_with(s1, s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::ends_with(s1, s4));

        char const s5[] = "";
        BOOST_TEST(str::ends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_char_)
    {
        std::string s1 = "foobar";

        BOOST_TEST(!str::ends_with(s1, 'f'));
        BOOST_TEST(str::ends_with(s1, 'r'));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_char_)
    {
        std::string s1 = "foobar";

        BOOST_TEST(!str::ends_with(s1.data(), 'f'));
        BOOST_TEST(str::ends_with(s1.data(), 'r'));
    }

    BOOST_AUTO_TEST_CASE(array_with_char_)
    {
        char const s1[] = "foobar";

        BOOST_TEST(!str::ends_with(s1, 'f'));
        BOOST_TEST(str::ends_with(s1, 'r'));
    }

BOOST_AUTO_TEST_SUITE_END() // ends_with_

BOOST_AUTO_TEST_SUITE(iends_with_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "bar";
        BOOST_TEST(str::iends_with(s1, s2));

        std::string const s3 = "car";
        BOOST_TEST(!str::iends_with(s1, s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4));

        std::string s5 = "";
        BOOST_TEST(str::iends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_pointer_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "BAR";
        BOOST_TEST(str::iends_with(s1, s2.data()));
        BOOST_TEST(str::iends_with(s1.data(), s2));

        std::string const s3 = "car";
        BOOST_TEST(!str::iends_with(s1, s3.data()));
        BOOST_TEST(!str::iends_with(s1.data(), s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4.data()));
        BOOST_TEST(!str::iends_with(s1.data(), s4));

        std::string s5 = "";
        BOOST_TEST(str::iends_with(s1, s5.data()));
        BOOST_TEST(str::iends_with(s1.data(), s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_array_)
    {
        std::string s1 = "foobar";

        char s2[] = "BAR";
        BOOST_TEST(str::iends_with(s1, s2));

        char const s3[] = "car";
        BOOST_TEST(!str::iends_with(s1, s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4));

        char s5[] = "";
        BOOST_TEST(str::iends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(array_with_range_)
    {
        char const s1[] = "foobar";

        std::string const s2 = "BAR";
        BOOST_TEST(str::iends_with(s1, s2));

        std::string const s3 = "car";
        BOOST_TEST(!str::iends_with(s1, s3));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4));

        std::string s5 = "";
        BOOST_TEST(str::iends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_pointer_)
    {
        std::string s1 = "foobar";

        std::string const s2 = "BAR";
        BOOST_TEST(str::iends_with(s1.data(), s2.data()));

        std::string const s3 = "car";
        BOOST_TEST(!str::iends_with(s1.data(), s3.data()));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1.data(), s4.data()));

        std::string s5 = "";
        BOOST_TEST(str::iends_with(s1.data(), s5.data()));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_array_)
    {
        std::string s1 = "foobar";

        char s2[] = "BAR";
        BOOST_TEST(str::iends_with(s1.data(), s2));

        char const s3[] = "car";
        BOOST_TEST(!str::iends_with(s1.data(), s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1.data(), s4));

        char const s5[] = "";
        BOOST_TEST(str::iends_with(s1.data(), s5));
    }

    BOOST_AUTO_TEST_CASE(array_with_pointer_)
    {
        char const s1[] = "foobar";

        std::string const s2 = "BAR";
        BOOST_TEST(str::iends_with(s1, s2.data()));

        std::string const s3 = "car";
        BOOST_TEST(!str::iends_with(s1, s3.data()));

        std::string const s4 = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4.data()));

        std::string s5 = "";
        BOOST_TEST(str::iends_with(s1, s5.data()));
    }

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        char const s1[] = "foobar";

        char s2[] = "BAR";
        BOOST_TEST(str::iends_with(s1, s2));

        char const s3[] = "car";
        BOOST_TEST(!str::iends_with(s1, s3));

        char s4[] = "xyzzyfoobar";
        BOOST_TEST(!str::iends_with(s1, s4));

        char const s5[] = "";
        BOOST_TEST(str::iends_with(s1, s5));
    }

    BOOST_AUTO_TEST_CASE(range_with_char_)
    {
        std::string s1 = "foobar";

        BOOST_TEST(!str::iends_with(s1, 'F'));
        BOOST_TEST(str::iends_with(s1, 'R'));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_char_)
    {
        std::string s1 = "foobar";

        BOOST_TEST(!str::iends_with(s1.data(), 'F'));
        BOOST_TEST(str::iends_with(s1.data(), 'R'));
    }

    BOOST_AUTO_TEST_CASE(array_with_char_)
    {
        char const s1[] = "foobar";

        BOOST_TEST(!str::iends_with(s1, 'F'));
        BOOST_TEST(str::iends_with(s1, 'R'));
    }

    BOOST_AUTO_TEST_CASE(empty_with_char_)
    {
        std::string s1;

        BOOST_TEST(!str::iends_with(s1, 'R'));
    }

BOOST_AUTO_TEST_SUITE_END() // iends_with_

} // namespace testing


