#include <stream9/strings/regex/re2.hpp>

#include <stream9/strings/finder/regex_finder.hpp>
#include <stream9/strings/query/find_all.hpp>
#include <stream9/strings/query/find_first.hpp>

#include <functional>

#include <boost/test/unit_test.hpp>

#include <re2/re2.h>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(re2_)

    BOOST_AUTO_TEST_CASE(regex_adaptor_)
    {
        re2::RE2 re { "\\d+" };
        auto const s = "xyz2019abc";

        auto const r = str::regex_adaptor<re2::RE2>::find(s, re);

        BOOST_REQUIRE(r);
        BOOST_TEST(r == "2019");
    }

    BOOST_AUTO_TEST_CASE(regex_finder_)
    {
        re2::RE2 re { "b" };
        str::regex_finder find { std::cref(re) };

        auto const s = "foobar";

        auto const& r = find(s);

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_first_)
    {
        re2::RE2 re { "b" };

        auto const s = "foobar";

        auto const r = str::find_first(s, std::cref(re));

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_all_)
    {
        std::string s = "foo foo foo";
        re2::RE2 re { "foo" };

        auto [it, last] = str::find_all(s, std::cref(re));

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // re2_

} // namespace testing
