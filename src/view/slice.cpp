#include <stream9/strings/view/slice.hpp>

#include <stream9/strings/core/char/classifier.hpp>

#include <string>
#include <string_view>
#include <type_traits>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(slice_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s = "123456";

        auto const s1 = str::views::slice(s, 1, -1);

        BOOST_TEST(s1 == "2345");
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        std::string_view const s = "123456";

        auto const s1 = str::views::slice(s, 1, -1);

        BOOST_TEST(s1 == "2345");
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s[] = "123456";

        auto const s1 = str::views::slice(s, 1, -1);

        BOOST_TEST(s1 == "2345");
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto const s = "123456";

        auto const s1 = str::views::slice(s, 1, -1);

        BOOST_TEST(s1 == "2345");
    }

    BOOST_AUTO_TEST_CASE(positive_positive_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, 2, 2) == "");
        BOOST_TEST(str::views::slice(s, 1, 2) == "2");
        BOOST_TEST(str::views::slice(s, 2, 6) == "3456");
        BOOST_TEST(str::views::slice(s, 2, 7) == "3456");
        BOOST_TEST(str::views::slice(s, 2, 100) == "3456");
    }

    BOOST_AUTO_TEST_CASE(positive_negative_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, 0, -1) == "12345");
        BOOST_TEST(str::views::slice(s, 0, -5) == "1");
        BOOST_TEST(str::views::slice(s, 0, -6) == "");
        BOOST_TEST(str::views::slice(s, 0, -7) == "");
        BOOST_TEST(str::views::slice(s, 0, -100) == "");
        BOOST_TEST(str::views::slice(s, 2, -2) == "34");
        BOOST_TEST(str::views::slice(s, 3, -3) == "");
    }

    BOOST_AUTO_TEST_CASE(positive_no_end_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, 2) == "3456");
        BOOST_TEST(str::views::slice(s, 5) == "6");
        BOOST_TEST(str::views::slice(s, 6) == "");
        BOOST_TEST(str::views::slice(s, 100) == "");
    }

    BOOST_AUTO_TEST_CASE(negative_positive_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, -5, 3) == "23");
        BOOST_TEST(str::views::slice(s, -5, 2) == "2");
        BOOST_TEST(str::views::slice(s, -5, 1) == "");
        BOOST_TEST(str::views::slice(s, -5, 0) == "");
        BOOST_TEST(str::views::slice(s, -2, 6) == "56");
        BOOST_TEST(str::views::slice(s, -1, 6) == "6");
        BOOST_TEST(str::views::slice(s, -1, 5) == "");
    }

    BOOST_AUTO_TEST_CASE(negative_negative_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, -6, -1) == "12345");
        BOOST_TEST(str::views::slice(s, -6, -2) == "1234");
        BOOST_TEST(str::views::slice(s, -6, -3) == "123");
        BOOST_TEST(str::views::slice(s, -6, -4) == "12");
        BOOST_TEST(str::views::slice(s, -6, -5) == "1");
        BOOST_TEST(str::views::slice(s, -6, -6) == "");
        BOOST_TEST(str::views::slice(s, -6, -7) == "");
        BOOST_TEST(str::views::slice(s, -6, -100) == "");
    }

    BOOST_AUTO_TEST_CASE(negative_no_end_)
    {
        auto const s = "123456";

        BOOST_TEST(str::views::slice(s, -1) == "6");
        BOOST_TEST(str::views::slice(s, -2) == "56");
        BOOST_TEST(str::views::slice(s, -3) == "456");
        BOOST_TEST(str::views::slice(s, -4) == "3456");
        BOOST_TEST(str::views::slice(s, -5) == "23456");
        BOOST_TEST(str::views::slice(s, -6) == "123456");
        BOOST_TEST(str::views::slice(s, -7) == "123456");
        BOOST_TEST(str::views::slice(s, -100) == "123456");
    }

BOOST_AUTO_TEST_SUITE_END() // slice_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
