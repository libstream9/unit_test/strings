#include <stream9/strings/utility/sized_cstring_view.hpp>

#include "../namespace.hpp"

#include <ranges>
#include <string>
#include <sstream>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/core/concepts.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(sized_cstring_view_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::regular<str::sized_cstring_view>);

        static_assert(rng::contiguous_range<str::sized_cstring_view>);
        static_assert(rng::borrowed_range<str::sized_cstring_view>);
        static_assert(rng::sized_range<str::sized_cstring_view>);
        static_assert(rng::common_range<str::sized_cstring_view>);
        static_assert(rng::view<str::sized_cstring_view>);

        static_assert(str::contiguous_string<str::sized_cstring_view>);

        using const_iterator = str::sized_cstring_view::const_iterator;
        static_assert(std::contiguous_iterator<const_iterator>);
        static_assert(std::sized_sentinel_for<const_iterator, const_iterator>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        str::sized_cstring_view v;
    }

    BOOST_AUTO_TEST_CASE(contruct_from_pointer_)
    {
        char* const s = const_cast<char*>("foo");
        str::sized_cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(contruct_from_const_pointer_)
    {
        char const* s = "foo";
        str::sized_cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_literal_)
    {
        str::sized_cstring_view v { "foo" };
    }

    BOOST_AUTO_TEST_CASE(construct_from_array_)
    {
        char s[] = "foo";
        str::sized_cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_const_array_)
    {
        char const s[] = "foo";
        str::sized_cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_cstring_view_)
    {
        str::cstring_view v1 { "foo" };
        str::sized_cstring_view v2 { v1 };

        BOOST_TEST(v1 == v2);
        static_assert(!std::convertible_to<str::cstring_view, str::sized_cstring_view>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_pointer_and_size_)
    {
        char const s[] = "12345";
        auto const len = sizeof(s) - 1;
        str::sized_cstring_view v { s, len };
    }

    BOOST_AUTO_TEST_CASE(construct_from_range_)
    {
        auto const s = "12345";
        str::sized_cstring_view v { s, s + 5 };
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_view_)
    {
        std::string_view sv = "foo";
        str::sized_cstring_view v { sv };

        BOOST_TEST(v == sv);
        static_assert(!std::convertible_to<std::string_view, str::sized_cstring_view>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_)
    {
        std::string const s = "12345";
        str::sized_cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(begin_)
    {
        str::sized_cstring_view v { "123" };

        auto it = v.begin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(cbegin_)
    {
        str::sized_cstring_view v { "123" };

        auto it = v.cbegin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        str::sized_cstring_view v { "123" };

        auto it = v.end();
        BOOST_TEST(*it == '\0');
    }

    BOOST_AUTO_TEST_CASE(cend_)
    {
        str::sized_cstring_view v { "123" };

        auto it = v.cend();
        BOOST_TEST(*it == '\0');
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(v[1] == '2');
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(v.at(1) == '2');

        BOOST_CHECK_EXCEPTION(
            v.at(3),
            err::error,
            [&](auto&& e) {
                using enum str::errc;
                BOOST_TEST((e.why() == index_is_out_of_range));

                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(v.front() == '1');
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(v.back() == '3');
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        char const s[] = "123";
        str::sized_cstring_view v { s };

        BOOST_TEST(v.data() == s);
    }

    BOOST_AUTO_TEST_CASE(c_str_)
    {
        char const s[] = "123";
        str::sized_cstring_view v { s };

        BOOST_TEST(v.c_str() == s);
    }

    BOOST_AUTO_TEST_CASE(size_default_)
    {
        str::sized_cstring_view v;

        BOOST_TEST(v.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(v.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_default_)
    {
        str::sized_cstring_view v;

        BOOST_TEST(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_empty_)
    {
        str::sized_cstring_view v { "" };

        BOOST_TEST(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        str::sized_cstring_view v { "123" };

        BOOST_TEST(!v.empty());
    }

    BOOST_AUTO_TEST_CASE(remove_prefix_)
    {
        auto const s = "12345";
        str::sized_cstring_view v { s };

        v.remove_prefix(2);

        BOOST_TEST(v.data() == &s[2]);
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        str::sized_cstring_view v1 { "123" };
        str::sized_cstring_view v2 { "123" };

        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_cstring_view_)
    {
        str::sized_cstring_view v1 { "123" };
        str::cstring_view v2 { "123" };

        BOOST_TEST(v1 == v2);
        BOOST_TEST(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_pointer_)
    {
        str::sized_cstring_view v1 { "123" };
        auto s2 = "123";

        BOOST_TEST(v1.operator==(s2));
        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_array_)
    {
        str::sized_cstring_view v1 { "123" };
        char s2[] = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_literal_)
    {
        str::sized_cstring_view v1 { "123" };

        BOOST_CHECK(v1 == "123");
        BOOST_CHECK("123" == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_)
    {
        str::sized_cstring_view v1 { "123" };
        std::string s2 = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_view_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string s2 { "12345" };
        auto v2 = std::string_view(s2).substr(0, 3);

        BOOST_CHECK(v1 == v2);
        BOOST_CHECK(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        str::sized_cstring_view v1 { "123" };
        str::sized_cstring_view v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_)
    {
        str::sized_cstring_view v1 { "123" };
        str::sized_cstring_view v2;

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::greater);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v2 == std::strong_ordering::equivalent);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_)
    {
        str::sized_cstring_view v1 { "123" };

        auto const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_to_empty_)
    {
        str::sized_cstring_view v1 { "123" };

        auto const s2 = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_literal_)
    {
        str::sized_cstring_view v1 { "123" };

        BOOST_CHECK(v1 <=> "124" == std::strong_ordering::less);
        BOOST_CHECK("124" <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_literal_)
    {
        str::sized_cstring_view v1 { "123" };

        BOOST_CHECK(v1 <=> "" == std::strong_ordering::greater);
        BOOST_CHECK("" <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_array_)
    {
        str::sized_cstring_view v1 { "123" };

        char s2[] = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_array_)
    {
        str::sized_cstring_view v1 { "123" };

        char s2[] = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_std_string_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_string_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string const s2;

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_string_view_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string_view v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_string_view_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string_view v2;

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::greater);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_)
    {
        str::sized_cstring_view v1 { "123" };

        char const* v2 = v1;

        BOOST_CHECK(v1 == v2);
        static_assert(std::convertible_to<str::sized_cstring_view, char const*>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_cstring_view_)
    {
        str::sized_cstring_view v1 { "123" };

        str::cstring_view v2 = v1;

        BOOST_CHECK(v1 == v2);
        static_assert(std::convertible_to<str::sized_cstring_view, std::string_view>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string_view v2 = v1;

        BOOST_CHECK(v1 == v2);
        static_assert(std::convertible_to<str::sized_cstring_view, std::string_view>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_1_)
    {
        str::sized_cstring_view v1 { "123" };

        std::string s2 { v1 };

        BOOST_CHECK(v1 == s2);
        static_assert(!std::convertible_to<str::sized_cstring_view, std::string>);
    }

    BOOST_AUTO_TEST_CASE(stream_)
    {
        str::sized_cstring_view v1 { "123" };
        std::ostringstream oss;

        oss << v1;

        BOOST_TEST(oss.str() == "123");
    }

    BOOST_AUTO_TEST_CASE(literal_)
    {
        using namespace str::literals;

        auto v = "123"_csv;

        BOOST_TEST(v == "123");
    }

    BOOST_AUTO_TEST_CASE(constexpr_)
    {
        constexpr str::sized_cstring_view v { "foo" };
    }

BOOST_AUTO_TEST_SUITE_END() // sized_cstring_view_

} // namespace testing
