#include <stream9/strings/utility/cstring.hpp>

#include "../namespace.hpp"

#include <filesystem>
#include <ranges>
#include <sstream>
#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/core/concepts.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(cstring_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::regular<str::cstring>);

        static_assert(rng::contiguous_range<str::cstring>);
        static_assert(!rng::sized_range<str::cstring>);
        static_assert(!rng::common_range<str::cstring>);

        static_assert(str::contiguous_string<str::cstring>);

        using const_iterator = str::cstring::const_iterator;
        static_assert(std::contiguous_iterator<const_iterator>);
        static_assert(std::sentinel_for<std::default_sentinel_t, const_iterator>);
        static_assert(!std::sized_sentinel_for<std::default_sentinel_t, const_iterator>);
        static_assert(std::sized_sentinel_for<const_iterator, const_iterator>);
    }

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        str::cstring v;
    }

    BOOST_AUTO_TEST_CASE(construct_from_literal_)
    {
        str::cstring v { "foo" };
    }

    BOOST_AUTO_TEST_CASE(construct_from_array_)
    {
        char s[] = "foo";
        str::cstring v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_const_array_)
    {
        char const s[] = "foo";
        str::cstring v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_pointer_)
    {
        char* s = const_cast<char*>("foo");
        str::cstring v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_const_pointer_)
    {
        char const* s = "foo";
        str::cstring v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_)
    {
        std::string s = "foo";
        str::cstring v { s };

        BOOST_TEST(v == s);
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_view_)
    {
        std::string_view v = "12345";
        str::cstring s { v };
    }

    BOOST_AUTO_TEST_CASE(construct_from_std_path_)
    {
        std::filesystem::path p = "/tmp";
        str::cstring v { p };
    }

    BOOST_AUTO_TEST_CASE(begin_)
    {
        str::cstring v { "123" };

        auto it = v.begin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(cbegin_)
    {
        str::cstring v { "123" };

        auto it = v.cbegin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        str::cstring v { "123" };

        static_assert(std::same_as<decltype(v.end()), std::default_sentinel_t>);
    }

    BOOST_AUTO_TEST_CASE(cend_)
    {
        str::cstring v { "123" };

        static_assert(std::same_as<decltype(v.cend()), std::default_sentinel_t>);
    }

    BOOST_AUTO_TEST_CASE(bracket_)
    {
        str::cstring v { "123" };

        BOOST_TEST(v[1] == '2');
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        str::cstring v { "123" };

        BOOST_TEST(v.front() == '1');
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        char const s[] = "123";
        str::cstring v { s };

        BOOST_TEST((v.data() != s));
    }

    BOOST_AUTO_TEST_CASE(c_str_)
    {
        char const s[] = "123";
        str::cstring v { s };

        BOOST_TEST((v.c_str() != s));
    }

    BOOST_AUTO_TEST_CASE(size_default_)
    {
        str::cstring v;

        BOOST_TEST(v.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(size_empty_)
    {
        str::cstring v { "" };

        BOOST_TEST(v.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        str::cstring v { "123" };

        BOOST_TEST(v.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_default_)
    {
        str::cstring v;

        BOOST_TEST(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_empty_)
    {
        str::cstring v { "" };

        BOOST_TEST(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        str::cstring v { "123" };

        BOOST_TEST(!v.empty());
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        str::cstring v1 { "123" };
        str::cstring v2 { "123" };

        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_literal_)
    {
        str::cstring v1 { "123" };

        BOOST_CHECK(v1 == "123");
        BOOST_CHECK("123" == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_array_)
    {
        str::cstring v1 { "123" };
        char s2[] = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_pointer_)
    {
        str::cstring v1 { "123" };
        auto s2 = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_view_)
    {
        str::cstring v1 { "123" };

        std::string s2 { "12345" };
        auto v2 = std::string_view(s2).substr(0, 3);

        BOOST_CHECK(v1 == v2);
        BOOST_CHECK(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_)
    {
        str::cstring v1 { "123" };
        std::string s2 = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        str::cstring v1 { "123" };
        str::cstring v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_)
    {
        str::cstring v1 { "123" };
        str::cstring v2;

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::greater);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v2 == std::strong_ordering::equivalent);
    }

    BOOST_AUTO_TEST_CASE(compare_with_literal_)
    {
        str::cstring v1 { "123" };

        BOOST_CHECK(v1 <=> "124" == std::strong_ordering::less);
        BOOST_CHECK("124" <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_literal_)
    {
        str::cstring v1 { "123" };

        BOOST_CHECK(v1 <=> "" == std::strong_ordering::greater);
        BOOST_CHECK("" <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_array_)
    {
        str::cstring v1 { "123" };

        char s2[] = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_array_)
    {
        str::cstring v1 { "123" };

        char s2[] = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_)
    {
        str::cstring v1 { "123" };

        auto const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_to_empty_)
    {
        str::cstring v1 { "123" };

        auto const s2 = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_string_view_)
    {
        str::cstring v1 { "123" };

        std::string_view v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_string_)
    {
        str::cstring v1 { "123" };

        std::string const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_string_)
    {
        str::cstring v1 { "123" };

        std::string const s2;

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

BOOST_AUTO_TEST_SUITE_END() // cstring_

} // namespace testing
