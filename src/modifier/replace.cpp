#include <stream9/strings/modifier/replace.hpp>

#include "../hack.hpp"
#include "../make.hpp"

#include <string>
#include <vector>
#include <tuple>

#include <boost/test/unit_test.hpp>

namespace testing {

//TODO non member

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(primitive_)

BOOST_AUTO_TEST_SUITE(replace_)

    template<typename S>
    concept can_replace_with_iter_range =
        requires (S&& s1, S&& s2) {
            str::replace(s1, str::begin(s1), str::end(s1), s2);
        };

    template<typename S>
    concept can_replace_with_index_range =
        requires (S&& s1, S&& s2) {
            str::replace(s1, 0, 1, s2);
        };

    template<typename S>
    concept invocable =
           can_replace_with_iter_range<S>
        && can_replace_with_index_range<S>;

    template<typename S>
    concept uninvocable =
           (!can_replace_with_iter_range<S>)
        && (!can_replace_with_index_range<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    using output_types = std::tuple<std::string, std::vector<char>>;

    BOOST_AUTO_TEST_CASE_TEMPLATE(by_index, StringT, output_types)
    {
        auto s = make<StringT>("112233");

        // std::string
        std::string s1 = "44";
        str::replace(s, 2, 2u, s1);
        BOOST_TEST(s == "114433");

        // std::string_view
        std::string_view s2 = "BB";
        str::replace(s, 2, 2u, s2);
        BOOST_TEST(s == "11BB33");

        // std::string_view
        auto const s3 = make<std::vector<char>>("CC");
        str::replace(s, 2, 2u, s3);
        BOOST_TEST(s == "11CC33");

        // literal
        str::replace(s, 2, 2u, "DD");
        BOOST_TEST(s == "11DD33");

        // array
        char s4[] = "EE";
        str::replace(s, 2, 2u, s4);
        BOOST_TEST(s == "11EE33");

        // pointer
        char const* s5 = "FF";
        str::replace(s, 2, 2u, s5);
        BOOST_TEST(s == "11FF33");
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(by_iterator, StringT, output_types)
    {
        auto s = make<StringT>("112233");

        // std::string
        std::string s1 = "44";
        auto const it = str::replace(s, begin(s) + 2, begin(s) + 4, s1);
        BOOST_TEST(s == "114433");
        BOOST_TEST((&*it == &s[2]));

        // std::string_view
        std::string_view s2 = "BB";
        auto const it2 = str::replace(s, begin(s) + 2, begin(s) + 4, s2);
        BOOST_TEST(s == "11BB33");
        BOOST_TEST((&*it2 == &s[2]));

        // std::vector
        auto const s3 = make<std::vector<char>>("CC");
        auto const it3 = str::replace(s, begin(s) + 2, begin(s) + 4, s3);
        BOOST_TEST(s == "11CC33");
        BOOST_TEST((&*it3 == &s[2]));

        // literal / array
        auto const it4 = str::replace(s, begin(s) + 2, begin(s) + 4, "DD");
        BOOST_TEST(s == "11DD33");
        BOOST_TEST((&*it4 == &s[2]));

        // pointer
        char const* s5 = "FF";
        auto const it5 = str::replace(s, begin(s) + 2, begin(s) + 4, s5);
        BOOST_TEST(s == "11FF33");
        BOOST_TEST((&*it5 == &s[2]));
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        std::string s = "";

        str::replace(s, 0, 0, "44");

        BOOST_TEST(s == "44");
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        std::string s = "112233";

        str::replace(s, 2, 0, "44");

        BOOST_TEST(s == "11442233");
    }

    BOOST_AUTO_TEST_CASE(empty_3_)
    {
        std::string s = "112233";

        str::replace(s, 2, 2, "");

        BOOST_TEST(s == "1133");
    }

BOOST_AUTO_TEST_SUITE_END() // replace_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
