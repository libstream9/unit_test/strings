#include <stream9/strings/core/char/comparator.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(comparator_)

    BOOST_AUTO_TEST_CASE(equal_to_)
    {
        str::equal_to<char> pred;
        static_assert(str::char_comparator<decltype(pred), char>);

        BOOST_TEST(pred('a', 'a'));
        BOOST_TEST(!pred('a', 'A'));
    }

    BOOST_AUTO_TEST_CASE(iequal_to_)
    {
        str::iequal_to<char> pred;
        static_assert(str::char_comparator<decltype(pred), char>);

        BOOST_TEST(pred('a', 'a'));
        BOOST_TEST(pred('a', 'A'));
        BOOST_TEST(!pred('a', 'b'));
        BOOST_TEST(!pred('a', 'B'));
    }

    BOOST_AUTO_TEST_CASE(less_)
    {
        str::less<char> pred;
        static_assert(str::char_comparator<decltype(pred), char>);

        BOOST_TEST(pred('a', 'b'));
        BOOST_TEST(pred('A', 'b'));
        BOOST_TEST(!pred('a', 'a'));
        BOOST_TEST(!pred('b', 'a'));
        BOOST_TEST(!pred('a', 'B'));
    }

    BOOST_AUTO_TEST_CASE(iless_)
    {
        str::iless<char> pred;
        static_assert(str::char_comparator<decltype(pred), char>);

        BOOST_TEST(pred('a', 'b'));
        BOOST_TEST(!pred('A', 'a'));
        BOOST_TEST(!pred('a', 'A'));
        BOOST_TEST(!pred('b', 'a'));
        BOOST_TEST(pred('a', 'B'));
    }

BOOST_AUTO_TEST_SUITE_END() // comparator_

} // namespace testing
