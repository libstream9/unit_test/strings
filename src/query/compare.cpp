#include <stream9/strings/query/compare.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(compare_)

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        auto const s1 = "foo";
        auto const s2 = "bar";

        BOOST_TEST(str::compare(s1, s2) > 0);
        BOOST_TEST(str::compare(s2, s1) < 0);
        BOOST_TEST(str::compare(s1, s1) == 0);

        auto const s3 = "fo";
        BOOST_TEST(str::compare(s1, s3) > 0);
        BOOST_TEST(str::compare(s3, s1) < 0);
    }

    BOOST_AUTO_TEST_CASE(string_with_string_)
    {
        std::string const s1 = "foo";
        std::string const s2 = "bar";

        BOOST_TEST(str::compare(s1, s2) > 0);
        BOOST_TEST(str::compare(s2, s1) < 0);
        BOOST_TEST(str::compare(s1, s1) == 0);

        std::string const s3 = "fo";
        BOOST_TEST(str::compare(s1, s3) > 0);
        BOOST_TEST(str::compare(s3, s1) < 0);
    }

BOOST_AUTO_TEST_SUITE_END() // compare_

} // namespace testing
