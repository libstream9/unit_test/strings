#include <stream9/strings/converter/to_string.hpp>

#include <chrono>
#include <concepts>
#include <ctime>
#include <filesystem>

#include <stream9/strings/stream.hpp>
#include <stream9/strings/view/insert.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace st9 = stream9;
namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(to_string_)

    BOOST_AUTO_TEST_CASE(from_bool_)
    {
        std::string s;

        str::to_string(true, s);
        BOOST_TEST(s == "true");

        str::to_string(false, s);
        BOOST_TEST(s == "false");

        volatile bool v = true;
        str::to_string(v, s);
        BOOST_TEST(s == "true");
    }

    BOOST_AUTO_TEST_CASE(from_integer_1)
    {
        std::string s;

        str::to_string(1000, s);
        BOOST_TEST(s == "1000");

        str::to_string(1000, s, 16);
        BOOST_TEST(s == "3e8");

        str::to_string(1000, s, 0, std::locale("en_US.UTF-8"));
        BOOST_TEST(s == "1,000");
    }

    BOOST_AUTO_TEST_CASE(from_integer_2)
    {
        auto s1 = str::to_string(1000);
        BOOST_TEST(s1 == "1000");

        auto s2 = str::to_string(1000, 16);
        BOOST_TEST(s2 == "3e8");

        auto s3 = str::to_string(1000, 0, std::locale("en_US.UTF-8"));
        BOOST_TEST(s3 == "1,000");
    }

    BOOST_AUTO_TEST_CASE(from_float_)
    {
        std::string s;

        str::to_string(1000.1, s);
        BOOST_TEST(s == "1000.1");

        using fmt = str::float_format;

        str::to_string(1000.1, s, fmt::scientific);
        BOOST_TEST(s == "1.000100e+03");

        str::to_string(1000.1, s, fmt::fixed);
        BOOST_TEST(s == "1000.100000");

        str::to_string(1000.1, s, fmt::general, std::locale("en_US.UTF-8"));
        BOOST_TEST(s == "1,000.1");
    }

    BOOST_AUTO_TEST_CASE(from_streamable_object_)
    {
        std::filesystem::path p { "/tmp" };

        auto const& s = str::to_string(p);
        static_assert(std::same_as<decltype(s), st9::string const&>);

        BOOST_TEST(s == "\"/tmp\"");
    }

    BOOST_AUTO_TEST_CASE(from_time_point_)
    {
        auto const ts = "2021-01-27 20:25:52";
        struct tm tm {};
        strptime(ts, "%F %T", &tm);
        auto const t = timelocal(&tm);

        auto const tp = std::chrono::system_clock::from_time_t(t);

        auto const& s = str::to_string(tp);

        BOOST_TEST(s == ts);
    }

    BOOST_AUTO_TEST_CASE(from_sys_time_)
    {
        auto const ts = "2021-01-27 20:25:52";
        struct tm tm {};
        strptime(ts, "%F %T", &tm);
        auto const t = timelocal(&tm);

        auto const tp = std::chrono::system_clock::from_time_t(t);

        auto const& s = str::to_string(tp);

        BOOST_TEST(s == ts);
    }

    BOOST_AUTO_TEST_CASE(from_file_time_)
    {
        auto const ts = "2021-01-27 20:25:52";
        struct tm tm {};
        strptime(ts, "%F %T", &tm);
        auto const t = timelocal(&tm);

        auto const tp = std::chrono::system_clock::from_time_t(t);
        auto const ftp = std::chrono::file_clock::from_sys(tp);

        auto const& s = str::to_string(ftp);

        BOOST_TEST(s == ts);
    }

    BOOST_AUTO_TEST_CASE(from_duration_)
    {
        namespace chrono = std::chrono;
        using namespace std::literals::chrono_literals;

        BOOST_TEST(str::to_string(123ns) == "123ns");
        BOOST_TEST(str::to_string(123us) == "123us");
        BOOST_TEST(str::to_string(123ms) == "123ms");
        BOOST_TEST(str::to_string(123s) == "123s");
        BOOST_TEST(str::to_string(123min) == "123min");
        BOOST_TEST(str::to_string(123h) == "123hour");
        BOOST_TEST(str::to_string(chrono::days(123)) == "123day");
        BOOST_TEST(str::to_string(chrono::weeks(123)) == "123week");
        BOOST_TEST(str::to_string(chrono::months(123)) == "123month");
        BOOST_TEST(str::to_string(chrono::years(123)) == "123year");
    }

    BOOST_AUTO_TEST_CASE(from_pointer_)
    {
        void* p = (void*)0x100;
        auto s = str::to_string(p);

        BOOST_TEST(s == "0x100");
    }

BOOST_AUTO_TEST_SUITE_END() // to_string_

} // namespace testing
