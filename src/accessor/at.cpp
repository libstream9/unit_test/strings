#include <stream9/strings/accessor/at.hpp>

#include <stream9/strings/core/string_range.hpp>

#include <string>
#include <list>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(at_)

    template<typename T>
    inline constexpr bool is_valid = requires (T s) {
        { str::at(s, 0) } -> std::same_as<str::reference_t<T>>;
    };

    BOOST_AUTO_TEST_CASE(type_)
    {
        static_assert(is_valid<char*>);
        static_assert(is_valid<char const*>);
        static_assert(is_valid<char(&)[4]>);
        static_assert(is_valid<char const(&)[4]>);
        static_assert(is_valid<std::string>);
        static_assert(is_valid<std::string const>);
        static_assert(is_valid<str::string_range<char*>>);
        static_assert(is_valid<str::string_range<char const*>>);
        static_assert(!is_valid<std::list<char>>);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string s1 = "123";

        BOOST_TEST(str::at(s1.data(), 1) == '2');
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = "123";

        BOOST_TEST(str::at(s1, 1) == '2');
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "123";

        BOOST_TEST(str::at(s1, 1) == '2');
    }

    BOOST_AUTO_TEST_CASE(string_range_)
    {
        str::string_range s1 = "123";

        BOOST_TEST(str::at(s1, 1) == '2');
    }

BOOST_AUTO_TEST_SUITE_END() // at_

} // namespace testing
