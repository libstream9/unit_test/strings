#include <stream9/strings/stream.hpp>

#include <locale>
#include <sstream>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

static boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(istream_)

    BOOST_AUTO_TEST_CASE(array_literal_)
    {
        auto const s1 = "100,000";
        str::istream is { s1 };

        int i;
        is >> i;

        BOOST_TEST(i == 100);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto* const s1 = "100,000";
        str::istream is { s1 };

        int i;
        is.imbue(std::locale("en_US.UTF-8"));
        is >> i;

        BOOST_TEST(i == 100000);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s1 = "foobar";
        str::istream is { s1 };

        std::string s2;
        is >> s2;

        BOOST_TEST(s2 == s1);
    }

    BOOST_AUTO_TEST_CASE(move_construct_)
    {
        str::istream is1 { "foo bar" };
        std::string s;

        is1 >> s;
        BOOST_TEST(s == "foo");

        str::istream is2 { std::move(is1) };

        is2 >> s;
        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(move_assignment_)
    {
        str::istream is1 { "foo bar" };
        std::string s;

        is1 >> s;
        BOOST_TEST(s == "foo");

        str::istream is2 { "ABC DEF" };
        is2 = std::move(is1);

        is2 >> s;
        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(stream_in_operator_)
    {
        using str::operator>>;

        auto const s1 = "hello 1 2.5";

        std::string s2;
        int i1;
        double d1;

        s1 >> s2 >> i1 >> d1;

        BOOST_TEST(s2 == "hello");
        BOOST_TEST(i1 == 1);
        BOOST_TEST(d1 == 2.5);
    }

BOOST_AUTO_TEST_SUITE_END() // istream_

BOOST_AUTO_TEST_SUITE(ostream_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "hello";
        str::ostream os { s1 };

        os << " world";
        BOOST_TEST(s1 == "hello world");
    }

    BOOST_AUTO_TEST_CASE(vector_)
    {
        std::vector<char> s1 { 'h', 'e', 'l', 'l', 'o' };
        str::ostream os { s1 };

        os << " world";

        auto const expected = {
            'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'
        };
        BOOST_TEST(s1 == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(move_construct_)
    {
        std::string s1;
        str::ostream os1 { s1 };

        os1 << "hello";

        str::ostream os2 { std::move(os1) };
        os2 << " world";

        BOOST_TEST(s1 == "hello world");
    }

    BOOST_AUTO_TEST_CASE(move_assignment_)
    {
        std::string s1;
        str::ostream os1 { s1 };

        os1 << "hello";

        std::string s2 = "bye bye";
        str::ostream os2 { s2 };
        os2 = std::move(os1);
        os2 << " world";

        BOOST_TEST(s1 == "hello world");
        BOOST_TEST(s2 == "bye bye");
    }

    BOOST_AUTO_TEST_CASE(stream_out_operator_)
    {
        using str::operator<<;

        std::string s1;

        s1 << "hello," << 1 << "," << 2.5;

        BOOST_TEST(s1 == "hello,1,2.5");
    }

BOOST_AUTO_TEST_SUITE_END() // ostream_

} // namespace testing
