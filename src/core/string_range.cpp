#include <stream9/strings/core/string_range.hpp>

#include <forward_list>
#include <iostream>
#include <list>
#include <string>

#include <concepts>
#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace rng = std::ranges;
namespace str = stream9::strings;

template<typename T, typename U>
constexpr bool is_same_type(T&&, U&&)
{
    return std::is_same_v<T, U>;
}

BOOST_AUTO_TEST_SUITE(string_range_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        template<typename S1>
        inline constexpr bool is_valid = requires (S1 s) {
            str::string_range { s };
        };

        template<typename S1>
        inline constexpr bool is_valid_iter = requires (S1 s) {
            str::string_range { s.begin(), s.end() };
        };

        BOOST_AUTO_TEST_CASE(types_)
        {
            static_assert(is_valid<char*>);
            static_assert(is_valid<char (&)[4]>);
            static_assert(is_valid<std::string>);
            static_assert(is_valid<std::vector<char>>);
            static_assert(!is_valid<std::vector<int>>);

            static_assert(is_valid_iter<std::string>);
            static_assert(is_valid_iter<std::vector<char>>);
            static_assert(!is_valid_iter<std::vector<int>>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            std::string s1 = "foo";
            str::string_range r1 { s1.data() };

            static_assert(std::same_as<decltype(r1), str::string_range<char*>>);
            BOOST_TEST((&*r1.begin() == &*s1.begin()));
            BOOST_TEST((&*r1.end() == &*s1.end()));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char const s1[] = "foo";
            str::string_range r1 { s1 };

            static_assert(std::same_as<decltype(r1), str::string_range<char const*>>);
            BOOST_TEST((&*r1.begin() == &*str::begin(s1)));
            BOOST_TEST((&*r1.end() == &*str::end(s1)));
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            std::string s1 = "foo";
            str::string_range r1 { s1 };

            static_assert(std::same_as<decltype(r1), str::string_range<std::string::iterator>>);
            BOOST_TEST((r1.begin() == s1.begin()));
            BOOST_TEST((r1.end() == s1.end()));
        }

        template<typename T>
        inline constexpr bool has_member_size = requires (T s) {
            s.size();
        };

        BOOST_AUTO_TEST_CASE(sized_range_)
        {
            std::list<char> s = { 'f', 'o', 'o' };
            str::string_range r1 = s;

            static_assert(has_member_size<decltype(r1)>);
            BOOST_TEST(r1.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(unsized_range_)
        {
            std::forward_list<char> s = { 'f', 'o', 'o' };
            str::string_range r1 = s;

            static_assert(!has_member_size<decltype(r1)>);
        }

        BOOST_AUTO_TEST_CASE(unsized_range_with_size)
        {
            std::forward_list<char> s = { 'f', 'o', 'o' };
            str::string_range r1 { s, static_cast<size_t>(3) };

            static_assert(has_member_size<decltype(r1)>);
            BOOST_TEST(r1.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(unsized_sentinel_)
        {
            std::list<char> s = { 'f', 'o', 'o' };
            str::string_range r1 { s.begin(), s.end() };

            static_assert(!has_member_size<decltype(r1)>);
        }

        BOOST_AUTO_TEST_CASE(unsized_sentinel_with_size_)
        {
            std::list<char> s = { 'f', 'o', 'o' };
            str::string_range r1 { s.begin(), s.end(), 3 };

            static_assert(has_member_size<decltype(r1)>);
            BOOST_TEST(r1.size() == 3);
        }

        BOOST_AUTO_TEST_CASE(iterator_)
        {
            std::string const s1 = "foo";
            str::string_range r1 { s1.begin(), s1.end() };

            static_assert(std::same_as<decltype(r1), str::string_range<std::string::const_iterator>>);
            BOOST_TEST((r1.begin() == s1.begin()));
            BOOST_TEST((r1.end() == s1.end()));
        }

        BOOST_AUTO_TEST_CASE(copy_)
        {
            str::string_range const r1 { "foo" };
            str::string_range r2 = r1;

            BOOST_TEST((r1.begin() == r2.begin()));
            BOOST_TEST((r1.end() == r2.end()));
        }

        BOOST_AUTO_TEST_CASE(copy_assign_)
        {
            str::string_range const r1 { "foo" };
            str::string_range r2 = r1;
            r2 = r1;

            BOOST_TEST((r1.begin() == r2.begin()));
            BOOST_TEST((r1.end() == r2.end()));
        }

        BOOST_AUTO_TEST_CASE(default_)
        {
            str::string_range<std::string::iterator> r1;

            BOOST_TEST((r1.begin() == std::string::iterator()));
            BOOST_TEST((r1.end() == std::string::iterator()));
        }

        BOOST_AUTO_TEST_CASE(subrange_)
        {
            std::string s1 = "foo";
            rng::subrange r1 = s1;

            str::string_range r2 { r1 }; // copy construct
            r2 = r1; // copy assign

            BOOST_TEST((r2.begin() == s1.begin()));
            BOOST_TEST((r2.end() == s1.end()));
        }
#if 0
        BOOST_AUTO_TEST_CASE(pair_like_)
        {
            std::string s1 = "foo";

            auto p1 = std::make_pair(s1.begin(), s1.end());

            str::string_range r1 = p1;

            BOOST_TEST((r1.begin() == s1.begin()));
            BOOST_TEST((r1.end() == s1.end()));
        }
#endif
    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(subscript_)

        template<typename T>
        inline constexpr bool is_valid = requires (T s) {
            { s[0] } -> std::same_as<str::str_char_t<T>&>;
        };

        BOOST_AUTO_TEST_CASE(type_)
        {
            static_assert(is_valid<str::string_range<char*>>);
            static_assert(is_valid<str::string_range<std::string::iterator>>);
            static_assert(!is_valid<str::string_range<std::list<char>::iterator>>);
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            str::string_range s1 = "abc";

            BOOST_TEST(s1[0] == 'a');
            BOOST_TEST(s1[1] == 'b');
            BOOST_TEST(s1[2] == 'c');
        }

    BOOST_AUTO_TEST_SUITE_END() // subscript_

    BOOST_AUTO_TEST_SUITE(remove_prefix_)

        template<typename T>
        inline constexpr bool is_valid = requires (T s) {
            s.remove_prefix(0);
        };

        BOOST_AUTO_TEST_CASE(type_)
        {
            static_assert(is_valid<str::string_range<char*>>);
            static_assert(is_valid<str::string_range<std::string::iterator>>);
            static_assert(!is_valid<str::string_range<std::list<char>::iterator>>);
            static_assert(!is_valid<str::string_range<std::forward_list<char>::iterator>>);
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            str::string_range r = "abc";

            r.remove_prefix(1);
            BOOST_TEST(r == "bc");

            r.remove_prefix(100);
            BOOST_TEST(r.empty());;
        }

    BOOST_AUTO_TEST_SUITE_END() // remove_prefix_

    BOOST_AUTO_TEST_SUITE(remove_suffix_)

        template<typename T>
        inline constexpr bool is_valid = requires (T s) {
            s.remove_suffix(0);
        };

        BOOST_AUTO_TEST_CASE(type_)
        {
            static_assert(is_valid<str::string_range<char*>>);
            static_assert(is_valid<str::string_range<std::string::iterator>>);
            static_assert(!is_valid<str::string_range<std::list<char>::iterator>>);
            static_assert(!is_valid<str::string_range<std::forward_list<char>::iterator>>);
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            str::string_range r = "abc";

            r.remove_suffix(1);
            BOOST_TEST(r == "ab");

            r.remove_suffix(100);
            BOOST_TEST(r.empty());;
        }

    BOOST_AUTO_TEST_SUITE_END() // remove_suffix_

    BOOST_AUTO_TEST_SUITE(equal_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "foo" };
            str::string_range r2 { "foo" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 == r2);
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "foo";
            str::string_range r1 { s1 };

            // same memory
            str::string_range r2 { s1.data() };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 == r2);
            BOOST_TEST(r2 == r1);

            // different memory
            str::string_range r3 { "foo" };
            static_assert(!is_same_type(r1, r3));
            BOOST_TEST(r1 == r3);
            BOOST_TEST(r3 == r1);
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "foo";
            str::string_range r1 { "foo" };

            BOOST_TEST(r1 == s1);
            BOOST_TEST(s1 == r1);
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "foo";
            str::string_range r1 { "foo" };

            BOOST_TEST(r1 == s1);
            BOOST_TEST(s1 == r1);
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "foo";
            str::string_range r1 { "foo" };

            BOOST_TEST(r1 == s1);
            BOOST_TEST(s1 == r1);
        }

    BOOST_AUTO_TEST_SUITE_END() // equal_

    BOOST_AUTO_TEST_SUITE(not_equal_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "foo" };
            str::string_range r2 { "bar" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 != r2);
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "foo";
            str::string_range r1 { s1 };

            str::string_range r2 { "bar" };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 != r2);
            BOOST_TEST(r2 != r1);
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "foo";
            str::string_range r1 { "bar" };

            BOOST_TEST(r1 != s1);
            BOOST_TEST(s1 != r1);
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "foo";
            str::string_range r1 { "bar" };

            BOOST_TEST(r1 != s1);
            BOOST_TEST(s1 != r1);
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "foo";
            str::string_range r1 { "bar" };

            BOOST_TEST(r1 != s1);
            BOOST_TEST(s1 != r1);
        }

    BOOST_AUTO_TEST_SUITE_END() // not_equal_

    BOOST_AUTO_TEST_SUITE(less_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "bar" };
            str::string_range r2 { "baz" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 < r2);
            BOOST_TEST(!(r2 < r1));
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "bar";
            str::string_range r1 { s1 };

            str::string_range r2 { "baz" };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 < r2);
            BOOST_TEST(!(r2 < r1));
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 < r1);
            BOOST_TEST(!(r1 < s1));
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 < r1);
            BOOST_TEST(!(r1 < s1));
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 < r1);
            BOOST_TEST(!(r1 < s1));
        }

    BOOST_AUTO_TEST_SUITE_END() // less_

    BOOST_AUTO_TEST_SUITE(greater_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "baz" };
            str::string_range r2 { "bar" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 > r2);
            BOOST_TEST(!(r2 > r1));
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "baz";
            str::string_range r1 { s1 };

            str::string_range r2 { "bar" };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 > r2);
            BOOST_TEST(!(r2 > r1));
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 > r1);
            BOOST_TEST(!(r1 > s1));
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 > r1);
            BOOST_TEST(!(r1 > s1));
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 > r1);
            BOOST_TEST(!(r1 > s1));
        }

    BOOST_AUTO_TEST_SUITE_END() // greater_

    BOOST_AUTO_TEST_SUITE(less_or_equal_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "bar" };
            str::string_range r2 { "baz" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 <= r2);
            BOOST_TEST(!(r2 <= r1));

            str::string_range r3 = "bar";
            BOOST_TEST(r1 <= r3);
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "bar";
            str::string_range r1 { s1 };

            str::string_range r2 { "baz" };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 <= r2);
            BOOST_TEST(!(r2 <= r1));

            str::string_range r3 = "bar";
            static_assert(!is_same_type(r1, r3));
            BOOST_TEST(r1 <= r3);
            BOOST_TEST(r3 <= r1);
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 <= r1);
            BOOST_TEST(!(r1 <= s1));

            str::string_range r2 = "bar";
            BOOST_TEST(s1 <= r2);
            BOOST_TEST(r2 <= s1);
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 <= r1);
            BOOST_TEST(!(r1 <= s1));

            str::string_range r2 = "bar";
            BOOST_TEST(s1 <= r2);
            BOOST_TEST(r2 <= s1);
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "bar";
            str::string_range r1 { "baz" };

            BOOST_TEST(s1 <= r1);
            BOOST_TEST(!(r1 <= s1));

            str::string_range r2 = "bar";
            BOOST_TEST(s1 <= r2);
            BOOST_TEST(r2 <= s1);
        }

    BOOST_AUTO_TEST_SUITE_END() // less_or_equal_

    BOOST_AUTO_TEST_SUITE(greater_or_equal_)

        BOOST_AUTO_TEST_CASE(with_same_string_range_)
        {
            str::string_range r1 { "baz" };
            str::string_range r2 { "bar" };
            static_assert(is_same_type(r1, r2));

            BOOST_TEST(r1 >= r2);
            BOOST_TEST(!(r2 >= r1));

            str::string_range r3 = "baz";
            BOOST_TEST(r1 <= r3);
        }

        BOOST_AUTO_TEST_CASE(with_different_string_range_)
        {
            std::string s1 = "baz";
            str::string_range r1 { s1 };

            str::string_range r2 { "bar" };
            static_assert(!is_same_type(r1, r2));
            BOOST_TEST(r1 >= r2);
            BOOST_TEST(!(r2 >= r1));

            str::string_range r3 = "baz";
            static_assert(!is_same_type(r1, r3));
            BOOST_TEST(r1 >= r3);
            BOOST_TEST(r3 >= r1);
        }

        BOOST_AUTO_TEST_CASE(with_range_)
        {
            std::string s1 = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 >= r1);
            BOOST_TEST(!(r1 >= s1));

            str::string_range r2 = "baz";
            BOOST_TEST(s1 >= r2);
            BOOST_TEST(r2 >= s1);
        }

        BOOST_AUTO_TEST_CASE(with_pointer_)
        {
            auto* s1 = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 >= r1);
            BOOST_TEST(!(r1 >= s1));

            str::string_range r2 = "baz";
            BOOST_TEST(s1 >= r2);
            BOOST_TEST(r2 >= s1);
        }

        BOOST_AUTO_TEST_CASE(with_array_)
        {
            char s1[] = "baz";
            str::string_range r1 { "bar" };

            BOOST_TEST(s1 >= r1);
            BOOST_TEST(!(r1 >= s1));

            str::string_range r2 = "baz";
            BOOST_TEST(s1 >= r2);
            BOOST_TEST(r2 >= s1);
        }

    BOOST_AUTO_TEST_SUITE_END() // greater_or_equal_

    BOOST_AUTO_TEST_CASE(decomposition)
    {
        str::string_range r = "foo";

        auto [first, last] = r;

        static_assert(std::same_as<decltype(first), decltype(r.begin())>);
        static_assert(std::same_as<decltype(last), decltype(r.end())>);
    }

    BOOST_AUTO_TEST_CASE(conversion_)
    {
        str::string_range r = "foo";

        using R = decltype(r);

        // explicitly to string
        static_assert(std::constructible_from<std::string, R>);

        // implicitly to string
        static_assert(!std::convertible_to<R, std::string>);

        // explicitly to string_view
        static_assert(std::constructible_from<std::string_view, R>);

        // implicitly to string_view
        static_assert(std::convertible_to<R, std::string_view>);
    }

    BOOST_AUTO_TEST_CASE(conversion_const_)
    {
        str::string_range const r = "foo";

        using R = decltype(r);

        // explicitly to string
        static_assert(std::constructible_from<std::string, R>);

        // implicitly to string
        static_assert(!std::convertible_to<R, std::string>);

        // explicitly to string_view
        static_assert(std::constructible_from<std::string_view, R>);

        // implicitly to string_view
        static_assert(std::convertible_to<R, std::string_view>);
    }

BOOST_AUTO_TEST_SUITE_END() // string_range_

} // namespace testing

