#include <stream9/strings/modifier/remove_prefix.hpp>

#include <stream9/strings/core/string_range.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(primitive_)
BOOST_AUTO_TEST_SUITE(remove_prefix_)

    template<typename S>
    concept can_invoke_with_length =
        requires (S&& s) {
            str::remove_prefix(s, 0);
        };

    template<typename S>
    concept can_invoke_with_iter =
        requires (S&& s) {
            str::remove_prefix(s, str::begin(s));
        };

    template<typename S>
    concept invocable =
           can_invoke_with_length<S>
        && can_invoke_with_iter<S>;

    template<typename S>
    concept uninvocable = !(invocable<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(invocable<std::string_view>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    BOOST_AUTO_TEST_CASE(string_by_index_)
    {
        std::string s = "foobar";

        str::remove_prefix(s, 3u);

        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_by_iterator_)
    {
        std::string s = "foobar";

        str::remove_prefix(s, str::begin(s) + 3);

        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_view_by_index_)
    {
        std::string_view s = "foobar";

        str::remove_prefix(s, 3u);

        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_view_by_iterator_)
    {
        std::string_view s = "foobar";

        str::remove_prefix(s, str::begin(s) + 3);

        BOOST_TEST(s == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_range_by_index_)
    {
        str::string_range s = "foobar";

        str::remove_prefix(s, 3u);

        BOOST_TEST(s == "bar");
    }

BOOST_AUTO_TEST_SUITE_END() // remove_prefix_
BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing

