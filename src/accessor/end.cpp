#include <stream9/strings/accessor/end.hpp>

#include <concepts>
#include <ranges>
#include <string>
#include <type_traits>
#include <utility>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

template<typename> struct type_of;

template<typename I>
using iter_pointer_t = typename std::iterator_traits<I>::pointer;

using std::same_as, std::input_or_output_iterator;

BOOST_AUTO_TEST_SUITE(primitive_)

    BOOST_AUTO_TEST_SUITE(end_)

        template<typename T>
        inline constexpr bool has_range_end = requires (T&& r) {
            rng::end(r);
        };

        template<typename S>
        inline constexpr bool can_end = requires (S&& s) {
            str::end(static_cast<S&&>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_end<std::string&>);
            static_assert(can_end<std::string const&>);
            static_assert(can_end<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            std::string s = "XYZ";
            char* p = s.data();
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_end<decltype(p)>);

            auto i = str::end(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == p + 3));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            auto* const p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_end<decltype(p)>);

            auto i = str::end(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p + 3));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_2_)
        {
            char s[] = "XYZ\0";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 5);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s + 4));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_3_)
        {
            char s[] = "\0";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 2);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s + 1));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((str::end(s) == rng::end(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_end<decltype(s)>);

            auto i = str::end(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::end(s) == rng::end(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // end_

    BOOST_AUTO_TEST_SUITE(cend_)

        template<typename T>
        inline constexpr bool has_range_cend = requires (T&& r) {
            rng::cend(r);
        };

        template<typename S>
        inline constexpr bool can_cend = requires (S&& s) {
            str::cend(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_cend<std::string&>);
            static_assert(can_cend<std::string const&>);
            static_assert(can_cend<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            std::string s = "XYZ";

            char* p = s.data();
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_cend<decltype(p)>);

            auto i = str::cend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p + 3));

        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_cend<decltype(p)>);

            auto i = str::cend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p + 3));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s + 3));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::cend(s) == rng::cend(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_cend<decltype(s)>);

            auto i = str::cend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::cend(s) == rng::cend(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // cend_

    BOOST_AUTO_TEST_SUITE(rend_)

        template<typename T>
        inline constexpr bool has_range_rend = requires (T&& r) {
            rng::rend(r);
        };

        template<typename S>
        inline constexpr bool can_rend = requires (S&& s) {
            str::rend(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_rend<std::string&>);
            static_assert(can_rend<std::string const&>);
            static_assert(can_rend<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "XYZ";
            char* p = arr;
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_rend<decltype(p)>);

            auto i = str::rend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == p - 1));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_rend<decltype(p)>);

            auto i = str::rend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p - 1));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_rend<decltype(s)>);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_rend<decltype(s)>);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_rend<decltype(s)>);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((str::rend(s) == rng::rend(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_rend<decltype(s)>);

            auto i = str::rend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::rend(s) == rng::rend(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // rend_

    BOOST_AUTO_TEST_SUITE(crend_)

        template<typename T>
        inline constexpr bool has_range_crend = requires (T&& r) {
            rng::crend(r);
        };

        template<typename S>
        inline constexpr bool can_crend = requires (S&& s) {
            str::crend(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_crend<std::string&>);
            static_assert(can_crend<std::string const&>);
            static_assert(can_crend<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "XYZ";
            char* p = arr;
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_crend<decltype(p)>);

            auto i = str::crend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p - 1));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_crend<decltype(p)>);

            auto i = str::crend(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p - 1));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_crend<decltype(s)>);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_crend<decltype(s)>);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s - 1));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_crend<decltype(s)>);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::crend(s) == rng::crend(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_crend<decltype(s)>);

            auto i = str::crend(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::crend(s) == rng::crend(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // crend_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
