#ifndef STREAM9_STRINGS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_STRINGS_TEST_SRC_NAMESPACE_HPP

namespace stream9::strings {}
namespace stream9::errors {}
namespace stream9::json {}
namespace std::ranges {}

namespace testing {

namespace st9 { using namespace stream9;}
namespace str { using namespace stream9::strings; }
namespace err { using namespace stream9::errors; }
namespace json { using namespace stream9::json; }
namespace rng { using namespace std::ranges; };

} // namespace testing

#endif // STREAM9_STRINGS_TEST_SRC_NAMESPACE_HPP
