#include <stream9/strings/view/join.hpp>

#include <ranges>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <stream9/string.hpp>
#include <stream9/strings/converter/to_string.hpp>
#include <stream9/strings/modifier/insert.hpp>

namespace testing {

namespace st9 = stream9;
namespace str = stream9::strings;
namespace rng = std::ranges;
namespace sv = str::views;

BOOST_AUTO_TEST_SUITE(join_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::vector<std::string> v {
            "foo", "bar", "xyzzy"
        };

        auto r = sv::join(v, ", ");
        static_assert(rng::view<decltype(r)>);
        //static_assert(sizeof(r.begin()) == 64); //TODO should it be smaller?

        BOOST_TEST(r == "foo, bar, xyzzy");
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::vector<char const*> v {
            "foo", "bar", "xyzzy"
        };

        auto r = sv::join(v, ", ");
        static_assert(rng::view<decltype(r)>);

        BOOST_TEST(r == "foo, bar, xyzzy");
    }

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        std::vector<std::string> v {
            "foo", "bar", "xyzzy"
        };

        auto r = sv::join(v, ", ");
        static_assert(rng::view<decltype(r)>);

        st9::string joined { r };

        BOOST_TEST(joined == "foo, bar, xyzzy");
    }

    BOOST_AUTO_TEST_CASE(insert_)
    {
        std::vector<std::string> v {
            "foo", "bar", "xyzzy"
        };

        auto r = sv::join(v, ", ");
        static_assert(rng::view<decltype(r)>);

        std::string s = ", xxx";
        str::insert(s, s.begin(), r);

        BOOST_TEST(s == "foo, bar, xyzzy, xxx");
    }

    BOOST_AUTO_TEST_CASE(empty_handling_1_)
    {
        std::vector<std::string> v {
            "foo", "bar", "xyzzy"
        };

        auto r = sv::join(v, "");

        BOOST_TEST(r == "foobarxyzzy");
    }

    BOOST_AUTO_TEST_CASE(empty_handling_2_)
    {
        std::vector<std::string> v {};

        auto r1 = sv::join(v, ", ");
        BOOST_TEST(r1 == "");

        auto r2 = sv::join(v, "");
        BOOST_TEST(r2 == "");
    }

BOOST_AUTO_TEST_SUITE_END() // join_

} // namespace testing

