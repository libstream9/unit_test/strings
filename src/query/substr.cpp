#include <stream9/strings/query/substr.hpp>

#include <string>
#include <list>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(substr_)

    template<typename S>
    inline constexpr bool is_valid = requires (S s) {
        str::substr(s, 0, 0);
    };

    BOOST_AUTO_TEST_CASE(types_)
    {
        static_assert(is_valid<char*>);
        static_assert(is_valid<char(&)[4]>);
        static_assert(is_valid<std::string>);
        static_assert(!is_valid<std::list<char>>);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string s1 = "foo bar";

        auto const r1 = str::substr(s1.data(), 4, 3);
        BOOST_TEST(r1 == "bar");

        auto const r2 = str::substr(s1.data(), 4);
        BOOST_TEST(r2 == "bar");
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = "foo bar";

        auto const r1 = str::substr(s1, 4, 3);
        BOOST_TEST(r1 == "bar");

        auto const r2 = str::substr(s1, 4);
        BOOST_TEST(r2 == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "foo bar";

        auto const r1 = str::substr(s1, 4, 3);
        BOOST_TEST(r1 == "bar");

        auto const r2 = str::substr(s1, 4);
        BOOST_TEST(r2 == "bar");
    }

BOOST_AUTO_TEST_SUITE_END() // replace_

} // namespace testing

