#include <stream9/strings/query/equal.hpp>

#include <functional>
#include <string>
#include <string_view>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(equal_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string const s1 = "foo";
        std::string const s2 = "foo";

        BOOST_TEST(str::equal(s1, s2));

        std::string_view const s3 = "bar";
        BOOST_TEST(!str::equal(s1, s3));
    }

    BOOST_AUTO_TEST_CASE(range_with_pointer_)
    {
        std::string const s1 = "foo";
        std::string const s2 = "foo";

        BOOST_TEST(str::equal(s1, s2.data()));

        std::string const s3 = "bar";
        BOOST_TEST(!str::equal(s1, s3.data()));
    }

    BOOST_AUTO_TEST_CASE(range_with_array_)
    {
        std::string s1 = "foo";
        char s2[] = "foo";

        BOOST_TEST(str::equal(s1, s2));

        char s3[] = "bar";
        BOOST_TEST(!str::equal(s1, s3));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_pointer_)
    {
        std::string const s1 = "foo";
        std::string const s2 = "foo";

        BOOST_TEST(str::equal(s1.data(), s2.data()));

        std::string const s3 = "bar";
        BOOST_TEST(!str::equal(s1.data(), s3.data()));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_array_)
    {
        std::string const s1 = "foo";
        char const s2[] = "foo";

        BOOST_TEST(str::equal(s1.data(), s2));

        char const s3[] = "bar";
        BOOST_TEST(!str::equal(s1.data(), s3));
    }

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        char s1[] = "foo";
        char const s2[] = "foo";

        BOOST_TEST(str::equal(s1, s2));

        char const s3[] = "bar";
        BOOST_TEST(!str::equal(s1, s3));
    }

    BOOST_AUTO_TEST_CASE(no_operator_)
    {
        std::vector<char> s1 = { 'f', 'o', 'o' };
        char const s2[] = "foo";

        BOOST_TEST(str::equal(s1, s2));

        char const s3[] = "bar";
        BOOST_TEST(!str::equal(s1, s3));
    }

BOOST_AUTO_TEST_SUITE_END() // equal_

BOOST_AUTO_TEST_SUITE(iequal_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string const s1 = "foo";
        std::string s2 = "Foo";

        BOOST_TEST(!str::equal(s1, s2));
        BOOST_TEST(str::iequal(s1, s2));

        std::string const s3 = "BAR";
        BOOST_TEST(!str::iequal(s1, s3));
    }

    BOOST_AUTO_TEST_CASE(range_with_pointer_)
    {
        std::string const s1 = "foo";
        std::string s2 = "Foo";

        BOOST_TEST(!str::equal(s1, s2.data()));
        BOOST_TEST(str::iequal(s1, s2.data()));

        std::string const s3 = "BAR";
        BOOST_TEST(!str::iequal(s1, s3.data()));
    }

    BOOST_AUTO_TEST_CASE(range_with_array_)
    {
        std::string const s1 = "foo";
        char s2[] = "Foo";

        BOOST_TEST(!str::equal(s1, s2));
        BOOST_TEST(str::iequal(s1, s2));

        char const s3[] = "BAR";
        BOOST_TEST(!str::iequal(s1, s3));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_pointer_)
    {
        std::string const s1 = "foo";
        std::string s2 = "Foo";

        BOOST_TEST(!str::equal(s1.data(), s2.data()));
        BOOST_TEST(str::iequal(s1.data(), s2.data()));

        std::string s3 = "BAR";
        BOOST_TEST(!str::iequal(s1.data(), s3.data()));
    }

    BOOST_AUTO_TEST_CASE(pointer_with_array_)
    {
        std::string const s1 = "foo";
        char s2[] = "Foo";

        BOOST_TEST(!str::equal(s1.data(), s2));
        BOOST_TEST(str::iequal(s1.data(), s2));

        char const s3[] = "BAR";
        BOOST_TEST(!str::iequal(s1.data(), s3));
    }

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        char const s1[] = "foo";
        char s2[] = "Foo";

        BOOST_TEST(!str::equal(s1, s2));
        BOOST_TEST(str::iequal(s1, s2));

        char const s3[] = "BAR";
        BOOST_TEST(!str::iequal(s1, s3));
    }

BOOST_AUTO_TEST_SUITE_END() // iequal_

} // namespace testing
