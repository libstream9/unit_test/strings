#include <stream9/strings/accessor/begin.hpp>

#include <string>
#include <type_traits>
#include <utility>

#include <concepts>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

template<typename I>
using iter_pointer_t = typename std::iterator_traits<I>::pointer;

using std::same_as, std::input_or_output_iterator, std::iter_value_t;

BOOST_AUTO_TEST_SUITE(primitive_)

    BOOST_AUTO_TEST_SUITE(begin_)

        template<typename T>
        inline constexpr bool has_range_begin = requires (T&& r) {
            rng::begin(r);
        };

        template<typename S>
        inline constexpr bool can_begin = requires (S&& s) {
            str::begin(static_cast<S&&>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_begin<std::string&>);
            static_assert(can_begin<std::string const&>);
            static_assert(can_begin<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            std::string s = "XYZ";
            char* p = s.data();
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_begin<decltype(p)>);

            auto i = str::begin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == p));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            auto* const p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_begin<decltype(p)>);

            auto i = str::begin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((str::begin(s) == rng::begin(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_begin<decltype(s)>);

            auto i = str::begin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::begin(s) == rng::begin(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // begin_

    BOOST_AUTO_TEST_SUITE(cbegin_)

        template<typename T>
        inline constexpr bool has_range_cbegin = requires (T&& r) {
            rng::cbegin(r);
        };

        template<typename S>
        inline constexpr bool can_cbegin = requires (S&& s) {
            str::cbegin(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_cbegin<std::string&>);
            static_assert(can_cbegin<std::string const&>);
            static_assert(can_cbegin<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            std::string s = "XYZ";

            char* p = s.data();
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_cbegin<decltype(p)>);

            auto i = str::cbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p));

        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_cbegin<decltype(p)>);

            auto i = str::cbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == p));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((i == s));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::cbegin(s) == rng::cbegin(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_cbegin<decltype(s)>);

            auto i = str::cbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::cbegin(s) == rng::cbegin(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // cbegin_

    BOOST_AUTO_TEST_SUITE(rbegin_)

        template<typename T>
        inline constexpr bool has_range_rbegin = requires (T&& r) {
            rng::rbegin(r);
        };

        template<typename S>
        inline constexpr bool can_rbegin = requires (S&& s) {
            str::rbegin(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_rbegin<std::string&>);
            static_assert(can_rbegin<std::string const&>);
            static_assert(can_rbegin<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "XYZ";
            char* p = arr;
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_rbegin<decltype(p)>);

            auto i = str::rbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == p + 2));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_rbegin<decltype(p)>);

            auto i = str::rbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p + 2));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_rbegin<decltype(s)>);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_rbegin<decltype(s)>);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_rbegin<decltype(s)>);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char*>);
            BOOST_TEST((str::rbegin(s) == rng::rbegin(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_rbegin<decltype(s)>);

            auto i = str::rbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::rbegin(s) == rng::rbegin(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // rbegin_

    BOOST_AUTO_TEST_SUITE(crbegin_)

        template<typename T>
        inline constexpr bool has_range_crbegin = requires (T&& r) {
            rng::crbegin(r);
        };

        template<typename S>
        inline constexpr bool can_crbegin = requires (S&& s) {
            str::crbegin(std::forward<S>(s));
        };

        BOOST_AUTO_TEST_CASE(value_category_)
        {
            static_assert(can_crbegin<std::string&>);
            static_assert(can_crbegin<std::string const&>);
            static_assert(can_crbegin<std::string&&>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "XYZ";
            char* p = arr;
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_crbegin<decltype(p)>);

            auto i = str::crbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p + 2));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* p = "XYZ";
            static_assert(std::is_pointer_v<decltype(p)>);
            static_assert(!has_range_crbegin<decltype(p)>);

            auto i = str::crbegin(p);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == p + 2));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_crbegin<decltype(s)>);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = { 'X', 'Y', 'Z' };
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 3);

            static_assert(has_range_crbegin<decltype(s)>);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_array_)
        {
            char s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(null_terminated_const_array_)
        {
            char const s[] = "XYZ";
            static_assert(std::is_array_v<decltype(s)>);
            static_assert(std::extent_v<decltype(s)> == 4);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((&*i == s + 2));
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            std::string s = "XYZ";
            static_assert(has_range_crbegin<decltype(s)>);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::crbegin(s) == rng::crbegin(s)));
        }

        BOOST_AUTO_TEST_CASE(const_string_)
        {
            std::string const s = "XYZ";
            static_assert(has_range_crbegin<decltype(s)>);

            auto i = str::crbegin(s);

            static_assert(input_or_output_iterator<decltype(i)>);
            static_assert(same_as<iter_pointer_t<decltype(i)>, char const*>);
            BOOST_TEST((str::crbegin(s) == rng::crbegin(s)));
        }

    BOOST_AUTO_TEST_SUITE_END() // crbegin_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
