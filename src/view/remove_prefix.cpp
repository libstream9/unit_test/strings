#include <stream9/strings/view/remove_prefix.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(remove_prefix_)

    BOOST_AUTO_TEST_CASE(by_size_)
    {
        std::string const s = "1234";

        auto v = str::views::remove_prefix(s, static_cast<size_t>(1));
        static_assert(rng::view<decltype(v)>);

        BOOST_TEST(v == "234");
    }

    BOOST_AUTO_TEST_CASE(by_iterator_)
    {
        std::string const s = "1234";

        auto it = s.begin() + 2;
        auto v = str::views::remove_prefix(s, it);
        static_assert(rng::view<decltype(v)>);

        BOOST_TEST(v == "34");
    }

BOOST_AUTO_TEST_SUITE_END() // remove_prefix_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
