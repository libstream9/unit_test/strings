#include <stream9/strings/find_prefix.hpp>

#include "../namespace.hpp"

#include <stream9/string.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::strings::find_prefix;
using st9::string_view;
using st9::string;

BOOST_AUTO_TEST_SUITE(find_prefix_)

    template<typename T1, typename T2>
    concept callable = requires (T1&& s1, T2&& s2) {
        find_prefix(std::forward<T1>(s1), std::forward<T2>(s2));
    };

    BOOST_AUTO_TEST_CASE(value_category_)
    {
        static_assert(!callable<string, string>);
        static_assert(!callable<string, string&>);
        static_assert(!callable<string, string const>);
        static_assert(!callable<string, string const&>);
        static_assert(!callable<string, string&&>);
        static_assert(callable<string&, string>);
        static_assert(callable<string&, string&>);
        static_assert(callable<string&, string const>);
        static_assert(callable<string&, string const&>);
        static_assert(callable<string&, string&&>);
        static_assert(callable<string const&, string>);
        static_assert(callable<string const&, string&>);
        static_assert(callable<string const&, string const>);
        static_assert(callable<string const&, string const&>);
        static_assert(callable<string const&, string&&>);
        static_assert(!callable<string&&, string>);
        static_assert(!callable<string&&, string&>);
        static_assert(!callable<string&&, string const>);
        static_assert(!callable<string&&, string const&>);
        static_assert(!callable<string&&, string&&>);
    }

    BOOST_AUTO_TEST_CASE(partial_match_1_)
    {
        string_view s1 = "abcde";
        auto x = find_prefix(s1, "ab");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin()+2);
    }

    BOOST_AUTO_TEST_CASE(partial_match_2_)
    {
        string_view s1 = "abcde";
        auto x = find_prefix(s1, "abd");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin());
    }

    BOOST_AUTO_TEST_CASE(full_match_)
    {
        string_view s1 = "abcde";
        auto x = find_prefix(s1, "abcde");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.end());
    }

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        string_view s1 = "abcde";
        auto x = find_prefix(s1, "bcde");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin());
    }

    BOOST_AUTO_TEST_CASE(empty_prefix_)
    {
        string_view s1 = "abcde";
        auto x = find_prefix(s1, "");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin());
    }

    BOOST_AUTO_TEST_CASE(empty_target_1_)
    {
        string_view s1 = "";
        auto x = find_prefix(s1, "");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin());
    }

    BOOST_AUTO_TEST_CASE(empty_target_2_)
    {
        string_view s1 = "";
        auto x = find_prefix(s1, "ab");

        BOOST_CHECK(x.begin() == s1.begin());
        BOOST_CHECK(x.end() == s1.begin());
    }

BOOST_AUTO_TEST_SUITE_END() // find_prefix_

} // namespace testing
