#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(std_regex_)

BOOST_AUTO_TEST_SUITE(find_regex_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s = "foobar";
        std::regex e { "b" };

        auto const r = str::regex_adaptor<std::regex>::find(s, e);

        BOOST_TEST(r.size() == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // find_regex_

BOOST_AUTO_TEST_SUITE_END() // std_regex_

} // namespace testing
