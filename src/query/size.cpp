#include <stream9/strings/query/size.hpp>

#include <forward_list>
#include <list>
#include <string>
#include <utility>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(primitive_)

BOOST_AUTO_TEST_SUITE(size_)

    template<typename S>
    inline constexpr bool is_valid = requires (S s) {
        str::size(s);
    };

    BOOST_AUTO_TEST_CASE(valid_types_)
    {
        static_assert(is_valid<char*>);
        static_assert(is_valid<char const*>);
        static_assert(is_valid<char(&)[4]>);
        static_assert(is_valid<char const(&)[4]>);
        static_assert(is_valid<std::string&>);
        static_assert(is_valid<std::string const&>);
        static_assert(is_valid<std::string&&>);
        static_assert(is_valid<std::list<char>>);
        static_assert(!is_valid<std::forward_list<char>>);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        char const* s = "foo";

        BOOST_TEST(str::size(s) == 3);
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s[] = "foo";

        BOOST_TEST(str::size(s) == 3);
    }

    BOOST_AUTO_TEST_CASE(literal_)
    {
        BOOST_TEST(str::size("foo") == 3);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s = "foo";

        BOOST_TEST(str::size(s) == 3);
    }

    BOOST_AUTO_TEST_CASE(vector_)
    {
        std::vector<char> s = { 'f', 'o', 'o' };

        BOOST_TEST(str::size(s) == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_literal_)
    {
        BOOST_TEST(str::size("") == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_pointer_)
    {
        char const* s = "";

        BOOST_TEST(str::size(s) == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_string_)
    {
        std::string s = "";

        BOOST_TEST(str::size(s) == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_vector_)
    {
        std::vector<char> s;

        BOOST_TEST(str::size(s) == 0);
    }

BOOST_AUTO_TEST_SUITE_END() // size_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
