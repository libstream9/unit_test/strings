#include <stream9/strings/core/view_interface.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_interface__)

    class my_string : public str::view_interface<my_string>
                    , public rng::view_interface<my_string>
    {
        using base_t = str::view_interface<my_string>;
    public:
        my_string() = default;

        my_string(std::string_view const s)
            : m_str { s }
        {}

        auto begin() const { return m_str.begin(); }
        auto end() const { return m_str.end(); }

    private:
        std::string m_str;
    };

    BOOST_AUTO_TEST_CASE(empty_)
    {
        my_string s;
        BOOST_TEST(s.empty());
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        my_string s { "foo" };

        BOOST_TEST((s == s));
        BOOST_TEST((s == "foo"));
        BOOST_TEST(("foo" == s));
    }

BOOST_AUTO_TEST_SUITE_END() // view_interface__

} // namespace testing
