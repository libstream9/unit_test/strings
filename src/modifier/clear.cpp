#include <stream9/strings/modifier/clear.hpp>

#include <stream9/strings/query/empty.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(clear_)

    template<typename S>
    concept invocable =
        requires (S&& s) {
            str::clear(s);
        };

    template<typename S>
    concept uninvocable = !(invocable<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "foo";

        str::clear(s1);

        BOOST_TEST(str::empty(s1));
    }

BOOST_AUTO_TEST_SUITE_END() // clear_

} // namespace testing

