#include <stream9/strings/modifier/to_upper.hpp>

#include <stream9/strings/query/equal.hpp>

#include <list>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(to_upper)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "foo";

        str::to_upper(s1);

        BOOST_TEST(s1 == "FOO");
    }

    BOOST_AUTO_TEST_CASE(list)
    {
        std::list<char> s1 = { 'f', 'o', 'o' };

        str::to_upper(s1);

        BOOST_TEST(str::equal(s1, "FOO"));
    }

BOOST_AUTO_TEST_SUITE_END() // to_upper

} // namespace testing
