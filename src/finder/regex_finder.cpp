#include <stream9/strings/finder/regex_finder.hpp>
#include <stream9/strings/regex/std.hpp>
#include <stream9/strings/regex/boost.hpp>

#include <string>

#include <boost/test/unit_test.hpp>
#include <boost/regex.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(regex_finder_)

    BOOST_AUTO_TEST_CASE(std_regex_)
    {
        std::regex e { "b" };
        str::regex_finder find { std::move(e) };

        // string
        std::string s1 = "foobar";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[3]));
    }

    BOOST_AUTO_TEST_CASE(boost_regex_)
    {
        boost::regex e { "b" };
        str::regex_finder find { std::move(e) };

        // string
        std::string s1 = "foobar";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[3]));
    }

BOOST_AUTO_TEST_SUITE_END() // regex_finder_

} // namespace testing
