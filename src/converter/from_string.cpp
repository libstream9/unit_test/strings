#include <stream9/strings/converter/from_string.hpp>

#include "../namespace.hpp"

#include <string>
#include <system_error>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/number.hpp>

/*
 * conversion methods
 * - iostream
 * - std::stoi, std::stof, std::to_string, etc
 * - std::from_chars (not support locale)
 * - std::printf / std::scanf
 * - boost::lexical_cast
 * - boost::numeric::conversion
 * - ADLed custom type converter
 */

/*
 * design consideration
 * - locale
 *   only iostream can handle locale fully and flexibly.
 * - error handling
 * - user type
 */

namespace testing {

BOOST_AUTO_TEST_SUITE(from_string_)

    //TODO test empty

    BOOST_AUTO_TEST_CASE(bool_true_1_)
    {
        std::string s1 = "true";

        auto v1 = str::from_string<bool>(s1);
        BOOST_TEST(v1 == true);
    }

    BOOST_AUTO_TEST_CASE(bool_true_2_)
    {
        std::string s1 = "True";

        auto v1 = str::from_string<bool>(s1);
        BOOST_TEST(v1 == true);
    }

    BOOST_AUTO_TEST_CASE(bool_false_1_)
    {
        std::string s1 = "false";

        auto v1 = str::from_string<bool>(s1);
        BOOST_TEST(v1 == false);
    }

    BOOST_AUTO_TEST_CASE(bool_false_2_)
    {
        std::string s1 = "False";

        auto v1 = str::from_string<bool>(s1);
        BOOST_TEST(v1 == false);
    }

    BOOST_AUTO_TEST_CASE(bool_error_1_)
    {
        try {
            std::string s1 = "XXX";
            json::object expected {
                { "s", s1 },
            };

            BOOST_CHECK_EXCEPTION(
                str::from_string<bool>(s1),
                err::error,
                [&](auto&& e) {
                    BOOST_TEST((e.why() == str::errc::invalid_bool_string));
                    BOOST_TEST(e.context() == expected);
                    return true;
                });
        }
        catch (...) {
            err::print_error();
            BOOST_REQUIRE(false);
        }
    }

    BOOST_AUTO_TEST_CASE(integer1_)
    {
        std::string s1 = "100";

        auto v1 = str::from_string<int>(s1);

        BOOST_TEST(v1 == 100);
    }

    BOOST_AUTO_TEST_CASE(integer2_)
    {
        std::string s1 = "100,000";

        auto v1 = str::from_string<int>(s1, std::locale("en_US.UTF-8"));

        BOOST_TEST(v1 == 100000);
    }

    BOOST_AUTO_TEST_CASE(integer3_)
    {
        auto const s1 = "100";

        auto const v1 = str::from_string<unsigned int>(s1, 16);

        BOOST_TEST(v1 == 256);
    }

    BOOST_AUTO_TEST_CASE(integer_error_1_)
    {
        std::string s1 = "10x0";

        BOOST_CHECK_EXCEPTION(
            str::from_string<int>(s1),
            err::error,
            [&](auto&& e) {
                using enum st9::errc;
                BOOST_TEST((e.why() == invalid_argument));

                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(integer_error_2_)
    {
        std::string s1 = "10,0";

        BOOST_CHECK_EXCEPTION(
            str::from_string<int>(s1, std::locale("en_US.UTF-8")),
            err::error,
            [&](auto&& e) {
                using enum str::errc;
                BOOST_TEST((e.why() == fail_to_convert_from_istream));

                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(float_1_)
    {
        auto s1 = "100";

        auto v1 = str::from_string<float>(s1);

        BOOST_TEST(v1 == 100.0);
    }

    BOOST_AUTO_TEST_CASE(float_2_)
    {
        auto s1 = "100.5";

        auto v1 = str::from_string<float>(s1, std::locale("en_US.UTF-8"));

        BOOST_TEST(v1 == 100.5);
    }

    BOOST_AUTO_TEST_CASE(double_)
    {
        auto s1 = "0.1e3";

        auto v1 = str::from_string<double>(s1);

        BOOST_TEST(v1 == 0.1e3);
    }

    BOOST_AUTO_TEST_CASE(double_error_1)
    {
        auto s1 = "0.1e3x";

        BOOST_CHECK_EXCEPTION(
            str::from_string<double>(s1),
            err::error,
            [&](auto&& e) {
                using enum str::errc;
                BOOST_TEST((e.why() == invalid_character));

                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(sys_time_)
    {
        namespace C = std::chrono;
        using T = C::sys_seconds;

        auto s1 = "2021-01-27 20:25:52";
        auto fmt = "%F %T";

        auto v1  = str::from_string<T>(s1, fmt);

        struct tm tm {};
        strptime(s1, fmt, &tm);
        auto expected = timelocal(&tm);

        BOOST_TEST(C::system_clock::to_time_t(v1) == expected);
    }

    BOOST_AUTO_TEST_CASE(file_time_)
    {
        namespace C = std::chrono;
        using T = C::file_time<C::seconds>;

        auto s1 = "2021-01-27 20:25:52";
        auto fmt = "%F %T";

        auto v1 = str::from_string<T>(s1, fmt);

        struct tm tm {};
        strptime(s1, fmt, &tm);
        auto expected = timelocal(&tm);

        auto stp = C::file_clock::to_sys(v1);
        BOOST_TEST(C::system_clock::to_time_t(stp) == expected);
    }

    BOOST_AUTO_TEST_CASE(safe_integer_)
    {
        using T = st9::natural<>;

        auto v1 = str::from_string<T>("100");

        BOOST_TEST(v1 == 100);
    }

BOOST_AUTO_TEST_SUITE_END() // from_string_

} // namespace testing
