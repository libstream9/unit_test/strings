#include <stream9/strings/modifier/trim.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(trim_)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = " foo ";

        str::trim(s1);
        BOOST_TEST(s1 == "foo");

        std::string s2 = "...bar...";

        str::trim(s2, str::is_punct<char>());
        BOOST_TEST(s2 == "bar");
    }

    BOOST_AUTO_TEST_CASE(string_view)
    {
        std::string_view s1 = " foo ";

        str::trim(s1);
        BOOST_TEST(s1 == "foo");

        std::string_view s2 = "...bar...";

        str::trim(s2, str::is_punct<char>());
        BOOST_TEST(s2 == "bar");
    }

    BOOST_AUTO_TEST_CASE(with_char_)
    {
        std::string_view s1 = " foo ";

        str::trim(s1, ' ');
        BOOST_TEST(s1 == "foo");

        std::string_view s2 = "...bar...";

        str::trim(s2, '.');
        BOOST_TEST(s2 == "bar");
    }

    //TODO array & pointer
BOOST_AUTO_TEST_SUITE_END() // trim_

} // namespace testing
