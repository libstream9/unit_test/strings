#include <stream9/strings/query/lexicographical_compare_three_way.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(lexicographical_compare_three_way_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string s1 = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(range_with_pointer_)
    {
        std::string s1 = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
    }

    BOOST_AUTO_TEST_CASE(pointer_with_range_)
    {
        std::string s1 = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1.data(), s2)) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1.data(), s2)) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1.data(), s2)) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1.data(), s2)) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1.data(), s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(range_with_array_)
    {
        std::string s1 = "foo";

        {
            char s2[] = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(array_with_range_)
    {
        char const s1[] = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(pointer_with_array_)
    {
        std::string s0 = "foo";
        auto s1 = s0.data();

        {
            char s2[] = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(array_with_pointer_)
    {
        char const s1[] = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2.data())) );
        }
    }

    BOOST_AUTO_TEST_CASE(array_with_array_)
    {
        char const s1[] = "foo";

        {
            char s2[] = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char const s2[] = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
        {
            char s2[] = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1, s2)) );
        }
    }

    BOOST_AUTO_TEST_CASE(pointer_with_pointer_)
    {
        std::string s1 = "foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1.data(), s2.data())) );
        }
        {
            std::string s2 = "fooa";
            BOOST_TEST(
                std::is_lt(str::lexicographical_compare_three_way(s1.data(), s2.data())) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::lexicographical_compare_three_way(s1.data(), s2.data())) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1.data(), s2.data())) );
        }
        {
            std::string s2 = "fo";
            BOOST_TEST(
                std::is_gt(str::lexicographical_compare_three_way(s1.data(), s2.data())) );
        }
    }

BOOST_AUTO_TEST_SUITE_END() // lexicographical_compare_three_way_

BOOST_AUTO_TEST_SUITE(ilexicographical_compare_three_way_)

    BOOST_AUTO_TEST_CASE(range_with_range_)
    {
        std::string s1 = "Foo";

        {
            std::string s2 = "fop";
            BOOST_TEST(
                std::is_lt(str::ilexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fOOa";
            BOOST_TEST(
                std::is_lt(str::ilexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "foo";
            BOOST_TEST(
                std::is_eq(str::ilexicographical_compare_three_way(s1, s2)) );
        }
        {
            std::string s2 = "fon";
            BOOST_TEST(
                std::is_gt(str::ilexicographical_compare_three_way(s1, s2)) );
        }
    }

BOOST_AUTO_TEST_SUITE_END() // ilexicographical_compare_

} // namespace testing
