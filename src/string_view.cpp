#include <stream9/strings/string_view.hpp>

#include "namespace.hpp"

#include <stream9/string.hpp>
#include <stream9/optional.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;
using st9::string_view;
using st9::opt;

BOOST_AUTO_TEST_SUITE(basic_string_view_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        static_assert(str::contiguous_string<string_view>);
        static_assert(std::is_nothrow_copy_constructible_v<string_view>);
        static_assert(std::is_nothrow_copy_assignable_v<string_view>);
        static_assert(std::is_nothrow_move_constructible_v<string_view>);
        static_assert(std::is_nothrow_move_assignable_v<string_view>);
        static_assert(std::is_nothrow_destructible_v<string_view>);
        static_assert(std::is_nothrow_swappable_v<string_view>);
    }

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(literal_)
        {
            string_view s = "foo";

            BOOST_TEST(s == "foo");
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            string s1 = "foo";
            string_view s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char const* s1 = "foo";
            string_view s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(std_string_)
        {
            std::string s1 = "foo";

            string_view s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(std_string_view_)
        {
            std::string_view s1 = "foo";

            string_view s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(first_and_length_)
        {
            string s1 = "foo";

            string_view s2 { s1.data(), s1.size() };

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(copy_)
        {
            string_view s1 = "foo";
            string_view s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            string_view s1 = "foo";
            string_view s2 = std::move(s1);

            BOOST_TEST(s2 == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(assignment_)

        BOOST_AUTO_TEST_CASE(literal_)
        {
            string_view s = "foo";
            s = "bar";

            BOOST_TEST(s == "bar");
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            string_view s2 = "";
            string s1 = "foo";
            s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            string_view s2 = "";
            char const* s1 = "foo";
            s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(std_string_)
        {
            string_view s2 = "";
            std::string s1 = "foo";
            s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(std_string_view_)
        {
            string_view s2 = "";
            std::string_view s1 = "foo";
            s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // assignment_

    BOOST_AUTO_TEST_CASE(swap_)
    {
        string_view s1 = "foo";
        string_view s2 = "bar";

        using std::swap;
        swap(s1, s2);

        BOOST_TEST(s1 == "bar");
        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(conversion_to_std_string_view_)
    {
        string_view s1 = "foo";

        std::string_view s2 = s1;
        BOOST_TEST(s2 == "foo");

        std::string_view s3;
        s3 = s1;
        BOOST_TEST(s3 == "foo");
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        string_view s1 = "0123";

        BOOST_TEST(s1[0] == '0');
        BOOST_TEST(s1[3] == '3');

        BOOST_CHECK_THROW(s1[4], st9::error);
        BOOST_CHECK_THROW(s1[-1], std::exception);
    }

    BOOST_AUTO_TEST_CASE(remove_prefix_)
    {
        string_view s1 = "12345";

        s1.remove_prefix(2);
        BOOST_TEST(s1 == "345");

        s1.remove_prefix(5);
        BOOST_TEST(s1 == "");
    }

    BOOST_AUTO_TEST_CASE(remove_suffix_)
    {
        string_view s1 = "12345";

        s1.remove_suffix(2);
        BOOST_TEST(s1 == "123");

        s1.remove_suffix(5);
        BOOST_TEST(s1 == "");
    }

    BOOST_AUTO_TEST_CASE(equality_1_)
    {
        string_view s1 = "12345";
        string_view s2 = "12345";

        BOOST_TEST(s1 == s2);
    }

    BOOST_AUTO_TEST_CASE(equality_2_)
    {
        string_view s1 = "12345";
        std::string s2 = "1234";

        BOOST_TEST(s1 != s2);
    }

    BOOST_AUTO_TEST_CASE(compare_1_)
    {
        string_view s1 = "1234";
        string_view s2 = "12345";

        BOOST_TEST(s1 < s2);
    }

    BOOST_AUTO_TEST_CASE(compare_2_)
    {
        string_view s1 = "12345";
        std::string s2 = "1234";

        BOOST_TEST(s1 > s2);
    }

    BOOST_AUTO_TEST_CASE(compare_3_)
    {
        string_view s1 = "12345";
        str::string_range s2 = "1234";

        BOOST_TEST(s1 > s2);
    }

    BOOST_AUTO_TEST_CASE(optional_)
    {
        opt<string_view> o_s;

        static_assert(sizeof(o_s) == sizeof(string_view));
        BOOST_TEST(!o_s);

        o_s = "foo";
        BOOST_TEST(!!o_s);
        BOOST_TEST(o_s == "foo");
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        string_view s1 = "12345";

        auto [i, e] = s1;

        BOOST_TEST(i == s1.begin());
        BOOST_TEST(e == s1.end());
    }

BOOST_AUTO_TEST_SUITE_END() // basic_string_view_

} // namespace testing
