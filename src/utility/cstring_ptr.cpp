#include <stream9/strings/utility/cstring_ptr.hpp>

#include "../namespace.hpp"

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/optional.hpp>

namespace testing {

using str::cstring_ptr;
using stream9::opt;

BOOST_AUTO_TEST_SUITE(cstring_ptr_)

    BOOST_AUTO_TEST_CASE(concept_)
    {
        str::cstring_ptr s = "abc";

        static_assert(str::contiguous_string<decltype(s)>);

        static_assert(!std::is_constructible_v<cstring_ptr, nullptr_t>);
    }

#if 0
    template<typename Ch, typename S>
    inline constexpr bool is_value_initializable_v =
        requires (S&& s) {
            str::basic_cstring_ptr<Ch> { s };
        };

    using chars = std::tuple<char, unsigned char, char8_t, char16_t, char32_t>;

    BOOST_AUTO_TEST_CASE_TEMPLATE(value_initialize_, Ch, chars)
    {
        using U = std::remove_cv_t<Ch>;

        static_assert(is_value_initializable_v<Ch, Ch*>);
        static_assert(is_value_initializable_v<Ch, std::basic_string<U>>);
        static_assert(is_value_initializable_v<Ch, std::basic_string_view<U>>);
        static_assert(is_value_initializable_v<Ch, U const*>);
        static_assert(is_value_initializable_v<Ch, std::basic_string<U> const>);
        static_assert(is_value_initializable_v<Ch, std::basic_string_view<U> const>);
        static_assert(is_value_initializable_v<Ch, std::vector<U>>);
        static_assert(is_value_initializable_v<Ch, std::vector<U> const>);

        static_assert(is_value_initializable_v<Ch const, Ch*>);
        static_assert(is_value_initializable_v<Ch const, std::basic_string<U>>);
        static_assert(is_value_initializable_v<Ch const, std::basic_string_view<U>>);
        static_assert(is_value_initializable_v<Ch const, U const*>);
        static_assert(is_value_initializable_v<Ch const, std::basic_string<U> const>);
        static_assert(is_value_initializable_v<Ch const, std::basic_string_view<U> const>);
        static_assert(is_value_initializable_v<Ch const, std::vector<U>>);
        static_assert(is_value_initializable_v<Ch const, std::vector<U> const>);
    }

    using compatibles = std::tuple<std::tuple<char8_t, char>,
                                   std::tuple<char8_t, unsigned char>,
                                   std::tuple<char8_t, signed char>,
                                   std::tuple<char, char8_t>,
                                   std::tuple<char, signed char>,
                                   std::tuple<char, unsigned char>,
                                   std::tuple<signed char, char>,
                                   std::tuple<signed char, unsigned char>,
                                   std::tuple<signed char, char8_t>,
                                   std::tuple<unsigned char, char>,
                                   std::tuple<unsigned char, signed char>,
                                   std::tuple<unsigned char, char8_t>
                        >;

    BOOST_AUTO_TEST_CASE_TEMPLATE(compatible_character_1_, T, compatibles)
    {
        using C1 = std::tuple_element_t<0, T>;
        using C2 = std::tuple_element_t<1, T>;

        static_assert(is_value_initializable_v<C1, C2*>);
        static_assert(is_value_initializable_v<C1, std::basic_string<C2>>);
        static_assert(is_value_initializable_v<C1, std::basic_string_view<C2>>);
        static_assert(is_value_initializable_v<C1, C2 const*>);
        static_assert(is_value_initializable_v<C1, std::basic_string<C2> const>);
        static_assert(is_value_initializable_v<C1, std::basic_string_view<C2> const>);
        static_assert(is_value_initializable_v<C1, std::vector<C2>>);
        static_assert(is_value_initializable_v<C1, std::vector<C2> const>);

        static_assert(is_value_initializable_v<C1 const, C2*>);
        static_assert(is_value_initializable_v<C1 const, std::basic_string<C2>>);
        static_assert(is_value_initializable_v<C1 const, std::basic_string_view<C2>>);
        static_assert(is_value_initializable_v<C1 const, C2 const*>);
        static_assert(is_value_initializable_v<C1 const, std::basic_string<C2> const>);
        static_assert(is_value_initializable_v<C1 const, std::basic_string_view<C2> const>);
        static_assert(is_value_initializable_v<C1 const, std::vector<C2>>);
        static_assert(is_value_initializable_v<C1 const, std::vector<C2> const>);
    }
#endif

#if 0
    BOOST_AUTO_TEST_CASE(compatible_character_2_)
    {
        auto const s1 = "foo";
        str::basic_cstring_ptr<char8_t const> cs1 = s1;
        BOOST_TEST(((void*)&s1[0] == (void*)&cs1[0]));

        std::string s2 = "foo";
        str::basic_cstring_ptr<char8_t> cs2 { s2 };
        BOOST_TEST(((void*)&s2[0] == (void*)&cs2[0]));

        std::string_view s3 = "foo";
        str::basic_cstring_ptr<char8_t const> cs3 = s3;
        BOOST_TEST(((void*)&s3[0] == (void*)&cs3[0]));
    }
#endif

    BOOST_AUTO_TEST_CASE(construct_from_char_literal_)
    {
        auto const s = "foo";
        str::cstring_ptr cs = s;

        BOOST_TEST((&s[0] == &cs[0]));
        BOOST_TEST(cs == "foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_char_array_)
    {
        char s[] = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST((&s[0] == &cs[0]));
        BOOST_TEST(cs == "foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_)
    {
        std::string s = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST((&s[0] == &cs[0]));
        BOOST_TEST(cs == "foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_null_terminated_string_range_)
    {
        std::string_view s = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST((&s[0] == &cs[0]));
        BOOST_TEST(cs == "foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_unterminated_string_range_)
    {
        std::string str = "foobar";
        auto const s = std::string_view(str).substr(0, 3);
        BOOST_TEST(s == "foo");

        str::cstring_ptr cs { s };

        BOOST_TEST((&s[0] != &cs[0]));
        BOOST_TEST(cs == "foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_path_)
    {
        std::filesystem::path p = "/foo";

        str::cstring_ptr cs { p };
        //str::cstring_ptr cs { p.c_str() };

        BOOST_TEST(cs == "/foo");
    }

    BOOST_AUTO_TEST_CASE(construct_from_different_constness_)
    {
        std::string s = "foo";
        str::basic_cstring_ptr<char const> cs = s;

        BOOST_TEST((&s[0] == &cs[0]));
    }

    BOOST_AUTO_TEST_CASE(construct_from_different_constness_2_)
    {
        char s[] = "foo";
        str::basic_cstring_ptr<char const> cs { s };

        BOOST_TEST((&s[0] == &cs[0]));

    }

    BOOST_AUTO_TEST_CASE(iterator_from_literal_)
    {
        auto const s = "foo";
        str::cstring_ptr cs { s };

        auto i = cs.begin();
        auto const end = cs.end();

        BOOST_TEST((i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i == end));
    }

    BOOST_AUTO_TEST_CASE(iterator_from_literal_const_)
    {
        auto const s = "foo";
        str::cstring_ptr const cs { s };

        auto i = cs.begin();
        auto const end = cs.end();

        BOOST_TEST((i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i == end));
    }

    BOOST_AUTO_TEST_CASE(iterator_from_std_string_)
    {
        std::string s = "foo";
        str::cstring_ptr cs { s };

        auto i = cs.begin();
        auto const end = cs.end();

        BOOST_TEST((i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i == end));
    }

    BOOST_AUTO_TEST_CASE(iterator_from_std_string_const_)
    {
        std::string s = "foo";
        str::cstring_ptr const cs { s };

        auto i = cs.begin();
        auto const end = cs.end();

        BOOST_TEST((i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i != end));
        BOOST_TEST((++i == end));
    }

    BOOST_AUTO_TEST_CASE(data_from_literal_)
    {
        auto const s = "foo";
        str::cstring_ptr cs { s };

        auto* const ptr = cs.data();

        BOOST_TEST((ptr == &s[0]));
    }

    BOOST_AUTO_TEST_CASE(data_from_literal_const_)
    {
        auto const s = "foo";
        str::cstring_ptr const cs { s };

        auto* const ptr = cs.data();

        BOOST_TEST((ptr == &s[0]));
    }

    BOOST_AUTO_TEST_CASE(data_from_std_string_)
    {
        std::string const s = "foo";
        str::cstring_ptr cs { s };

        auto* const ptr = cs.data();

        BOOST_TEST((ptr == &s[0]), (void*)ptr << " and " << (void*)&s[0]);
    }

    BOOST_AUTO_TEST_CASE(data_from_std_string_const_)
    {
        std::string const s = "foo";
        str::cstring_ptr const cs { s };

        auto* const ptr = cs.data();

        BOOST_TEST((ptr == &s[0]));
    }

    BOOST_AUTO_TEST_CASE(size_from_literal_)
    {
        auto const s = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST(cs.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(size_from_literal_const_)
    {
        auto const s = "foo";
        str::cstring_ptr const cs { s };

        BOOST_TEST(cs.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(size_from_std_string_)
    {
        std::string s = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST(cs.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(size_from_std_string_const_)
    {
        std::string const s = "foo";
        str::cstring_ptr cs { s };

        BOOST_TEST(cs.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_literal_)
    {
        auto const s = "foo";
        str::cstring_ptr cs { s };

        char const* ptr = cs;
        BOOST_TEST((ptr == s));
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_literal_const_)
    {
        auto const s = "foo";
        str::cstring_ptr const cs { s };

        char const* ptr = cs;
        BOOST_TEST((ptr == s));
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_std_string_)
    {
        std::string s = "foo";
        str::cstring_ptr cs { s };

        char const* ptr = cs;
        BOOST_TEST((ptr == s));
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_std_string_const_)
    {
        std::string const s = "foo";
        str::cstring_ptr const cs { s };

        char const* ptr = cs;
        BOOST_TEST((ptr == s));
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view_)
    {
        auto const s = "foo";
        str::cstring_ptr const cs { s };

        std::string_view sv = cs;
        BOOST_TEST((sv == s));
    }

    BOOST_AUTO_TEST_CASE(iterator_difference_)
    {
        str::cstring_ptr const cs = "foo";

        auto it1 = cs.begin();
        auto it2 = it1 + 2;

        BOOST_CHECK(it2 - it1 == 2);
    }

    BOOST_AUTO_TEST_CASE(json_)
    {
        str::cstring_ptr const cs = "foo";

        auto v = json::value_from(cs);

        json::value expected = "foo";

        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_CASE(null_)
    {
        opt<cstring_ptr> o_s;
        static_assert(sizeof(o_s) == sizeof(cstring_ptr));

        BOOST_REQUIRE(!o_s);

        o_s = "foo";
        BOOST_REQUIRE(o_s);
        BOOST_TEST(*o_s == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // cstring_

} // namespace testing
