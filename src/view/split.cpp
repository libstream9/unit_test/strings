#include <stream9/strings/view/split.hpp>

#include <stream9/strings/finder/last_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;
namespace sv = str::views;

BOOST_AUTO_TEST_SUITE(split_)

    BOOST_AUTO_TEST_CASE(string_with_whitespace_)
    {
        std::string const s1 = "one two\t three";

        auto v = sv::split(s1);
        static_assert(rng::view<decltype(v)>);

        auto [it, end] = v;

        BOOST_TEST((it != end));
        auto s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[0]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[4]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 5);
        BOOST_TEST((&s2[0] == &s1[9]));

        ++it;
        BOOST_TEST((it == end));
    }

    BOOST_AUTO_TEST_CASE(string_with_char_)
    {
        std::string const s1 = "one,two,three";

        auto v = sv::split(s1, ',');
        static_assert(rng::view<decltype(v)>);

        auto [it, end] = v;

        BOOST_TEST((it != end));
        auto s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[0]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[4]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 5);
        BOOST_TEST((&s2[0] == &s1[8]));

        ++it;
        BOOST_TEST((it == end));
    }

    BOOST_AUTO_TEST_CASE(string_with_string_)
    {
        std::string const s1 = "one two three";

        auto v = sv::split(s1, " ");
        static_assert(rng::view<decltype(v)>);

        auto [it, end] = v;

        BOOST_TEST((it != end));
        auto s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[0]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[4]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 5);
        BOOST_TEST((&s2[0] == &s1[8]));

        ++it;
        BOOST_TEST((it == end));
    }

    BOOST_AUTO_TEST_CASE(string_with_regex_)
    {
        std::string const s1 = "one  two three";
        std::regex e { "\\s+" };

        auto v = sv::split(s1, e);
        static_assert(rng::view<decltype(v)>);

        auto [it, end] = v;

        BOOST_TEST((it != end));
        auto s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[0]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[5]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 5);
        BOOST_TEST((&s2[0] == &s1[9]));

        ++it;
        BOOST_TEST((it == end));
    }

    BOOST_AUTO_TEST_CASE(string_with_finder_)
    {
        std::string const s1 = "one.two...three";

        str::last_finder fn { '.' };
        auto v = sv::split(s1, fn, sv::split_option::skip_empty_item);
        static_assert(rng::view<decltype(v)>);

        auto [it, end] = v;

        BOOST_TEST((it != end));
        auto s2 = *it;
        BOOST_TEST(s2.size() == 5);
        BOOST_TEST((&s2[0] == &s1[10]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[4]));

        ++it;
        BOOST_TEST((it != end));
        s2 = *it;
        BOOST_TEST(s2.size() == 3);
        BOOST_TEST((&s2[0] == &s1[0]));

        ++it;
        BOOST_TEST((it == end));
    }

    BOOST_AUTO_TEST_CASE(empty_handling_)
    {
        std::string const s1 = "one two\t three";

        auto v = sv::split(s1, "");
        std::vector<std::string_view> result;
        rng::copy(v, std::back_inserter(result));

        auto const expected = { "one two\t three" };

        BOOST_TEST(result == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // split_

} // namespace testing
