#include <stream9/strings/modifier/to_lower.hpp>

#include <stream9/strings/query/equal.hpp>

#include <string>
#include <list>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(to_lower)

    BOOST_AUTO_TEST_CASE(string)
    {
        std::string s1 = "FOO";

        str::to_lower(s1);

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(list)
    {
        std::list<char> s1 = { 'F', 'o', 'O' };

        str::to_lower(s1);

        BOOST_TEST(str::equal(s1, "foo"));
    }

BOOST_AUTO_TEST_SUITE_END() // to_lower

} // namespace testing
