#include <stream9/strings/query/contains.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(contains_)

    BOOST_AUTO_TEST_CASE(char_)
    {
        std::string s = "foo";

        BOOST_TEST(str::contains(s, 'o'));
        BOOST_TEST(!str::contains(s, 'x'));
    }

    BOOST_AUTO_TEST_CASE(substring_)
    {
        std::string const s = "foo bar xyzzy";

        BOOST_TEST(str::contains(s, "bar"));
        BOOST_TEST(!str::contains(s, "baz"));
    }

BOOST_AUTO_TEST_SUITE_END() // contains_

} // namespace testing
