#include <stream9/strings/modifier/remove_suffix.hpp>

#include "../namespace.hpp"

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

using std::string_view;

BOOST_AUTO_TEST_SUITE(remove_suffix_)

    BOOST_AUTO_TEST_CASE(string_by_index_)
    {
        std::string s = "foobar";

        str::remove_suffix(s, 3u);

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_view_index_)
    {
        std::string_view s = "foobar";

        str::remove_suffix(s, 3u);

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_suffix_1_)
    {
        string_view s1 = "foo.txt";
        string_view suffix = ".txt";

        str::remove_suffix(s1, suffix);

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_suffix_2_)
    {
        string_view s1 = "foo.txt";
        string_view suffix = ".zip";

        str::remove_suffix(s1, suffix);

        BOOST_TEST(s1 == "foo.txt");
    }

    BOOST_AUTO_TEST_CASE(with_suffix_3_)
    {
        string_view s1 = "foo.txt";
        string_view suffix = "";

        str::remove_suffix(s1, suffix);

        BOOST_TEST(s1 == "foo.txt");
    }

    BOOST_AUTO_TEST_CASE(with_suffix_4_)
    {
        string_view s1 = "";
        string_view suffix = ".txt";

        str::remove_suffix(s1, suffix);

        BOOST_TEST(s1 == "");
    }

    BOOST_AUTO_TEST_CASE(with_suffix_5_)
    {
        string_view s1 = "";
        string_view suffix = "";

        str::remove_suffix(s1, suffix);

        BOOST_TEST(s1 == "");
    }

BOOST_AUTO_TEST_SUITE_END() // remove_suffix_

} // namespace testing
