#include <stream9/strings/modifier/insert.hpp>

#include <stream9/strings/query/size.hpp>

#include "../hack.hpp"

#include <tuple>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

//TODO non member

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(primitive_)

BOOST_AUTO_TEST_SUITE(insert_)

    template<typename S>
    concept can_insert_char_by_idx =
        requires (S&& s) {
            str::insert(s, 0, str::str_char_t<S>());
        };

    template<typename S>
    concept can_insert_char_by_iter =
        requires (S&& s) {
            str::insert(s, str::end(s), str::str_char_t<S>());
        };

    template<typename S>
    concept can_insert_str_by_idx =
        requires (S&& s) {
            str::insert(s, 0, s);
        };

    template<typename S>
    concept can_insert_str_by_iter =
        requires (S&& s) {
            str::insert(s, str::end(s), s);
        };

    template<typename S>
    concept invocable =
           can_insert_char_by_idx<S>
        && can_insert_char_by_iter<S>
        && can_insert_str_by_idx<S>
        && can_insert_str_by_iter<S>;

    template<typename S>
    concept uninvocable =
           (!can_insert_char_by_idx<S>)
        && (!can_insert_char_by_iter<S>)
        && (!can_insert_str_by_idx<S>)
        && (!can_insert_str_by_iter<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    using output_types = std::tuple<std::string, std::vector<char>>;

    BOOST_AUTO_TEST_CASE_TEMPLATE(char_by_index, StringT, output_types)
    {
        StringT s1;

        str::insert(s1, 0, '1');
        BOOST_TEST(s1 == "1");

        str::insert(s1, 0, '2');
        BOOST_TEST(s1 == "21");

        str::insert(s1, 1, '3');
        BOOST_TEST(s1 == "231");
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(char_by_iterator, StringT, output_types)
    {
        StringT s1;

        str::insert(s1, s1.begin(), '1');
        BOOST_TEST(s1 == "1");

        str::insert(s1, s1.cbegin(), '2');
        BOOST_TEST(s1 == "21");

        str::insert(s1, s1.end(), '3');
        BOOST_TEST(s1 == "213");

        str::insert(s1, s1.cend(), '4');
        BOOST_TEST(s1 == "2134");
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(string_by_index, StringT, output_types)
    {
        StringT s1;
        using diff_t = StringT::difference_type;

        // std::string
        std::string const s2 = "1";
        str::insert(s1, 0, s2);
        BOOST_CHECK_EQUAL(s1, "1");

        // std::string_view
        std::string_view const s3 = "2";
        str::insert(s1, static_cast<diff_t>(str::size(s1)), s3);
        BOOST_CHECK_EQUAL(s1, "12");

        // std::vector
        std::vector<char> s4 { '3' };
        str::insert(s1, 0u, s4);
        BOOST_CHECK_EQUAL(s1, "312");

        // literal
        str::insert(s1, static_cast<diff_t>(str::size(s1)), "4");
        BOOST_CHECK_EQUAL(s1, "3124");

        // array
        char s6[] = "5";
        str::insert(s1, 0u, s6);
        BOOST_CHECK_EQUAL(s1, "53124");

        // pointer
        char const* s7 = "6";
        str::insert(s1, static_cast<diff_t>(str::size(s1)), s7);
        BOOST_CHECK_EQUAL(s1, "531246");
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(string_by_iterator, StringT, output_types)
    {
        StringT s1;

        // std::string
        std::string const s2 = "1";
        str::insert(s1, s1.begin(), s2);
        BOOST_CHECK_EQUAL(s1, "1");

        // std::string_view
        std::string_view const s3 = "2";
        str::insert(s1, s1.end(), s3);
        BOOST_CHECK_EQUAL(s1, "12");

        // std::vector
        std::vector<char> s4 { '3' };
        str::insert(s1, s1.begin(), s4);
        BOOST_CHECK_EQUAL(s1, "312");

        // literal
        str::insert(s1, s1.end(), "4");
        BOOST_CHECK_EQUAL(s1, "3124");

        // array
        char s6[] = "5";
        str::insert(s1, s1.begin(), s6);
        BOOST_CHECK_EQUAL(s1, "53124");

        // pointer
        char const* s7 = "6";
        str::insert(s1, s1.end(), s7);
        BOOST_CHECK_EQUAL(s1, "531246");
    }

BOOST_AUTO_TEST_SUITE_END() // insert_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing

