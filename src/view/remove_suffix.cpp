#include <stream9/strings/view/remove_suffix.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(remove_suffix_)

    BOOST_AUTO_TEST_CASE(by_size_)
    {
        std::string const s = "1234";

        auto v = str::views::remove_suffix(s, 1);
        static_assert(rng::view<decltype(v)>);

        BOOST_TEST(v == "123");
    }

    BOOST_AUTO_TEST_CASE(by_iterator_)
    {
        std::string const s = "1234";

        auto it = s.begin() + 2;
        auto v = str::views::remove_suffix(s, it);
        static_assert(rng::view<decltype(v)>);

        BOOST_TEST(v == "12");
    }

BOOST_AUTO_TEST_SUITE_END() // remove_suffix_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing

