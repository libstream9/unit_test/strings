#include <stream9/strings/query/find_first.hpp>

#include "make.hpp"

#include <algorithm>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <list>
#include <string>
#include <string_view>
#include <tuple>

#include <experimental/ranges/algorithm>

#include <boost/test/unit_test.hpp>

using namespace std::literals;

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(through_)

BOOST_AUTO_TEST_SUITE(find_first_)

    using string_types = std::tuple<
        std::string,
        std::string_view,
        std::vector<char>,
        std::list<char>,
        std::forward_list<char>
    >;

    using range_type_product = std::tuple<
        std::tuple<std::string, std::string>,
        std::tuple<std::string, std::string_view>,
        std::tuple<std::string, std::vector<char>>,
        std::tuple<std::string, std::list<char>>,
        std::tuple<std::string, std::forward_list<char>>,
        std::tuple<std::string_view, std::string>,
        std::tuple<std::string_view, std::string_view>,
        std::tuple<std::string_view, std::vector<char>>,
        std::tuple<std::string_view, std::list<char>>,
        std::tuple<std::string_view, std::forward_list<char>>,
        std::tuple<std::vector<char>, std::string>,
        std::tuple<std::vector<char>, std::string_view>,
        std::tuple<std::vector<char>, std::vector<char>>,
        std::tuple<std::vector<char>, std::list<char>>,
        std::tuple<std::vector<char>, std::forward_list<char>>,
        std::tuple<std::list<char>, std::string>,
        std::tuple<std::list<char>, std::string_view>,
        std::tuple<std::list<char>, std::vector<char>>,
        std::tuple<std::list<char>, std::list<char>>,
        std::tuple<std::list<char>, std::forward_list<char>>,
        std::tuple<std::forward_list<char>, std::string>,
        std::tuple<std::forward_list<char>, std::string_view>,
        std::tuple<std::forward_list<char>, std::vector<char>>,
        std::tuple<std::forward_list<char>, std::list<char>>,
        std::tuple<std::forward_list<char>, std::forward_list<char>>
    >;

    BOOST_AUTO_TEST_CASE_TEMPLATE(char_, T, string_types)
    {
        auto s = make<T>("foo bar");
        auto const i = str::find_first(s, ' ');
        BOOST_TEST((i == rng::next(s.begin(), 3)));
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(range_range, T, range_type_product)
    {
        using input_t = std::tuple_element_t<0, T>;
        using pattern_t = std::tuple_element_t<1, T>;

        auto const s = make<input_t>("foo bar");
        auto const keyword = make<pattern_t>("bar");

        auto const r = str::find_first(s, keyword);
        BOOST_TEST(std::distance(s.begin(), r.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r.end()) == 7);
    }

BOOST_AUTO_TEST_SUITE_END() // find_first_

BOOST_AUTO_TEST_SUITE_END() // through_

} // namespace testing
