#include <stream9/strings/view/to_upper.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace rng = std::ranges;
namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(to_upper_)

    template<rng::view V>
    constexpr bool is_view_v = true;

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s1 = "foo";

        auto r = str::views::to_upper(s1);
        static_assert(is_view_v<decltype(r)>);

        BOOST_TEST(r == "FOO");
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string const s1 = "foo";

        auto r = str::views::to_upper(s1.data());
        static_assert(is_view_v<decltype(r)>);

        BOOST_TEST(r == "FOO");
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = "foo";

        auto r = str::views::to_upper(s1);
        static_assert(is_view_v<decltype(r)>);

        BOOST_TEST(r == "FOO");
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        std::string_view s1;

        auto r = str::views::to_upper(s1);
        static_assert(is_view_v<decltype(r)>);

        BOOST_TEST(r.empty());
    }

BOOST_AUTO_TEST_SUITE_END() // to_upper_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
