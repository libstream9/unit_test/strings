#ifndef ALGORITHM_STRING_MAKE_HPP
#define ALGORITHM_STRING_MAKE_HPP

#include <string>
#include <string_view>
#include <vector>

namespace testing {

template<typename T>
T make(std::string_view const sv);

template<>
inline std::string_view
make<std::string_view>(std::string_view const sv)
{
    return sv;
}

template<typename T>
    requires requires (std::string_view sv) { T { sv.begin(), sv.end() }; }
inline T
make(std::string_view const sv)
{
    return { sv.begin(), sv.end() };
}

} // namespace testing

#endif // ALGORITHM_STRING_MAKE_HPP
