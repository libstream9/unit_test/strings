#include <stream9/strings/finder/last_finder.hpp>

#include <stream9/strings/accessor/begin.hpp>
#include <stream9/strings/accessor/end.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(last_finder_)

    BOOST_AUTO_TEST_CASE(char_match_)
    {
        str::last_finder find { 'B' };

        // literal / array
        char s1[] = "ABBC";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[2]));

        // pointer
        auto* const s2 = "ABBC";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.size() == 1);
        BOOST_TEST((&m2[0] == &s2[2]));

        // string
        std::string s3 = "ABBC";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 1);
        BOOST_TEST((&m3[0] == &s3[2]));
    }

    BOOST_AUTO_TEST_CASE(char_miss_)
    {
        str::last_finder find { 'X' };

        // literal / array
        char s1[] = "ABBC";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.empty());

        // pointer
        auto* const s2 = "ABBC";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.empty());

        // string
        std::string s3 = "ABBC";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.empty());
    }

    BOOST_AUTO_TEST_CASE(substring_match_)
    {
        str::last_finder find { "bar" };

        // literal / array
        char s1[] = "baz bar bar";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 3);
        BOOST_TEST((&m1[0] == &s1[8]));

        // pointer
        auto* const s2 = "baz bar bar";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.size() == 3);
        BOOST_TEST((&m2[0] == &s2[8]));

        // string
        std::string const s3 = "baz bar bar";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 3);
        BOOST_TEST((&m3[0] == &s3[8]));
    }

    BOOST_AUTO_TEST_CASE(substring_miss_)
    {
        str::last_finder find { "XXX" };

        // literal / array
        char s1[] = "baz bar bar";
        auto const& m1 = find(s1);

        BOOST_TEST(m1.empty());

        // pointer
        auto* const s2 = "baz bar bar";
        auto const& m2 = find(s2);

        BOOST_TEST(m2.empty());

        // string
        std::string const s3 = "baz bar bar";
        auto const& m3 = find(s3);

        BOOST_TEST(m3.empty());
    }

    BOOST_AUTO_TEST_CASE(classifier_match_)
    {
        str::is_space<char> pred;
        str::last_finder<char, decltype(pred)> find { pred };

        // literal / array
        char s1[] = "A B C";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[3]));

        // pointer
        std::string s2 = "A B C";
        auto const& m2 = find(s2.data());

        BOOST_REQUIRE(m2.size() == 1);
        BOOST_TEST((&m2[0] == &s2[3]));

        // string
        std::string const s3 = "A B C";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 1);
        BOOST_TEST((&m3[0] == &s3[3]));
    }

    BOOST_AUTO_TEST_CASE(classifier_miss_)
    {
        str::is_space<char> pred;
        str::last_finder<char, decltype(pred)> find { pred };

        // literal / array
        char s1[] = "AXB";
        auto const& m1 = find(s1);

        BOOST_TEST(m1.empty());

        // pointer
        std::string s2 = "AXB";
        auto const& m2 = find(s2.data());

        BOOST_TEST(m2.empty());

        // string
        std::string const s3 = "AXB";
        auto const& m3 = find(s3);

        BOOST_TEST(m3.empty());
    }

BOOST_AUTO_TEST_SUITE_END() // last_finder_

} // namespace testing
