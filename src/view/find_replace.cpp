#include <stream9/strings/view/find_replace.hpp>

#include <stream9/strings/finder/last_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(find_replace_)

    BOOST_AUTO_TEST_CASE(view_)
    {
        std::string s = "foo bar bar foo";
        auto v = str::views::find_replace(s, "bar", "baz");

        BOOST_TEST(v == "foo baz bar foo");
    }

    BOOST_AUTO_TEST_CASE(regex_)
    {
        std::string s = "foo bar bar foo";
        std::regex const e { "b\\w+" };

        auto v = str::views::find_replace(s, e, "baz");

        BOOST_TEST(v == "foo baz bar foo");
    }

    BOOST_AUTO_TEST_CASE(view_with_finder_)
    {
        std::string s = "foo bar bar foo";
        str::last_finder find { "bar" };
        auto v = str::views::find_replace(s, find, "baz");

        BOOST_TEST(v == "foo bar baz foo");
    }

    BOOST_AUTO_TEST_CASE(when_keyword_doesnt_exist_)
    {
        std::string s = "foo bar bar foo";
        auto v = str::views::find_replace(s, "X", "baz");

        BOOST_TEST(v == "foo bar bar foo");
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        std::string s = "";
        auto v = str::views::find_replace(s, "X", "baz");

        BOOST_TEST(v == "");
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        std::string s = "foo bar bar foo";
        auto v = str::views::find_replace(s, "", "baz");

        BOOST_TEST(v == "foo bar bar foo");
    }

    BOOST_AUTO_TEST_CASE(empty_3_)
    {
        std::string s = "foo bar bar foo";
        auto v = str::views::find_replace(s, "bar", "");

        BOOST_TEST(v == "foo  bar foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
