#include <stream9/strings/view/find_erase_all.hpp>

#include <stream9/strings/core/char/comparator.hpp>
#include <stream9/strings/finder/first_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(find_erase_all_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s = "foo bar bar xyzzy";

        auto const v = str::views::find_erase_all(s, "bar");

        BOOST_TEST(v == "foo   xyzzy");
    }

    BOOST_AUTO_TEST_CASE(regex_)
    {
        std::string const s = "foo bar bar xyzzy";
        std::regex const e { "b\\w+\\s*" };

        auto const v = str::views::find_erase_all(s, e);

        BOOST_TEST(v == "foo xyzzy");
    }

    BOOST_AUTO_TEST_CASE(finder_)
    {
        std::string s = "foo bar bar foo";
        str::first_finder find { "BAR", str::iequal_to<char>() };

        auto const v = str::views::find_erase_all(s, find);

        BOOST_TEST(v == "foo   foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_all_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
