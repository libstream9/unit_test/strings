#include <stream9/strings/view/trim_right.hpp>

#include <stream9/strings/core/char/classifier.hpp>

#include <string>
#include <string_view>
#include <type_traits>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(trim_right_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s1 = " foo ";
        auto r1 = str::views::trim_right(s1);
        static_assert(std::is_same_v<
            decltype(r1), str::string_range<std::string::const_iterator> >);
        BOOST_TEST(r1 == " foo");

        std::string const s2 = "...bar...";
        auto r2 = str::views::trim_right(s2, str::is_punct<char>());
        BOOST_TEST(r2 == "...bar");
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        std::string_view const s1 = " foo ";
        auto r1 = str::views::trim_right(s1);
        static_assert(std::is_same_v<
            decltype(r1), str::string_range<char const*> >);
        BOOST_TEST(r1 == " foo");

        std::string_view const s2 = "...bar...";
        auto r2 = str::views::trim_right(s2, str::is_punct<char>());
        BOOST_TEST(r2 == "...bar");
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string const s1 = " foo ";
        auto r1 = str::views::trim_right(s1.data());
        static_assert(std::is_same_v<
            decltype(r1), str::string_range<char const*> >);
        BOOST_TEST(r1 == " foo");

        std::string const s2 = "...bar...";
        auto r2 = str::views::trim_right(s2.data(), str::is_punct<char>());
        BOOST_TEST(r2 == "...bar");
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = " foo ";
        auto r1 = str::views::trim_right(s1);
        static_assert(std::is_same_v<
            decltype(r1), str::string_range<char*> >);
        BOOST_TEST(r1 == " foo");

        char s2[] = "...bar...";
        auto r2 = str::views::trim_right(s2, str::is_punct<char>());
        BOOST_TEST(r2 == "...bar");
    }

BOOST_AUTO_TEST_SUITE_END() // trim_right_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
