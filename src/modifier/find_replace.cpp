#include <stream9/strings/modifier/find_replace.hpp>

#include <stream9/strings/finder/last_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(find_replace_)

    template<typename S>
    concept invocable =
        requires (S&& s) {
            str::find_replace(s, s, s);
        };

    template<typename S>
    concept uninvocable = !invocable<S>;

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s = "foo bar bar xyzzy";

        str::find_replace(s, "bar", "baz");

        BOOST_TEST(s == "foo baz bar xyzzy");
    }

    BOOST_AUTO_TEST_CASE(finder_)
    {
        std::string s = "foo bar bar xyzzy";
        str::last_finder finder { "bar" };

        str::find_replace(s, finder, "baz");

        BOOST_TEST(s == "foo bar baz xyzzy");
    }

    BOOST_AUTO_TEST_CASE(regex_)
    {
        std::string s = "foo bar bar xyzzy";
        std::regex e { "bar" };

        str::find_replace(s, e, "baz");

        BOOST_TEST(s == "foo baz bar xyzzy");
    }

    BOOST_AUTO_TEST_CASE(when_keyword_doesnt_exist_)
    {
        std::string s = "foo bar bar xyzzy";

        str::find_replace(s, "XXX", "baz");

        BOOST_TEST(s == "foo bar bar xyzzy");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_

} // namespace testing
