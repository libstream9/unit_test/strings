#include <stream9/strings/finder/first_finder.hpp>

#include <stream9/strings/core/char/classifier.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(finder_)

BOOST_AUTO_TEST_SUITE(first_finder_)

    BOOST_AUTO_TEST_CASE(char_match_)
    {
        str::first_finder find { 'B' };

        // literal / array
        char const s1[] = "ABC";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[1]));

        // pointer
        auto* const s2 = "ABC";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.size() == 1);
        BOOST_TEST((&m2[0] == &s2[1]));

        // string
        std::string s3 = "ABC";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 1);
        BOOST_TEST((&m3[0] == &s3[1]));
    }

    BOOST_AUTO_TEST_CASE(char_miss_)
    {
        str::first_finder find { 'X' };

        // literal / array
        char s1[] = "ABC";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.empty());

        // pointer
        auto* const s2 = "ABC";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.empty());

        // string
        std::string s3 = "ABC";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.empty());
    }

    BOOST_AUTO_TEST_CASE(substring_match_)
    {
        str::first_finder find { "bar" };

        // literal / array
        char s1[] = "baz bar bar";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 3);
        BOOST_TEST((&m1[0] == &s1[4]));

        // pointer
        auto* const s2 = "baz bar bar";
        auto const& m2 = find(s2);

        BOOST_REQUIRE(m2.size() == 3);
        BOOST_TEST((&m2[0] == &s2[4]));

        // string
        std::string const s3 = "baz bar bar";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 3);
        BOOST_TEST((&m3[0] == &s3[4]));
    }

    BOOST_AUTO_TEST_CASE(substring_miss_)
    {
        str::first_finder find { "XXX" };

        // literal / array
        char s1[] = "baz bar bar";
        auto const& m1 = find(s1);

        BOOST_TEST(m1.empty());

        // pointer
        auto* const s2 = "baz bar bar";
        auto const& m2 = find(s2);

        BOOST_TEST(m2.empty());

        // string
        std::string const s3 = "baz bar bar";
        auto const& m3 = find(s3);

        BOOST_TEST(m3.empty());
    }

    BOOST_AUTO_TEST_CASE(classifier_match_)
    {
        str::is_space<char> pred;
        str::first_finder find { pred };

        // literal / array
        char s1[] = "A B";
        auto const& m1 = find(s1);

        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[1]));

        // pointer
        std::string s2 = "A B";
        auto const& m2 = find(s2.data());

        BOOST_REQUIRE(m2.size() == 1);
        BOOST_TEST((&m2[0] == &s2[1]));

        // string
        std::string const s3 = "A B";
        auto const& m3 = find(s3);

        BOOST_REQUIRE(m3.size() == 1);
        BOOST_TEST((&m3[0] == &s3[1]));
    }

    static bool mypred(char c)
    {
        return c == ' ';
    }

    BOOST_AUTO_TEST_CASE(classifier_miss_)
    {
        str::first_finder find { mypred };

        // literal / array
        char s1[] = "AXB";
        auto const& m1 = find(s1);

        BOOST_TEST(m1.empty());

        // pointer
        std::string s2 = "AXB";
        auto const& m2 = find(s2.data());

        BOOST_TEST(m2.empty());

        // string
        std::string const s3 = "AXB";
        auto const& m3 = find(s3);

        BOOST_TEST(m3.empty());
    }

BOOST_AUTO_TEST_SUITE_END() // first_finder_

BOOST_AUTO_TEST_SUITE_END() // finder_

} // namespace testing
