#include <stream9/strings/string.hpp>

#include "namespace.hpp"

#include <stream9/json.hpp>

#include <ranges>
#include <sstream>
#include <utility>

#include <boost/test/unit_test.hpp>

namespace testing {

using string = st9::basic_string<char>;
using string_view = std::string_view;

BOOST_AUTO_TEST_SUITE(basic_string_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            string s;

            BOOST_TEST(s.empty());
        }

        BOOST_AUTO_TEST_CASE(char_)
        {
            string s = 'x';

            BOOST_TEST(s == "x");
        }

        BOOST_AUTO_TEST_CASE(string_literal_)
        {
            string s = "foo";

            BOOST_TEST(s == "foo");
        }

        BOOST_AUTO_TEST_CASE(sv_copy_)
        {
            string_view sv = "foo";
            string s = sv;

            BOOST_TEST(s == "foo");
        }

        BOOST_AUTO_TEST_CASE(copy_)
        {
            string s1 = "foo";
            string s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            string s1 = "foo";
            string s2 = std::move(s1);

            BOOST_TEST(s2 == "foo");
            BOOST_TEST(s1.empty());
        }

        BOOST_AUTO_TEST_CASE(std_string_move_)
        {
            std::string s1 = "foo";
            string s2 = std::move(s1);

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(iterator_pair_)
        {
            string_view s1 = "foo";

            string s2 { s1.begin(), s1.end() };

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(iterator_and_length_)
        {
            string_view s1 = "foo";

            string s2 { s1.begin(), s1.size() };

            BOOST_TEST(s2 == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(assignment_)

        BOOST_AUTO_TEST_CASE(char_)
        {
            string s;
            s = 'x';

            BOOST_TEST(s == "x");
        }

        BOOST_AUTO_TEST_CASE(literal_)
        {
            string s;
            s = "foo";

            BOOST_TEST(s == "foo");
        }

        BOOST_AUTO_TEST_CASE(sv_copy_)
        {
            string s;
            string_view sv = "foo";
            s = sv;

            BOOST_TEST(s == "foo");
        }

        BOOST_AUTO_TEST_CASE(copy_)
        {
            string s1 = "foo";
            string s2;
            s2 = s1;

            BOOST_TEST(s2 == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            string s1 = "foo";
            string s2;
            s2 = std::move(s1);

            BOOST_TEST(s2 == "foo");
            BOOST_TEST(s1.empty());
        }

        BOOST_AUTO_TEST_CASE(std_string_move_)
        {
            std::string s1 = "foo";
            string s2;
            s2 = std::move(s1);

            BOOST_TEST(s2 == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // assignment_

    BOOST_AUTO_TEST_CASE(swap_)
    {
        string s1 = "foo";
        string s2 = "bar";

        using std::swap;
        swap(s1, s2);

        BOOST_TEST(s1 == "bar");
        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_SUITE(conversion_)

        BOOST_AUTO_TEST_CASE(string_view_)
        {
            string s1 = "foo";
            string_view sv = s1;

            BOOST_TEST(sv == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_to_std_string_)
        {
            string s1 = "foo";
            std::string s2 = std::move(s1);

            BOOST_TEST(s1.empty());
            BOOST_TEST(s2 == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // conversion_

    BOOST_AUTO_TEST_SUITE(begin_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            string s1 = "foo";

            auto i = s1.begin();

            BOOST_TEST(*i == 'f');
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            string s1;

            auto i = s1.begin();

            BOOST_TEST(*i == '\0');
        }

        BOOST_AUTO_TEST_CASE(const_)
        {
            string const s1 = "foo";

            auto i = s1.begin();

            BOOST_TEST(*i == 'f');
        }

        BOOST_AUTO_TEST_CASE(const_empty_)
        {
            string const s1;

            auto i = s1.begin();

            BOOST_TEST(*i == '\0');
        }

    BOOST_AUTO_TEST_SUITE_END() // begin_

    BOOST_AUTO_TEST_SUITE(end_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            string s1 = "foo";

            auto i = s1.end();

            BOOST_TEST(*i == '\0');
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            string s1;

            auto i = s1.end();

            BOOST_TEST(*i == '\0');
        }

        BOOST_AUTO_TEST_CASE(const_)
        {
            string const s1 = "foo";

            auto i = s1.end();

            BOOST_TEST(*i == '\0');
        }

        BOOST_AUTO_TEST_CASE(const_empty_)
        {
            string const s1;

            auto i = s1.end();

            BOOST_TEST(*i == '\0');
        }

    BOOST_AUTO_TEST_SUITE_END() // end_

    BOOST_AUTO_TEST_SUITE(at_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            string s1 = "xyz";

            BOOST_TEST(s1[0] == 'x');
            BOOST_TEST(s1[1] == 'y');
            BOOST_TEST(s1[2] == 'z');
        }

        BOOST_AUTO_TEST_CASE(const_)
        {
            string const s1 = "xyz";

            BOOST_TEST(s1[0] == 'x');
            BOOST_TEST(s1[1] == 'y');
            BOOST_TEST(s1[2] == 'z');
        }

    BOOST_AUTO_TEST_SUITE_END() // at_

    BOOST_AUTO_TEST_SUITE(c_str_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            string s1 = "xyz";

            BOOST_TEST(s1.c_str() == s1.data());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            string s1;

            BOOST_TEST(s1.c_str() != nullptr);
            BOOST_TEST(s1.c_str() == s1.data());
        }

    BOOST_AUTO_TEST_SUITE_END() // c_str_

    BOOST_AUTO_TEST_SUITE(data_)

        BOOST_AUTO_TEST_CASE(basic_)
        {
            string s1;

            BOOST_TEST(s1.data() != nullptr);
        }

        BOOST_AUTO_TEST_CASE(const_)
        {
            string const s1;

            BOOST_TEST(s1.data() != nullptr);
        }

    BOOST_AUTO_TEST_SUITE_END() // data_

    BOOST_AUTO_TEST_SUITE(natural_insert_)

        BOOST_AUTO_TEST_CASE(char_)
        {
            string s1 = "123";

            auto i = s1.insert('x');

            BOOST_TEST(s1 == "123x");
            BOOST_TEST(*i == 'x');
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            string s1 = "123";

            auto s2 = s1.insert("xyz");

            BOOST_TEST(s1 == "123xyz");
            BOOST_TEST(s2 == "xyz");
        }

    BOOST_AUTO_TEST_SUITE_END() // natural_insert_

    BOOST_AUTO_TEST_SUITE(positional_insert_)

        BOOST_AUTO_TEST_CASE(char_literal_)
        {
            string s1;

            s1.insert(s1.begin(), 'x');

            BOOST_TEST(s1 == "x");
        }

        BOOST_AUTO_TEST_CASE(string_literal_)
        {
            string s1;

            s1.insert(s1.begin(), "xyz");

            BOOST_TEST(s1 == "xyz");
        }

    BOOST_AUTO_TEST_SUITE_END() // positional_insert_

    BOOST_AUTO_TEST_SUITE(append_)

        BOOST_AUTO_TEST_CASE(char_)
        {
            string s1 = "123";

            auto i = s1.append('x');

            BOOST_TEST(s1 == "123x");
            BOOST_TEST(*i == 'x');
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            string s1 = "123";

            auto s2 = s1.append("xyz");

            BOOST_TEST(s1 == "123xyz");
            BOOST_TEST(s2 == "xyz");
        }

    BOOST_AUTO_TEST_SUITE_END() // append_

    BOOST_AUTO_TEST_SUITE(prepend_)

        BOOST_AUTO_TEST_CASE(char_)
        {
            string s1 = "123";

            auto i = s1.prepend('x');

            BOOST_TEST(s1 == "x123");
            BOOST_TEST(*i == 'x');
        }

        BOOST_AUTO_TEST_CASE(string_)
        {
            string s1 = "123";

            auto s2 = s1.prepend("xyz");

            BOOST_TEST(s1 == "xyz123");
            BOOST_TEST(s2 == "xyz");
        }

    BOOST_AUTO_TEST_SUITE_END() // prepend_

    BOOST_AUTO_TEST_SUITE(erase_)

        BOOST_AUTO_TEST_CASE(with_pos_)
        {
            string s1 = "12345";

            auto i1 = s1.erase(s1.begin());

            BOOST_REQUIRE(i1 != s1.end());
            BOOST_TEST(*i1 == '2');
            BOOST_TEST(s1 == "2345");
        }

        BOOST_AUTO_TEST_CASE(with_iterator_pair_)
        {
            string s1 = "12345";

            auto i1 = s1.erase(s1.begin(), s1.begin() + 2);

            BOOST_REQUIRE(i1 != s1.end());
            BOOST_TEST(*i1 == '3');
            BOOST_TEST(s1 == "345");
        }

    BOOST_AUTO_TEST_SUITE_END() // erase_

    BOOST_AUTO_TEST_SUITE(remove_prefix_)

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            string s1;

            auto n = s1.remove_prefix(5);

            BOOST_TEST(n == 0);
            BOOST_TEST(s1 == "");
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            string s1 = "12345";

            auto n = s1.remove_prefix(3);

            BOOST_TEST(n == 3);
            BOOST_TEST(s1 == "45");
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            string s1 = "12345";

            auto n = s1.remove_prefix(6);

            BOOST_TEST(n == 5);
            BOOST_TEST(s1 == "");
        }

    BOOST_AUTO_TEST_SUITE_END() // remove_prefix_

    BOOST_AUTO_TEST_SUITE(remove_suffix_)

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            string s1;

            auto n = s1.remove_suffix(5);

            BOOST_TEST(n == 0);
            BOOST_TEST(s1 == "");
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            string s1 = "12345";

            auto n = s1.remove_suffix(3);

            BOOST_TEST(n == 3);
            BOOST_TEST(s1 == "12");
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            string s1 = "12345";

            auto n = s1.remove_suffix(6);

            BOOST_TEST(n == 5);
            BOOST_TEST(s1 == "");
        }

    BOOST_AUTO_TEST_SUITE_END() // remove_suffix_

    BOOST_AUTO_TEST_CASE(clear_)
    {
        string s1 = "12345";

        s1.clear();

        BOOST_TEST(s1.empty());
    }

    BOOST_AUTO_TEST_SUITE(resize_)

        BOOST_AUTO_TEST_CASE(with_size_)
        {
            string s1;

            BOOST_TEST(s1.empty());

            s1.resize(5);

            BOOST_TEST(s1.size() == 5);
            BOOST_TEST(s1[0] == '\0');
        }

        BOOST_AUTO_TEST_CASE(with_size_and_char_)
        {
            string s1;

            BOOST_TEST(s1.empty());

            s1.resize(5, '0');

            BOOST_TEST(s1 == "00000");
        }

    BOOST_AUTO_TEST_SUITE_END() // resize_

    BOOST_AUTO_TEST_CASE(reserve_)
    {
        string s1;

        BOOST_TEST(s1.capacity() != 50);

        s1.reserve(50);
        BOOST_TEST(s1.capacity() == 50);
    }

    BOOST_AUTO_TEST_CASE(shrink_to_fit_)
    {
        string s1 = "1234567890";

        s1.shrink_to_fit();
    }

    BOOST_AUTO_TEST_CASE(append_char_)
    {
        string s1 = "123";

        s1 += '4';

        BOOST_TEST(s1 == "1234");
    }

    BOOST_AUTO_TEST_CASE(append_string_)
    {
        string s1 = "123";

        s1 += "456";

        BOOST_TEST(s1 == "123456");
    }

    BOOST_AUTO_TEST_SUITE(concat_)

        BOOST_AUTO_TEST_CASE(str_char_)
        {
            string s1 = "123";

            auto s2 = s1 + '4';

            BOOST_TEST(s2 == "1234");
        }

        BOOST_AUTO_TEST_CASE(char_str_)
        {
            string s1 = "123";

            auto s2 = '4' + s1;

            BOOST_TEST(s2 == "4123");
        }

        BOOST_AUTO_TEST_CASE(str_str_)
        {
            string s1 = "123";

            auto s2 = s1 + "456";

            BOOST_TEST(s2 == "123456");
        }

    BOOST_AUTO_TEST_SUITE_END() // concat_

    BOOST_AUTO_TEST_CASE(conversion_to_string_view_)
    {
        string s1 = "123";

        string_view sv = s1;

        BOOST_TEST(sv == "123");
    }

    BOOST_AUTO_TEST_CASE(equal_1_)
    {
        string s1 = "foo";
        string s2 = "bar";

        BOOST_CHECK((s1 != s2));
    }

    BOOST_AUTO_TEST_CASE(equal_2_)
    {
        string s1 = "foo";

        BOOST_CHECK((s1 == "foo"));
    }

    BOOST_AUTO_TEST_CASE(equal_3_)
    {
        string s1 = "foo";

        BOOST_CHECK(("bar" != s1));
    }

    BOOST_AUTO_TEST_SUITE(three_way_)

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            string s1 = "123";
            string s2 = "1234";

            BOOST_CHECK((s1 < s2));
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            string s1 = "123";

            BOOST_CHECK((s1 < "1234"));
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            string s1 = "123";

            BOOST_CHECK(("12" < s1));
        }

    BOOST_AUTO_TEST_SUITE_END() // three_way_

    BOOST_AUTO_TEST_CASE(ostream_)
    {
        string s1 = "foo";
        std::ostringstream os;

        os << s1;

        BOOST_TEST(os.str() == "foo");
    }

    BOOST_AUTO_TEST_CASE(istream_)
    {
        string s1;
        s1.resize(10);
        std::stringstream ss;

        ss << "foo";
        ss.seekg(0);
        ss >> s1;

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(stream_out_)
    {
        string s1;

        s1 << "abc" << 123 << "def";

        BOOST_TEST(s1 == "abc123def");
    }

    BOOST_AUTO_TEST_CASE(stream_in_)
    {
        string s1 = "abc";
        std::string s2;

        s1 >> s2;

        BOOST_TEST(s2 == "abc");
        BOOST_TEST(s1 == "abc");
    }

    BOOST_AUTO_TEST_CASE(structured_binding_)
    {
        string s1 = "12345";

        auto [i, e] = s1;

        BOOST_TEST(i == s1.begin());
        BOOST_TEST(e == s1.end());
    }

BOOST_AUTO_TEST_SUITE_END() // basic_string_

} // namespace testing
