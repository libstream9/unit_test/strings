#include <stream9/strings/utility/cstring_view.hpp>

#include "../namespace.hpp"

#include <filesystem>
#include <ranges>
#include <sstream>
#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>
#include <stream9/strings/core/concepts.hpp>
#include <stream9/optional.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(cstring_view_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(rng::contiguous_range<str::cstring_view>);
        static_assert(rng::borrowed_range<str::cstring_view>);
        static_assert(rng::view<str::cstring_view>);

        static_assert(str::contiguous_string<str::cstring_view>);

        using const_iterator = str::cstring_view::const_iterator;
        static_assert(std::contiguous_iterator<const_iterator>);
        static_assert(std::sized_sentinel_for<const_iterator, const_iterator>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_literal_)
    {
        str::cstring_view v { "foo" };
    }

    BOOST_AUTO_TEST_CASE(construct_from_array_)
    {
        char s[] = "foo";
        str::cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_const_array_)
    {
        char const s[] = "foo";
        str::cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_pointer_)
    {
        char* const s = const_cast<char*>("foo");
        str::cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_const_pointer_)
    {
        char const* s = "foo";
        str::cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_view_)
    {
        std::string_view sv = "foo";
        str::cstring_view v { sv };

        BOOST_TEST(v == sv);
        static_assert(!std::convertible_to<std::string_view, str::cstring_view>);
    }

    BOOST_AUTO_TEST_CASE(construct_from_string_)
    {
        std::string const s = "12345";
        str::cstring_view v { s };
    }

    BOOST_AUTO_TEST_CASE(construct_from_std_path_)
    {
        std::filesystem::path p = "/tmp";
        str::cstring_view v { p };
    }

    BOOST_AUTO_TEST_CASE(construction_error_1_)
    {
        char const* p = nullptr;

        BOOST_CHECK_EXCEPTION(
            st9::cstring_view(p, 10),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_pointer);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_2_)
    {
        char const* f = nullptr;
        char const* l = nullptr;

        BOOST_CHECK_EXCEPTION(
            st9::cstring_view(f, l),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_pointer);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_3_)
    {
        char const* f = "foo";
        char const* l = nullptr;

        BOOST_CHECK_EXCEPTION(
            st9::cstring_view(f, l),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_pointer);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_4_)
    {
        std::string_view s = "foo";

        BOOST_CHECK_EXCEPTION(
            st9::cstring_view(s.end(), s.begin()),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_range);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_5_)
    {
        std::string_view s = "foo";

        BOOST_CHECK_EXCEPTION(
            st9::cstring_view(s.begin(), s.begin()+1),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::not_terminated_with_null);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_6_)
    {
        char const* p = nullptr;

        BOOST_CHECK_EXCEPTION(
            [&]{st9::cstring_view s(p);}(),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_pointer);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_7_)
    {
        std::string_view s;

        BOOST_CHECK_EXCEPTION(
            [&]{st9::cstring_view v(s);}(),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::invalid_pointer);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(construction_error_8_)
    {
        std::string_view s1 = "foo";
        auto s2 = s1.substr(0,1);

        BOOST_CHECK_EXCEPTION(
            [&]{st9::cstring_view v(s2);}(),
            st9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == str::errc::not_terminated_with_null);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(begin_)
    {
        str::cstring_view v { "123" };

        auto it = v.begin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(cbegin_)
    {
        str::cstring_view v { "123" };

        auto it = v.cbegin();
        BOOST_TEST(*it == '1');
    }

    BOOST_AUTO_TEST_CASE(end_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(v.end() == v.begin() + 3);
    }

    BOOST_AUTO_TEST_CASE(cend_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(v.cend() == v.cbegin() + 3);
    }

    BOOST_AUTO_TEST_CASE(bracket_1_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(v[1] == '2');
    }

    BOOST_AUTO_TEST_CASE(bracket_2_)
    {
        str::cstring_view v { "123" };

        st9::safe_integer<int, 1> i = 1;

        BOOST_TEST(v[i] == '2');
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(v.front() == '1');
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        char const s[] = "123";
        str::cstring_view v { s };

        BOOST_TEST(v.data() == s);
    }

    BOOST_AUTO_TEST_CASE(c_str_)
    {
        char const s[] = "123";
        str::cstring_view v { s };

        BOOST_TEST(v.c_str() == s);
    }

    BOOST_AUTO_TEST_CASE(size_empty_)
    {
        str::cstring_view v { "" };

        BOOST_TEST(v.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(v.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_empty_)
    {
        str::cstring_view v { "" };

        BOOST_TEST(v.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        str::cstring_view v { "123" };

        BOOST_TEST(!v.empty());
    }

    BOOST_AUTO_TEST_CASE(remove_prefix_)
    {
        auto const s = "12345";
        str::cstring_view v { s };

        v.remove_prefix(2);

        BOOST_TEST(v.data() == &s[2]);
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        str::cstring_view v1 { "123" };
        str::cstring_view v2 { "123" };

        BOOST_TEST(v1 == v2);
    }

    BOOST_AUTO_TEST_CASE(equal_with_literal_)
    {
        str::cstring_view v1 { "123" };

        BOOST_CHECK(v1 == "123");
        BOOST_CHECK("123" == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_array_)
    {
        str::cstring_view v1 { "123" };
        char s2[] = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_pointer_)
    {
        str::cstring_view v1 { "123" };
        auto s2 = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_view_)
    {
        str::cstring_view v1 { "123" };

        std::string s2 { "12345" };
        auto v2 = std::string_view(s2).substr(0, 3);

        BOOST_CHECK(v1 == v2);
        BOOST_CHECK(v2 == v1);
    }

    BOOST_AUTO_TEST_CASE(equal_with_string_)
    {
        str::cstring_view v1 { "123" };
        std::string s2 = "123";

        BOOST_CHECK(v1 == s2);
        BOOST_CHECK(s2 == v1);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        str::cstring_view v1 { "123" };
        str::cstring_view v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_)
    {
        str::cstring_view v1 { "123" };
        str::cstring_view v2 { "" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::greater);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v2 == std::strong_ordering::equivalent);
    }

    BOOST_AUTO_TEST_CASE(compare_with_literal_)
    {
        str::cstring_view v1 { "123" };

        BOOST_CHECK(v1 <=> "124" == std::strong_ordering::less);
        BOOST_CHECK("124" <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_literal_)
    {
        str::cstring_view v1 { "123" };

        BOOST_CHECK(v1 <=> "" == std::strong_ordering::greater);
        BOOST_CHECK("" <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_array_)
    {
        str::cstring_view v1 { "123" };

        char s2[] = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_array_)
    {
        str::cstring_view v1 { "123" };

        char s2[] = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_)
    {
        str::cstring_view v1 { "123" };

        auto const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_pointer_to_empty_)
    {
        str::cstring_view v1 { "123" };

        auto const s2 = "";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(compare_string_view_)
    {
        str::cstring_view v1 { "123" };

        std::string_view v2 { "124" };

        BOOST_CHECK(v1 <=> v2 == std::strong_ordering::less);
        BOOST_CHECK(v2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_string_)
    {
        str::cstring_view v1 { "123" };

        std::string const s2 = "124";

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::less);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::greater);
    }

    BOOST_AUTO_TEST_CASE(compare_with_empty_string_)
    {
        str::cstring_view v1 { "123" };

        std::string const s2;

        BOOST_CHECK(v1 <=> s2 == std::strong_ordering::greater);
        BOOST_CHECK(s2 <=> v1 == std::strong_ordering::less);
    }

    BOOST_AUTO_TEST_CASE(convert_to_pointer_)
    {
        str::cstring_view v1 { "123" };

        char const* v2 = v1;

        BOOST_CHECK(v1 == v2);
        static_assert(std::convertible_to<str::cstring_view, char const*>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view_)
    {
        str::cstring_view v1 { "123" };

        std::string_view v2 = v1;

        BOOST_CHECK(v1 == v2);
        static_assert(std::convertible_to<str::cstring_view, std::string_view>);
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_)
    {
        str::cstring_view v1 { "123" };

        std::string s2 { v1 };

        BOOST_CHECK(v1 == s2);
        static_assert(!std::convertible_to<str::cstring_view, std::string>);
    }

    BOOST_AUTO_TEST_CASE(stream_)
    {
        str::cstring_view v1 { "123" };
        std::ostringstream oss;

        oss << v1;

        BOOST_TEST(oss.str() == "123");
    }

    BOOST_AUTO_TEST_CASE(constexpr_)
    {
        constexpr str::cstring_view v { "foo" };
    }

    BOOST_AUTO_TEST_CASE(optional_1_)
    {
        using stream9::opt;

        opt<str::cstring_view> o_s1;
        static_assert(sizeof(o_s1) == sizeof(str::cstring_view));

        BOOST_TEST(!o_s1);

        o_s1 = "foo";
        BOOST_REQUIRE(o_s1);
        BOOST_TEST(*o_s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // cstring_view_

} // namespace testing
