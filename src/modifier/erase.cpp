#include <stream9/strings/modifier/erase.hpp>

#include <stream9/strings/accessor/begin.hpp>
#include <stream9/strings/accessor/end.hpp>

#include "../hack.hpp"
#include "../make.hpp"

#include <string>
#include <vector>
#include <tuple>

#include <boost/test/unit_test.hpp>

namespace testing {

//TODO non member

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(primitive_)

BOOST_AUTO_TEST_SUITE(erase_)

    template<typename S>
    concept can_erase_single =
        requires (S&& s) {
            str::erase(s, str::begin(s));
        };

    template<typename S>
    concept can_erase_range_by_idx =
        requires (S&& s) {
            str::erase(s, 0, 1);
        };

    template<typename S>
    concept can_erase_range_by_iter_pair =
        requires (S&& s) {
            str::erase(s, str::begin(s), str::end(s));
        };

    template<typename S>
    concept invocable =
           can_erase_single<S>
        && can_erase_range_by_idx<S>
        && can_erase_range_by_iter_pair<S>;

    template<typename S>
    concept uninvocable =
           (!can_erase_single<S>)
        && (!can_erase_range_by_idx<S>)
        && (!can_erase_range_by_iter_pair<S>);

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    using output_types = std::tuple<std::string, std::vector<char>>;

    BOOST_AUTO_TEST_CASE_TEMPLATE(by_index, StringT, output_types)
    {
        auto s = make<StringT>("123");

        str::erase(s, 1u, 1u);

        BOOST_TEST(s == "13");
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(by_iterator, StringT, output_types)
    {
        auto s = make<StringT>("123");

        auto it = str::erase(s, str::begin(s));

        BOOST_TEST(s == "23");
        BOOST_TEST((it == str::begin(s)));
    }

    BOOST_AUTO_TEST_CASE_TEMPLATE(two_iterator, StringT, output_types)
    {
        auto s = make<StringT>("123");

        auto it = str::erase(s, s.begin(), s.end());

        BOOST_TEST(s.empty());
        BOOST_TEST((it == str::end(s)));
    }

BOOST_AUTO_TEST_SUITE_END() // erase_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
