#include <stream9/strings/query/find_first.hpp>

#include <stream9/strings/core/string_range.hpp>
#include <stream9/strings/finder/last_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <algorithm>
#include <forward_list>
#include <functional>
#include <iterator>
#include <list>
#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(find_first_)

    BOOST_AUTO_TEST_CASE(by_character_)
    {
        // literal / array
        auto const s1 = "ABBC";
        auto const r1 = str::find_first(s1, 'B');
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[1]));

        auto const r1x = str::find_first(s1, 'X');
        BOOST_REQUIRE(r1x.empty());

        // pointer
        auto* const s2 = "ABBC";
        auto const r2 = str::find_first(s2, 'B');
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[1]));

        auto const r2x = str::find_first(s2, 'X');
        BOOST_REQUIRE(r2x.empty());

        // string (random access range)
        std::string s3 = "ABBC";
        auto const r3 = str::find_first(s3, 'B');
        BOOST_REQUIRE(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[1]));

        auto const r3x = str::find_first(s2, 'X');
        BOOST_REQUIRE(r3x.empty());

        // list (bidirectional range)
        std::list<char> const s4 = { 'A', 'B', 'B', 'C' };
        auto const r4 = str::find_first(s4, 'B');
        BOOST_REQUIRE(!r4.empty());
        BOOST_TEST((r4.begin() == rng::next(str::begin(s4), 1)));

        auto const r4x = str::find_first(s2, 'X');
        BOOST_REQUIRE(r4x.empty());

        // forward_list (forward range)
        std::forward_list<char> const s5 = { 'A', 'B', 'B', 'C' };
        auto const r5 = str::find_first(s5, 'B');
        BOOST_REQUIRE(!r5.empty());
        BOOST_TEST((r5.begin() == rng::next(str::begin(s5), 1)));

        auto const r5x = str::find_first(s2, 'X');
        BOOST_REQUIRE(r5x.empty());
    }

    BOOST_AUTO_TEST_CASE(by_string_)
    {
        std::string const s = "foo bar";

        // literal / array
        auto const r1 = str::find_first(s, "bar");
        BOOST_TEST(std::distance(s.begin(), r1.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r1.end()) == 7);

        // pointer
        auto* const s2 = "bar";
        auto const r2 = str::find_first(s, s2);
        BOOST_TEST(std::distance(s.begin(), r2.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r2.end()) == 7);

        // string
        std::string const s3 = "bar";
        auto const r3 = str::find_first(s, s3);
        BOOST_TEST(std::distance(s.begin(), r3.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r3.end()) == 7);

        // string_view
        std::string_view const s4 = "bar";
        auto const r4 = str::find_first(s, s4);
        BOOST_TEST(std::distance(s.begin(), r4.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r4.end()) == 7);

        // vector (random access)
        std::vector<char> const s5 = { 'b', 'a', 'r' };
        auto const r5 = str::find_first(s, s5);
        BOOST_TEST(std::distance(s.begin(), r5.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r5.end()) == 7);

        // list (bidirectional)
        std::list<char> const s6 = { 'b', 'a', 'r' };
        auto const r6 = str::find_first(s, s6);
        BOOST_TEST(std::distance(s.begin(), r6.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r6.end()) == 7);

        // forward_list (forward)
        std::forward_list<char> const s7 = { 'b', 'a', 'r' };
        auto const r7 = str::find_first(s, s7);
        BOOST_TEST(std::distance(s.begin(), r7.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r7.end()) == 7);
    }

    BOOST_AUTO_TEST_CASE(by_classifier_)
    {
        str::is_punct<char> fn;

        // literal / array
        auto const s1 = "A..B";
        auto const r1 = str::find_first(s1, fn);
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[1]));

        // pointer
        auto* const s2 = "A..B";
        auto const r2 = str::find_first(s2, fn);
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[1]));

        // string
        std::string s3 = "A..B";
        auto const r3 = str::find_first(s3, fn);
        BOOST_REQUIRE(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[1]));

        // list (bidirectional range)
        std::list<char> const s4 = { 'A', '.', '.', 'B' };
        auto const r4 = str::find_first(s4, fn);
        BOOST_REQUIRE(!r4.empty());
        BOOST_TEST((r4.begin() == rng::next(str::begin(s4), 1)));

        // forward_list (forward range)
        std::forward_list<char> const s5 = { 'A', '.', '.', 'B' };
        auto const r5 = str::find_first(s5, fn);
        BOOST_REQUIRE(!r5.empty());
        BOOST_TEST((r5.begin() == rng::next(str::begin(s5), 1)));
    }

    BOOST_AUTO_TEST_CASE(by_regex_)
    {
        std::regex const re { "o" };

        // literal / array
        auto const s1 = "foobar";
        auto const m1 = str::find_first(s1, re);
        BOOST_REQUIRE(m1.size() == 1);
        BOOST_TEST((&m1[0] == &s1[1]));

        // pointer
        auto* const s2 = "foobar";
        auto const m2 = str::find_first(s2, re);
        BOOST_REQUIRE(m2.size() == 1);
        BOOST_TEST((&m2[0] == &s2[1]));

        // string
        std::string const s3 = "foobar";
        auto const m3 = str::find_first(s3, re);
        BOOST_REQUIRE(m3.size() == 1);
        BOOST_TEST((&m3[0] == &s3[1]));

        // list (bidirectional range)
        std::list<char> const s4 = { 'f', 'o', 'o', 'b', 'a', 'r' };
        auto const m4 = str::find_first(s4, re);
        BOOST_REQUIRE(!m4.empty());
        BOOST_TEST((m4.begin() == rng::next(str::begin(s4), 1)));
    }

    BOOST_AUTO_TEST_CASE(by_finder_)
    {
        str::last_finder fn { 'B' };

        // literal / array
        auto const s1 = "ABBA";
        auto const r1 = str::find_first(s1, fn);
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[2]));

        // pointer
        auto* const s2 = "ABBA";
        auto const r2 = str::find_first(s2, fn);
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[2]));

        // string
        std::string const s3 = "ABBA";
        auto const r3 = str::find_first(s3, fn);
        BOOST_REQUIRE(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[2]));

        // list (bidirectional range)
        std::list<char> const s4 = { 'A', 'B', 'B', 'A' };
        auto const r4 = str::find_first(s4, fn);
        BOOST_REQUIRE(!r4.empty());
        BOOST_TEST((r4.begin() == rng::next(str::begin(s4), 2)));
    }

    BOOST_AUTO_TEST_CASE(searcher_)
    {
        std::string const s1 = "foo bar";
        std::string const s2 = "bar";

        std::boyer_moore_searcher searcher { s2.begin(), s2.end() };

        auto finder = [searcher](str::string auto&& s) {
            auto [first, last] = searcher(str::begin(s), str::end(s));
            return str::string_range { first, last };
        };

        auto const r = str::find_first(s1, finder);

        BOOST_TEST(std::distance(s1.begin(), r.begin()) == 4);
        BOOST_TEST(std::distance(s1.begin(), r.end()) == 7);
    }

BOOST_AUTO_TEST_SUITE_END() // find_first_

BOOST_AUTO_TEST_SUITE(ifind_first_)

    BOOST_AUTO_TEST_CASE(char_pointers)
    {
        // literal
        auto s1 = "ABC";
        auto const r1 = str::ifind_first(s1, 'b');
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s1[1]));

        // pointer
        auto* const s2 = "abc";
        auto const r2 = str::ifind_first(s2, 'B');
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s2[1]));

        // array
        char s3[] = "ABC";
        auto const r3 = str::ifind_first(s3, 'b');
        BOOST_REQUIRE(r3.size() == 1);
        BOOST_TEST((&r3[0] == &s3[1]));

        // string
        std::string s4 = "abc";
        auto const r4 = str::ifind_first(s4, 'B');
        BOOST_REQUIRE(r4.size() == 1);
        BOOST_TEST((&r4[0] == &s4[1]));

        // list (bidirectional range)
        std::list<char> const s5 = { 'A', 'B', 'C' };
        auto const r5 = str::ifind_first(s5, 'b');
        BOOST_REQUIRE(!r5.empty());
        BOOST_TEST((r5.begin() == rng::next(str::begin(s5), 1)));

        // forward_list (forward range)
        std::forward_list<char> const s6 = { 'a', 'b', 'c' };
        auto const r6 = str::ifind_first(s6, 'B');
        BOOST_REQUIRE(!r6.empty());
        BOOST_TEST((r6.begin() == rng::next(str::begin(s6), 1)));
    }

    BOOST_AUTO_TEST_CASE(substring_)
    {
        std::string const s = "foo bar";

        // literal / array
        auto const r1 = str::ifind_first(s, "bar");
        BOOST_TEST(std::distance(s.begin(), r1.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r1.end()) == 7);

        // pointer
        auto* const s2 = "bar";
        auto const r2 = str::ifind_first(s, s2);
        BOOST_TEST(std::distance(s.begin(), r2.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r2.end()) == 7);

        // string
        std::string const s3 = "bar";
        auto const r3 = str::ifind_first(s, s3);
        BOOST_TEST(std::distance(s.begin(), r3.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r3.end()) == 7);

        // string_view
        std::string_view const s4 = "bar";
        auto const r4 = str::ifind_first(s, s4);
        BOOST_TEST(std::distance(s.begin(), r4.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r4.end()) == 7);

        // vector (random access)
        std::vector<char> const s5 = { 'b', 'a', 'r' };
        auto const r5 = str::ifind_first(s, s5);
        BOOST_TEST(std::distance(s.begin(), r5.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r5.end()) == 7);

        // list (bidirectional)
        std::list<char> const s6 = { 'b', 'a', 'r' };
        auto const r6 = str::ifind_first(s, s6);
        BOOST_TEST(std::distance(s.begin(), r6.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r6.end()) == 7);

        // forward_list (forward)
        std::forward_list<char> const s7 = { 'b', 'a', 'r' };
        auto const r7 = str::ifind_first(s, s7);
        BOOST_TEST(std::distance(s.begin(), r7.begin()) == 4);
        BOOST_TEST(std::distance(s.begin(), r7.end()) == 7);
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_first_

} // namespace testing
