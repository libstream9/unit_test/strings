#include <stream9/strings/core/string_traits.hpp>

#include <concepts>
#include <forward_list>
#include <list>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(string_traits_)

    BOOST_AUTO_TEST_SUITE(string_traits_)

        template<typename T, typename CharT, typename IndexT, typename SizeT, typename DiffT>
        void
        test()
        {
            static_assert(std::same_as<typename str::string_traits<T>::char_type, CharT>);
            static_assert(std::same_as<typename str::string_traits<T>::index_type, IndexT>);
            static_assert(std::same_as<typename str::string_traits<T>::size_type, SizeT>);
            static_assert(std::same_as<typename str::string_traits<T>::difference_type, DiffT>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            test<char*, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const*, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const* const, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char*&, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const*&, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const* const&, char, ptrdiff_t, size_t, ptrdiff_t>();
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            test<char[], char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char (&)[], char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const[], char, ptrdiff_t, size_t, ptrdiff_t>();
            test<char const (&)[], char, ptrdiff_t, size_t, ptrdiff_t>();
        }

        BOOST_AUTO_TEST_CASE(range_with_size_type_defined)
        {
            test<std::string, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<std::string const, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<std::string&, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<std::string const&, char, ptrdiff_t, size_t, ptrdiff_t>();
        }

        BOOST_AUTO_TEST_CASE(range_with_size_member_function)
        {
            test<rng::subrange<char*>, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<rng::subrange<char*>, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<rng::subrange<char*>, char, ptrdiff_t, size_t, ptrdiff_t>();
            test<rng::subrange<char*>, char, ptrdiff_t, size_t, ptrdiff_t>();
        }

#if 0 //TODO they can't have size_type
        BOOST_AUTO_TEST_CASE(range_with_bidirectional_iterator)
        {
            std::list<char> l;
            rng::subrange r { l };

            test<decltype(r), char, ptrdiff_t, size_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, size_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, size_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, size_t, ptrdiff_t>();
        }

        BOOST_AUTO_TEST_CASE(range_with_forward_iterator)
        {
            std::forward_list<char> l;
            rng::subrange r { l };

            test<decltype(r), char, ptrdiff_t, ptrdiff_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, ptrdiff_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, ptrdiff_t, ptrdiff_t>();
            test<decltype(r), char, ptrdiff_t, ptrdiff_t, ptrdiff_t>();
        }
#endif

    BOOST_AUTO_TEST_SUITE_END() // string_traits_

    BOOST_AUTO_TEST_SUITE(character_t_)

        BOOST_AUTO_TEST_CASE(char_)
        {
            static_assert(std::same_as<str::character_t<char>, char>);
            static_assert(std::same_as<str::character_t<char&>, char>);
            static_assert(std::same_as<str::character_t<char const>, char>);
            static_assert(std::same_as<str::character_t<char const&>, char>);
        }

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            static_assert(std::same_as<str::character_t<char*>, char>);
            static_assert(std::same_as<str::character_t<char*&>, char>);
            static_assert(std::same_as<str::character_t<char* const>, char>);
            static_assert(std::same_as<str::character_t<char* const&>, char>);

            static_assert(std::same_as<str::character_t<char const*>, char>);
            static_assert(std::same_as<str::character_t<char const*&>, char>);
            static_assert(std::same_as<str::character_t<char const* const>, char>);
            static_assert(std::same_as<str::character_t<char const* const&>, char>);
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            static_assert(std::same_as<str::character_t<char[]>, char>);
            static_assert(std::same_as<str::character_t<char (&)[]>, char>);
            static_assert(std::same_as<str::character_t<char const []>, char>);
            static_assert(std::same_as<str::character_t<char const (&)[]>, char>);
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            static_assert(std::same_as<str::character_t<std::string>, char>);
            static_assert(std::same_as<str::character_t<std::string&>, char>);
            static_assert(std::same_as<str::character_t<std::string const>, char>);
            static_assert(std::same_as<str::character_t<std::string const&>, char>);
        }

        BOOST_AUTO_TEST_CASE(iterator_)
        {
            static_assert(std::same_as<str::character_t<std::string::iterator>, char>);
            static_assert(std::same_as<str::character_t<std::string::iterator&>, char>);
            static_assert(std::same_as<str::character_t<std::string::iterator const>, char>);
            static_assert(std::same_as<str::character_t<std::string::iterator const&>, char>);

            static_assert(std::same_as<str::character_t<std::string::const_iterator>, char>);
            static_assert(std::same_as<str::character_t<std::string::const_iterator&>, char>);
            static_assert(std::same_as<str::character_t<std::string::const_iterator const>, char>);
            static_assert(std::same_as<str::character_t<std::string::const_iterator const&>, char>);
        }

    BOOST_AUTO_TEST_SUITE_END() // character_t_

    BOOST_AUTO_TEST_SUITE(is_same_char_)

        BOOST_AUTO_TEST_CASE(variadic_parameter_)
        {
            static_assert(str::is_same_char_v<char>);
            static_assert(str::is_same_char_v<char, char>);
            static_assert(!str::is_same_char_v<char, signed char>);
            static_assert(str::is_same_char_v<char, char, char>);
            static_assert(!str::is_same_char_v<unsigned char, char, char>);
            static_assert(!str::is_same_char_v<char, char16_t, char>);
            static_assert(!str::is_same_char_v<char, char, char32_t>);
        }

        BOOST_AUTO_TEST_CASE(different_kind_of_types_)
        {
            static_assert(str::is_same_char_v<char, char*>);
            static_assert(str::is_same_char_v<char, char*, char[]>);
            static_assert(str::is_same_char_v<char, char*, char[], std::string>);

            static_assert(!str::is_same_char_v<char, char16_t*>);
            static_assert(!str::is_same_char_v<char, char*, char32_t[]>);
            static_assert(!str::is_same_char_v<char, char*, char[], std::wstring>);
        }

    BOOST_AUTO_TEST_SUITE_END() // is_same_char_

BOOST_AUTO_TEST_SUITE_END() // string_traits_

} // namespace testing
