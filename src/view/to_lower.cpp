#include <stream9/strings/view/to_lower.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace rng = std::ranges;
namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(to_lower_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s1 = "FOO";

        auto r = str::views::to_lower(s1);
        static_assert(rng::view<decltype(r)>);

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string const s1 = "FOO";

        auto r = str::views::to_lower(s1.data());
        static_assert(rng::view<decltype(r)>);

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = "FOO";

        auto r = str::views::to_lower(s1);
        static_assert(rng::view<decltype(r)>);

        BOOST_TEST(r == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // to_lower_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
