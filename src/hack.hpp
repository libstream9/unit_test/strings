#ifndef ALGORITHM_STRING_TEST_HACK_HPP
#define ALGORITHM_STRING_TEST_HACK_HPP

#include <vector>
#include <string_view>
#include <ostream>

namespace std { // hack

inline bool
operator==(std::vector<char> const& lhs, std::string_view const rhs)
{
    return std::string_view(lhs.data(), lhs.size()) == rhs;
}

inline std::ostream&
operator<<(std::ostream& os, std::vector<char> const& v)
{
    return os << std::string_view(v.data(), v.size());
}

} // namespace std

#endif // ALGORITHM_STRING_TEST_HACK_HPP
