#include <stream9/strings/query/find_all.hpp>

#include <stream9/strings/finder/first_finder.hpp>
#include <stream9/strings/finder/last_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>
#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(find_all_)

    BOOST_AUTO_TEST_CASE(character_)
    {
        std::string s = "foo foo foo";

        auto [it, last] = str::find_all(s, 'f');

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(classifier_)
    {
        std::string s = "foo.foo,foo";

        str::is_punct<char> pred;
        auto [it, last] = str::find_all(s, pred);

        BOOST_REQUIRE((it != last));
        auto const& r1 = *it;
        BOOST_REQUIRE(r1.size() == 1);
        BOOST_TEST((&r1[0] == &s[3]));

        ++it;
        BOOST_REQUIRE((it != last));
        auto const& r2 = *it;
        BOOST_REQUIRE(r2.size() == 1);
        BOOST_TEST((&r2[0] == &s[7]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(substring_)
    {
        std::string s = "foo foo foo";

        auto [it, last] = str::find_all(s, "foo");

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(not_found_)
    {
        std::string s = "foo foo foo";

        auto [it, last] = str::find_all(s, "bar");
        BOOST_CHECK(it == last);
    }

    BOOST_AUTO_TEST_CASE(empty_handling_1_)
    {
        std::string s = "foo foo foo";

        auto [it, last] = str::find_all(s, "");
        BOOST_CHECK(it == last);
    }

    BOOST_AUTO_TEST_CASE(empty_handling_2_)
    {
        std::string s = "";

        auto [it, last] = str::find_all(s, "");
        BOOST_CHECK(it == last);
    }

    BOOST_AUTO_TEST_CASE(empty_handling_3_)
    {
        std::string s = "";

        auto [it, last] = str::find_all(s, "foo");
        BOOST_CHECK(it == last);
    }

    BOOST_AUTO_TEST_CASE(first_finder_)
    {
        std::string s = "bar bar bar";
        str::first_finder fn { "bar" };

        auto v = str::find_all(s, std::move(fn));
        static_assert(rng::view<decltype(v)>);

        auto [it, last] = v;

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(last_finder_)
    {
        std::string s = "bar bar bar";
        str::last_finder fn { "bar" };

        auto v = str::find_all(s, std::move(fn));
        static_assert(rng::view<decltype(v)>);

        auto [it, last] = v;

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(regex_)
    {
        std::string s = "foo foo foo";
        std::regex e { "foo" };

        auto [it, last] = str::find_all(s, e);

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // find_all_

BOOST_AUTO_TEST_SUITE(ifind_all_)

    BOOST_AUTO_TEST_CASE(character_)
    {
        std::string s = "FOO FOO FOO";

        auto [it, last] = str::ifind_all(s, 'f');

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 1);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

    BOOST_AUTO_TEST_CASE(substring_)
    {
        std::string s = "FOO foo Foo";

        auto [it, last] = str::ifind_all(s, "foo");

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // ifind_all_

} // namespace testing
