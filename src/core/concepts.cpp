#include <stream9/strings/core/concepts.hpp>

#include <concepts>
#include <iterator>
#include <tuple>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(concepts_)

    using char_types = std::tuple<
        char,
        signed char,
        unsigned char,
        wchar_t,
        //char8_t,
        char16_t,
        char32_t
    >;

    using not_char_types = std::tuple<
        int,
        short,
        long,
        double,
        bool,
        std::string,
        std::pair<bool, bool>
    >;

    BOOST_AUTO_TEST_SUITE(character_)

        BOOST_AUTO_TEST_CASE_TEMPLATE(positive_, C, char_types)
        {
            static_assert(str::character<C>);
            static_assert(str::character<C const>);
            static_assert(str::character<C volatile>);
            static_assert(str::character<C const volatile>);
            static_assert(str::character<C&>);
            static_assert(str::character<C const&>);
            static_assert(str::character<C volatile&>);
            static_assert(str::character<C const volatile&>);
            static_assert(str::character<C&&>);
            static_assert(str::character<C const&&>);
            static_assert(str::character<C volatile&&>);
            static_assert(str::character<C const volatile&&>);
        }

        BOOST_AUTO_TEST_CASE_TEMPLATE(negative_, C, not_char_types)
        {
            static_assert(!str::character<C>);
            static_assert(!str::character<C const>);
            static_assert(!str::character<C volatile>);
            static_assert(!str::character<C const volatile>);
            static_assert(!str::character<C&>);
            static_assert(!str::character<C const&>);
            static_assert(!str::character<C volatile&>);
            static_assert(!str::character<C const volatile&>);
            static_assert(!str::character<C&&>);
            static_assert(!str::character<C const&&>);
            static_assert(!str::character<C volatile&&>);
            static_assert(!str::character<C const volatile&&>);
        }

    BOOST_AUTO_TEST_SUITE_END() // character_

    BOOST_AUTO_TEST_SUITE(character_pointer_)

        BOOST_AUTO_TEST_CASE_TEMPLATE(positive_, C, char_types)
        {
            static_assert(str::character_pointer<C*>);
            static_assert(str::character_pointer<C* const>);
            static_assert(str::character_pointer<C* volatile>);
            static_assert(str::character_pointer<C* const volatile>);
            static_assert(str::character_pointer<C const*>);
            static_assert(str::character_pointer<C const* const>);
            static_assert(str::character_pointer<C const* volatile>);
            static_assert(str::character_pointer<C const* const volatile>);
            static_assert(str::character_pointer<C volatile*>);
            static_assert(str::character_pointer<C volatile* const>);
            static_assert(str::character_pointer<C volatile* volatile>);
            static_assert(str::character_pointer<C volatile* const volatile>);
            static_assert(str::character_pointer<C const volatile*>);
            static_assert(str::character_pointer<C const volatile* const>);
            static_assert(str::character_pointer<C const volatile* volatile>);
            static_assert(str::character_pointer<C const volatile* const volatile>);

            static_assert(str::character_pointer<C*&>);
            static_assert(str::character_pointer<C* const&>);
            static_assert(str::character_pointer<C* volatile&>);
            static_assert(str::character_pointer<C* const volatile&>);
            static_assert(str::character_pointer<C const*&>);
            static_assert(str::character_pointer<C const* const&>);
            static_assert(str::character_pointer<C const* volatile&>);
            static_assert(str::character_pointer<C const* const volatile&>);
            static_assert(str::character_pointer<C volatile*&>);
            static_assert(str::character_pointer<C volatile* const&>);
            static_assert(str::character_pointer<C volatile* volatile&>);
            static_assert(str::character_pointer<C volatile* const volatile&>);
            static_assert(str::character_pointer<C const volatile*&>);
            static_assert(str::character_pointer<C const volatile* const&>);
            static_assert(str::character_pointer<C const volatile* volatile&>);
            static_assert(str::character_pointer<C const volatile* const volatile&>);

            static_assert(str::character_pointer<C*&&>);
            static_assert(str::character_pointer<C* const&&>);
            static_assert(str::character_pointer<C* volatile&&>);
            static_assert(str::character_pointer<C* const volatile&&>);
            static_assert(str::character_pointer<C const*&&>);
            static_assert(str::character_pointer<C const* const&&>);
            static_assert(str::character_pointer<C const* volatile&&>);
            static_assert(str::character_pointer<C const* const volatile&&>);
            static_assert(str::character_pointer<C volatile*&&>);
            static_assert(str::character_pointer<C volatile* const&&>);
            static_assert(str::character_pointer<C volatile* volatile&&>);
            static_assert(str::character_pointer<C volatile* const volatile&&>);
            static_assert(str::character_pointer<C const volatile*&&>);
            static_assert(str::character_pointer<C const volatile* const&&>);
            static_assert(str::character_pointer<C const volatile* volatile&&>);
            static_assert(str::character_pointer<C const volatile* const volatile&&>);
        }

        BOOST_AUTO_TEST_CASE_TEMPLATE(negative1_, C, not_char_types)
        {
            static_assert(!str::character_pointer<C*>);
            static_assert(!str::character_pointer<C* const>);
            static_assert(!str::character_pointer<C* volatile>);
            static_assert(!str::character_pointer<C* const volatile>);
            static_assert(!str::character_pointer<C const*>);
            static_assert(!str::character_pointer<C const* const>);
            static_assert(!str::character_pointer<C const* volatile>);
            static_assert(!str::character_pointer<C const* const volatile>);
            static_assert(!str::character_pointer<C volatile*>);
            static_assert(!str::character_pointer<C volatile* const>);
            static_assert(!str::character_pointer<C volatile* volatile>);
            static_assert(!str::character_pointer<C volatile* const volatile>);
            static_assert(!str::character_pointer<C const volatile*>);
            static_assert(!str::character_pointer<C const volatile* const>);
            static_assert(!str::character_pointer<C const volatile* volatile>);
            static_assert(!str::character_pointer<C const volatile* const volatile>);

            static_assert(!str::character_pointer<C*&>);
            static_assert(!str::character_pointer<C* const&>);
            static_assert(!str::character_pointer<C* volatile&>);
            static_assert(!str::character_pointer<C* const volatile&>);
            static_assert(!str::character_pointer<C const*&>);
            static_assert(!str::character_pointer<C const* const&>);
            static_assert(!str::character_pointer<C const* volatile&>);
            static_assert(!str::character_pointer<C const* const volatile&>);
            static_assert(!str::character_pointer<C volatile*&>);
            static_assert(!str::character_pointer<C volatile* const&>);
            static_assert(!str::character_pointer<C volatile* volatile&>);
            static_assert(!str::character_pointer<C volatile* const volatile&>);
            static_assert(!str::character_pointer<C const volatile*&>);
            static_assert(!str::character_pointer<C const volatile* const&>);
            static_assert(!str::character_pointer<C const volatile* volatile&>);
            static_assert(!str::character_pointer<C const volatile* const volatile&>);

            static_assert(!str::character_pointer<C*&&>);
            static_assert(!str::character_pointer<C* const&&>);
            static_assert(!str::character_pointer<C* volatile&&>);
            static_assert(!str::character_pointer<C* const volatile&&>);
            static_assert(!str::character_pointer<C const*&&>);
            static_assert(!str::character_pointer<C const* const&&>);
            static_assert(!str::character_pointer<C const* volatile&&>);
            static_assert(!str::character_pointer<C const* const volatile&&>);
            static_assert(!str::character_pointer<C volatile*&&>);
            static_assert(!str::character_pointer<C volatile* const&&>);
            static_assert(!str::character_pointer<C volatile* volatile&&>);
            static_assert(!str::character_pointer<C volatile* const volatile&&>);
            static_assert(!str::character_pointer<C const volatile*&&>);
            static_assert(!str::character_pointer<C const volatile* const&&>);
            static_assert(!str::character_pointer<C const volatile* volatile&&>);
            static_assert(!str::character_pointer<C const volatile* const volatile&&>);
        }

        BOOST_AUTO_TEST_CASE_TEMPLATE(negative2_, C, char_types)
        {
            static_assert(!str::character_pointer<C>);
            static_assert(!str::character_pointer<C const>);
            static_assert(!str::character_pointer<C volatile>);
            static_assert(!str::character_pointer<C const volatile>);

            static_assert(!str::character_pointer<C&>);
            static_assert(!str::character_pointer<C const&>);
            static_assert(!str::character_pointer<C volatile&>);
            static_assert(!str::character_pointer<C const volatile&>);

            static_assert(!str::character_pointer<C&&>);
            static_assert(!str::character_pointer<C const&&>);
            static_assert(!str::character_pointer<C volatile&&>);
            static_assert(!str::character_pointer<C const volatile&&>);

            static_assert(!str::character_pointer<C[]>);
            static_assert(!str::character_pointer<C const[]>);
            static_assert(!str::character_pointer<C volatile[]>);
            static_assert(!str::character_pointer<C const volatile[]>);

            static_assert(!str::character_pointer<C (&)[]>);
            static_assert(!str::character_pointer<C const (&)[]>);
            static_assert(!str::character_pointer<C volatile (&)[]>);
            static_assert(!str::character_pointer<C const volatile (&)[]>);

            static_assert(!str::character_pointer<C (&&)[]>);
            static_assert(!str::character_pointer<C const (&&)[]>);
            static_assert(!str::character_pointer<C volatile (&&)[]>);
            static_assert(!str::character_pointer<C const volatile (&&)[]>);
        }

    BOOST_AUTO_TEST_SUITE_END() // character_pointer_

    BOOST_AUTO_TEST_SUITE(character_array_)

        BOOST_AUTO_TEST_CASE_TEMPLATE(positive_, C, char_types)
        {
            static_assert(str::character_array<C[]>);
            static_assert(str::character_array<C const[]>);
            static_assert(str::character_array<C volatile[]>);
            static_assert(str::character_array<C const volatile[]>);

            static_assert(str::character_array<C (&)[]>);
            static_assert(str::character_array<C const (&)[]>);
            static_assert(str::character_array<C volatile (&)[]>);
            static_assert(str::character_array<C const volatile (&)[]>);

            static_assert(str::character_array<C (&&)[]>);
            static_assert(str::character_array<C const (&&)[]>);
            static_assert(str::character_array<C volatile (&&)[]>);
            static_assert(str::character_array<C const volatile (&&)[]>);
        }

        BOOST_AUTO_TEST_CASE_TEMPLATE(negative1_, C, not_char_types)
        {
            static_assert(!str::character_array<C[]>);
            static_assert(!str::character_array<C const[]>);
            static_assert(!str::character_array<C volatile[]>);
            static_assert(!str::character_array<C const volatile[]>);

            static_assert(!str::character_array<C (&)[]>);
            static_assert(!str::character_array<C const (&)[]>);
            static_assert(!str::character_array<C volatile (&)[]>);
            static_assert(!str::character_array<C const volatile (&)[]>);

            static_assert(!str::character_array<C (&&)[]>);
            static_assert(!str::character_array<C const (&&)[]>);
            static_assert(!str::character_array<C volatile (&&)[]>);
            static_assert(!str::character_array<C const volatile (&&)[]>);
        }

        BOOST_AUTO_TEST_CASE_TEMPLATE(negative2_, C, char_types)
        {
            static_assert(!str::character_array<C>);
            static_assert(!str::character_array<C const>);
            static_assert(!str::character_array<C volatile>);
            static_assert(!str::character_array<C const volatile>);

            static_assert(!str::character_array<C&>);
            static_assert(!str::character_array<C const&>);
            static_assert(!str::character_array<C volatile&>);
            static_assert(!str::character_array<C const volatile&>);

            static_assert(!str::character_array<C&&>);
            static_assert(!str::character_array<C const&&>);
            static_assert(!str::character_array<C volatile&&>);
            static_assert(!str::character_array<C const volatile&&>);

            static_assert(!str::character_array<C*>);
            static_assert(!str::character_array<C* const>);
            static_assert(!str::character_array<C* volatile>);
            static_assert(!str::character_array<C* const volatile>);

            static_assert(!str::character_array<C*&>);
            static_assert(!str::character_array<C* const&>);
            static_assert(!str::character_array<C* volatile&>);
            static_assert(!str::character_array<C* const volatile&>);

            static_assert(!str::character_array<C*&&>);
            static_assert(!str::character_array<C* const&&>);
            static_assert(!str::character_array<C* volatile&&>);
            static_assert(!str::character_array<C* const volatile&&>);
        }

    BOOST_AUTO_TEST_SUITE_END() // character_array_

    BOOST_AUTO_TEST_SUITE(character_range_)

        BOOST_AUTO_TEST_CASE(positive_)
        {
            static_assert(str::character_range<std::string>);
            static_assert(str::character_range<std::string const>);
            static_assert(str::character_range<std::string&>);
            static_assert(str::character_range<std::string const&>);
            static_assert(str::character_range<std::string&&>);
            static_assert(str::character_range<std::string const&&>);
            static_assert(str::character_range<std::vector<char>>);
        }

        BOOST_AUTO_TEST_CASE(negative_)
        {
            static_assert(!str::character_range<char[]>);
            static_assert(!str::character_range<char*>);
            static_assert(!str::character_range<int>);
            static_assert(!str::character_range<std::vector<int>>);
            static_assert(!str::character_range<std::vector<char*>>);
            // volatile aren't even Range
            static_assert(!str::character_range<std::string volatile>);
            static_assert(!str::character_range<std::string const volatile>);
            static_assert(!str::character_range<std::string volatile&>);
            static_assert(!str::character_range<std::string const volatile&>);
            static_assert(!str::character_range<std::string volatile&&>);
            static_assert(!str::character_range<std::string const volatile&&>);
        }

    BOOST_AUTO_TEST_SUITE_END() // character_range_

    BOOST_AUTO_TEST_SUITE(string_)

        BOOST_AUTO_TEST_CASE(positive_)
        {
            static_assert(str::string<char*>);
            static_assert(str::string<char[]>);
            static_assert(str::string<std::string>);

            static_assert(str::string<char*&>);
            static_assert(str::string<char (&)[]>);
            static_assert(str::string<std::string&>);
        }

        BOOST_AUTO_TEST_CASE(negative_)
        {
            static_assert(!str::string<char>);
            static_assert(!str::string<char[][3]>);
            static_assert(!str::string<std::basic_string<int>>);
        }

    BOOST_AUTO_TEST_SUITE_END() // String_

    BOOST_AUTO_TEST_SUITE(iterator_t_)

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            static_assert(std::same_as<str::iterator_t<char*>, char*>);
            static_assert(std::same_as<str::iterator_t<char*&>, char*>);
            static_assert(std::same_as<str::iterator_t<char* const&>, char*>);
            static_assert(std::same_as<str::iterator_t<char* const>, char*>);
            static_assert(std::same_as<str::iterator_t<char* const&>, char*>);

            static_assert(std::same_as<str::iterator_t<char const*>, char const*>);
            static_assert(std::same_as<str::iterator_t<char const*&>, char const*>);
            static_assert(std::same_as<str::iterator_t<char const* const&>, char const*>);
            static_assert(std::same_as<str::iterator_t<char const* const>, char const*>);
            static_assert(std::same_as<str::iterator_t<char const* const&>, char const*>);
        }

    BOOST_AUTO_TEST_SUITE_END() // iterator_t_

    BOOST_AUTO_TEST_SUITE(readable_string_)

        BOOST_AUTO_TEST_CASE(negative_)
        {
            static_assert(!str::readable_string<char>);
        }

    BOOST_AUTO_TEST_SUITE_END() // readable_string_

    BOOST_AUTO_TEST_SUITE(writable_string_)

        BOOST_AUTO_TEST_CASE(negative_)
        {
            static_assert(!str::writable_string<char>);
        }

    BOOST_AUTO_TEST_SUITE_END() // writable_string_

    BOOST_AUTO_TEST_CASE(forward_string_)
    {
        static_assert(str::forward_string<std::string>);
        static_assert(str::forward_string<char*>);
        static_assert(str::forward_string<char[4]>);

        static_assert(!str::forward_string<char>);
        static_assert(!str::forward_string<int[4]>);
    }

    BOOST_AUTO_TEST_CASE(bidirectional_string_)
    {
        static_assert(str::bidirectional_string<std::string>);
        static_assert(str::bidirectional_string<char*>);
        static_assert(str::bidirectional_string<char[4]>);

        static_assert(!str::bidirectional_string<char>);
        static_assert(!str::bidirectional_string<int[4]>);
    }

    BOOST_AUTO_TEST_CASE(random_access_string_)
    {
        static_assert(str::random_access_string<std::string>);
        static_assert(str::random_access_string<char*>);
        static_assert(str::random_access_string<char[4]>);

        static_assert(!str::random_access_string<char>);
        static_assert(!str::random_access_string<int[4]>);
    }

    BOOST_AUTO_TEST_CASE(contiguous_string_)
    {
        static_assert(str::contiguous_string<std::string>);
        static_assert(str::contiguous_string<char*>);
        static_assert(str::contiguous_string<char[4]>);

        static_assert(!str::contiguous_string<char>);
        static_assert(!str::contiguous_string<int[4]>);
    }

    template<typename T>
    //concept forward_range = rng::range<T> && rng::ForwardIterator<rng::iterator_t<T>>;
    //concept forward_range = std::forward_iterator<rng::iterator_t<T>>;
    //concept forward_range = requires { typename rng::iterator_t<T>; };
    //concept forward_range = str::readable_string<T> && rng::ForwardIterator<rng::iterator_t<T>>;
    concept forward_range = str::readable_string<T> && std::forward_iterator<str::iterator_t<T>>;

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        static_assert(std::input_iterator<char*>);
        static_assert(std::forward_iterator<char*>);
        static_assert(std::bidirectional_iterator<char*>);
        static_assert(std::random_access_iterator<char*>);
        static_assert(std::contiguous_iterator<char*>);

        //static_assert(rng::iterator_t<char>);
        static_assert(!forward_range<char>); //TODO what the fuck is this?
    }

BOOST_AUTO_TEST_SUITE_END() // concepts_

} // namespace testing
