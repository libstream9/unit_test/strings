#include <stream9/strings/view/find_erase.hpp>

#include <stream9/strings/finder/first_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(find_erase_)

    BOOST_AUTO_TEST_CASE(pointer_with_range_)
    {
        std::string const s = "foobar";

        auto const r = str::views::find_erase(s.data(), "bar");

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(array_with_range_)
    {
        char const s[] = "foobar";

        auto const r = str::views::find_erase(s, "bar");

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_range_)
    {
        std::string const s = "foobar";

        auto const r = str::views::find_erase(s, "bar");

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(pointer_with_finder_)
    {
        std::string s = "foobar";

        str::first_finder finder { "bar" };
        auto const r = str::views::find_erase(s.data(), finder);

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(array_with_finder_)
    {
        char s[] = "foobar";

        str::first_finder finder { "bar" };
        auto const r = str::views::find_erase(s, finder);

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(string_with_finder_)
    {
        std::string s = "foobar";

        str::first_finder finder { "bar" };
        auto const r = str::views::find_erase(s, finder);

        BOOST_TEST(r == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_regex_)
    {
        std::regex const e { "b\\w+" };

        // pointer
        std::string const s1 = "foobar";
        auto const r1 = str::views::find_erase(s1.data(), e);
        BOOST_TEST(r1 == "foo");

        // array
        char s2[] = "foobar";
        auto const r2 = str::views::find_erase(s2, e);
        BOOST_TEST(r2 == "foo");

        // string
        std::string const s3 = "foobar";
        auto const r3 = str::views::find_erase(s3, e);
        BOOST_TEST(r3 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
