#include <stream9/strings/find_suffix.hpp>

#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/string.hpp>

namespace testing {

using stream9::string_view;

BOOST_AUTO_TEST_SUITE(find_suffix_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        string_view s1 = "foo.txt";
        string_view s2 = ".txt";

        auto r1 = str::find_suffix(s1, s2);

        BOOST_TEST(r1 == ".txt");
        BOOST_CHECK(r1.begin() == s1.begin()+3);
        BOOST_CHECK(r1.end() == s1.end());
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        string_view s1 = "foo.zip";
        string_view s2 = ".txt";

        auto r1 = str::find_suffix(s1, s2);

        BOOST_TEST(r1.empty());
        BOOST_CHECK(r1.begin() == s1.end());
        BOOST_CHECK(r1.end() == s1.end());
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        string_view s1 = "";
        string_view s2 = ".txt";

        auto r1 = str::find_suffix(s1, s2);

        BOOST_TEST(r1.empty());
        BOOST_CHECK(r1.begin() == s1.end());
        BOOST_CHECK(r1.end() == s1.end());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        string_view s1 = "foo.txt";
        string_view s2 = "";

        auto r1 = str::find_suffix(s1, s2);

        BOOST_TEST(r1.empty());
        BOOST_CHECK(r1.begin() == s1.end());
        BOOST_CHECK(r1.end() == s1.end());
    }

    BOOST_AUTO_TEST_CASE(empty_3_)
    {
        string_view s1 = "";
        string_view s2 = "";

        auto r1 = str::find_suffix(s1, s2);

        BOOST_TEST(r1.empty());
        BOOST_CHECK(r1.begin() == s1.end());
        BOOST_CHECK(r1.end() == s1.end());
    }

BOOST_AUTO_TEST_SUITE_END() // find_suffix_

} // namespace testing
