#include <stream9/strings/regex/pcre2.hpp>

#include <stream9/strings/accessor//data.hpp>
#include <stream9/strings/finder/regex_finder.hpp>
#include <stream9/strings/query/find_all.hpp>
#include <stream9/strings/query/find_first.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(pcre2_)

    class regex
    {
    public:
        template<str::contiguous_string S>
        regex(S&& s)
        {
            int errnum;
            PCRE2_SIZE erroff;
            auto ptr = str::data(s);

            m_re = ::pcre2_compile(
                reinterpret_cast<PCRE2_SPTR>(ptr),
                PCRE2_ZERO_TERMINATED,
                0, // default options
                &errnum,
                &erroff,
                nullptr // default context
            );
            BOOST_REQUIRE(m_re);
        }

        ~regex() noexcept { ::pcre2_code_free(m_re); }

        auto get() const { return m_re; }

    private:
        pcre2_code* m_re;
    };

    BOOST_AUTO_TEST_CASE(regex_adaptor_)
    {
        std::string s = "foobar";

        regex re { "b" };

        auto const r = str::regex_adaptor<pcre2_code*>::find(s, re.get());

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == s.begin() + 3));
    }

    BOOST_AUTO_TEST_CASE(regex_finder_)
    {
        regex re { "b" };
        str::regex_finder find { re.get() };

        auto const s = "foobar";

        auto const& r = find(s);

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_first_)
    {
        regex re { "b" };

        auto const s = "foobar";

        auto const r = str::find_first(s, re.get());

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_all_)
    {
        std::string s = "foo foo foo";
        regex e { "foo" };

        auto [it, last] = str::find_all(s, e.get());

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // pcre2_

} // namespace testing
