#include <stream9/strings/view/find_replace_all.hpp>

#include <stream9/strings/core/char/comparator.hpp>
#include <stream9/strings/finder/first_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <iterator>
#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(find_replace_all_)

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string const s = "foo bar bar xyzzy";

        auto const v = str::views::find_replace_all(s, "bar", "baz");
        static_assert(rng::view<std::remove_cv_t<decltype(v)>>);

        BOOST_TEST(v == "foo baz baz xyzzy");
    }

    BOOST_AUTO_TEST_CASE(regex_)
    {
        std::string const s = "foo bar bar xyzzy";
        std::regex const e { "b\\w+" };

        auto const v = str::views::find_replace_all(s, e, "baz");
        static_assert(rng::view<std::remove_cv_t<decltype(v)>>);

        BOOST_TEST(v == "foo baz baz xyzzy");
    }

    BOOST_AUTO_TEST_CASE(finder_)
    {
        std::string s = "foo bar bar foo";
        str::first_finder find { "BAR", str::iequal_to<char>() };

        auto const v = str::views::find_replace_all(s, find, "baz");
        static_assert(rng::view<std::remove_cv_t<decltype(v)>>);

        BOOST_TEST(v == "foo baz baz foo");
    }

    template<typename I, typename S>
    bool
    is_sentinel(S, I)
    {
        return std::sentinel_for<S, I>;
    }

    BOOST_AUTO_TEST_CASE(decomposition_)
    {
        std::string const s = "foo bar bar xyzzy";

        auto const v = str::views::find_replace_all(s, "bar", "baz");
        static_assert(rng::view<std::remove_cv_t<decltype(v)>>);
        auto& [begin, end] = v;

        BOOST_TEST((begin == v.begin()));
        BOOST_TEST(is_sentinel(end, begin));
    }

    BOOST_AUTO_TEST_CASE(empty_handling_1_)
    {
        std::string s1 = "foo bar xyzzy";

        auto v1 = str::views::find_replace_all(s1, "bar", "");
        BOOST_TEST(v1 == "foo  xyzzy");

        auto v2 = str::views::find_replace_all(s1, "", "baz");
        BOOST_TEST(v2 == "foo bar xyzzy");

        auto v3 = str::views::find_replace_all(s1, "", "");
        BOOST_TEST(v3 == "foo bar xyzzy");
    }

    BOOST_AUTO_TEST_CASE(empty_handling_2_)
    {
        std::string s1 = "";

        auto v1 = str::views::find_replace_all(s1, "bar", "");
        BOOST_TEST(v1 == "");

        auto v2 = str::views::find_replace_all(s1, "", "baz");
        BOOST_TEST(v2 == "");

        auto v3 = str::views::find_replace_all(s1, "", "");
        BOOST_TEST(v3 == "");
    }

BOOST_AUTO_TEST_SUITE_END() // find_replace_all_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
