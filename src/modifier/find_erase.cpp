#include <stream9/strings/modifier/find_erase.hpp>

#include <stream9/strings/finder/first_finder.hpp>
#include <stream9/strings/regex/std.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(find_erase_)

BOOST_AUTO_TEST_SUITE(find_erase_)

    template<typename S>
    concept invocable =
        requires (S&& s) {
            str::find_erase(s, s);
        };

    template<typename S>
    concept uninvocable = !invocable<S>;

    BOOST_AUTO_TEST_CASE(invocability_)
    {
        static_assert(invocable<std::string>);
        static_assert(invocable<std::vector<char>>);
        static_assert(uninvocable<char[8]>);
        static_assert(uninvocable<char const[8]>);
        static_assert(uninvocable<char*>);
        static_assert(uninvocable<char const*>);
        static_assert(uninvocable<std::string const>);
        static_assert(uninvocable<std::string_view>);
        static_assert(uninvocable<std::vector<char> const>);
    }

    BOOST_AUTO_TEST_CASE(by_range_)
    {
        std::string s1 = "foobar";

        str::find_erase(s1, "bar");

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(by_regex_)
    {
        std::string s1 = "foobar";
        std::regex const e { "b\\w+" };

        str::find_erase(s1, e);

        BOOST_TEST(s1 == "foo");
    }

    BOOST_AUTO_TEST_CASE(by_finder_)
    {
        std::string s1 = "foobar";

        str::find_erase(s1, str::first_finder("bar"));

        BOOST_TEST(s1 == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // find_erase_

BOOST_AUTO_TEST_SUITE_END() // find_erase_

} // namespace testing
