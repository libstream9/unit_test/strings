#include <stream9/strings/view/append.hpp>

#include <string>

#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(append_)

    template<rng::view V>
    constexpr bool is_view = true;

    BOOST_AUTO_TEST_CASE(char_)
    {
        std::string const s1 = "foo";

        auto v = str::views::append(s1, 'X');
        static_assert(is_view<decltype(v)>);

        BOOST_TEST(v == "fooX");
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "foo";

        auto v = str::views::append(s1, "bar");
        static_assert(is_view<decltype(v)>);

        BOOST_TEST(v == "foobar");
    }

BOOST_AUTO_TEST_SUITE_END() // append_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
