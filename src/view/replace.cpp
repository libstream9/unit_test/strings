#include <stream9/strings/view/replace.hpp>

#include <stream9/strings/core/string_range.hpp>
#include <stream9/strings/query/substr.hpp>

#include <iterator>
#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(replace_)

    BOOST_AUTO_TEST_CASE(pointer_by_iterator_)
    {
        std::string s1 = "123456";

        auto const first = str::begin(s1.data()) + 2;
        auto const last = first + 2;

        auto r = str::views::replace(s1.data(), first, last, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        BOOST_TEST(r == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(decomposition_)
    {
        std::string s1 = "123456";

        auto const first = str::begin(s1.data()) + 2;
        auto const last = first + 2;

        auto r = str::views::replace(s1.data(), first, last, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        auto [it1, it2] = r;
        BOOST_TEST((it1 == r.begin()));
        static_assert(std::sentinel_for<decltype(it2), decltype(it1)>);
    }

    BOOST_AUTO_TEST_CASE(array_by_iterator_)
    {
        char s1[] = "123456";

        auto const first = str::begin(s1) + 2;
        auto const last = first + 2;

        auto r = str::views::replace(s1, first, last, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        BOOST_TEST(r == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(string_by_iterator_)
    {
        std::string s1 = "123456";

        auto const first = str::begin(s1) + 2;
        auto const last = first + 2;

        auto r = str::views::replace(s1, first, last, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        BOOST_TEST(r == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(pointer_by_range_)
    {
        std::string s1 = "123456";

        auto const r1 = str::substr(s1.data(), 2, 2);

        auto const r2 = str::views::replace(s1.data(), r1, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r2)>>);

        BOOST_TEST(r2 == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(array_by_range_)
    {
        char s1[] = "123456";

        auto const r1 = str::substr(s1, 2, 2);

        auto const r2 = str::views::replace(s1, r1, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r2)>>);

        BOOST_TEST(r2 == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(string_by_range_)
    {
        std::string s1 = "123456";

        auto const r1 = str::substr(s1, 2, 2);

        auto const r2 = str::views::replace(s1, r1, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r2)>>);

        BOOST_TEST(r2 == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(pointer_by_index_)
    {
        std::string s1 = "123456";

        auto const r = str::views::replace(s1.data(), 2, 2, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        BOOST_TEST(r == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(array_by_index_)
    {
        char s1[] = "123456";

        auto const r = str::views::replace(s1, 2, 2, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r)>>);

        BOOST_TEST(r == "12XX56");
    }

    BOOST_AUTO_TEST_CASE(string_by_index_)
    {
        std::string s1 = "123456";

        auto const r1 = str::views::replace(s1, 2, 2, "XX");
        static_assert(rng::view<std::remove_cv_t<decltype(r1)>>);
        BOOST_TEST(r1 == "12XX56");

        auto const r2 = str::views::replace(s1, 2, 2, "");
        BOOST_TEST(r2 == "1256");
    }

    BOOST_AUTO_TEST_CASE(handling_empty_)
    {
        std::string s1 = "123456";

        auto const r1 = str::views::replace(s1, 0, 0, "XX");
        BOOST_TEST(r1 == "XX123456");

        auto const r2 = str::views::replace(s1, 2, 0, "XX");
        BOOST_TEST(r2 == "12XX3456");

        auto const r3 = str::views::replace(s1, 6, 0, "XX");
        BOOST_TEST(r3 == "123456XX");

        auto const r4 = str::views::replace(s1, 0, 0, "");
        BOOST_TEST(r4 == "123456");

        auto const r5 = str::views::replace(s1, 2, 0, "");
        BOOST_TEST(r5 == "123456");

        auto const r6 = str::views::replace(s1, 6, 0, "");
        BOOST_TEST(r6 == "123456");

        s1 = "";
        auto const r7 = str::views::replace(s1, 0, 0, "XXX");
        BOOST_TEST(r7 == "XXX");

        auto const r8 = str::views::replace(s1, 0, 0, "");
        BOOST_TEST(r8 == "");
    }

BOOST_AUTO_TEST_SUITE_END() // replace_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
