#include <stream9/strings/concat.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/string.hpp>
#include <stream9/cstring_view.hpp>

namespace testing {

using str::concat;

BOOST_AUTO_TEST_SUITE(concat_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        auto s = concat("hello ", "world");
        BOOST_TEST(s == "hello world");
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        using stream9::cstring_view;
        using str::operator+;

        auto s = cstring_view("hello ") + "world";
        BOOST_TEST(s == "hello world");
    }

BOOST_AUTO_TEST_SUITE_END() // concat_

} // namespace testing
