#include <stream9/strings/view/erase.hpp>

#include <ranges>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;
namespace rng = std::ranges;

BOOST_AUTO_TEST_SUITE(view_)

BOOST_AUTO_TEST_SUITE(erase_)

    template<rng::view V> struct check_view {};

    BOOST_AUTO_TEST_CASE(by_index_)
    {
        std::string const s1 = "1234";

        auto v1 = str::views::erase(s1, 0, 2);
        static_assert(rng::view<decltype(v1)>);
        //check_view<decltype(v)> c1;
        BOOST_TEST(v1 == "34");

        auto v2 = str::views::erase(s1, 2);
        static_assert(rng::view<decltype(v2)>);
        BOOST_TEST(v2 == "124");

        auto v3 = str::views::erase(s1, 2, 2);
        static_assert(rng::view<decltype(v3)>);
        BOOST_TEST(v3 == "12");
    }

    BOOST_AUTO_TEST_CASE(by_iterator_)
    {
        std::string const s1 = "1234";

        auto v1 = str::views::erase(s1, s1.begin());
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "234");

        auto v2 = str::views::erase(s1, s1.begin() + 1);
        static_assert(rng::view<decltype(v2)>);
        BOOST_TEST(v2 == "134");

        auto v3 = str::views::erase(s1, s1.end() - 1);
        static_assert(rng::view<decltype(v3)>);
        BOOST_TEST(v3 == "123");
    }

    BOOST_AUTO_TEST_CASE(by_range_)
    {
        std::string const s1 = "1234";

        str::string_range r1 { s1.begin(), s1.begin() + 2 };
        auto v1 = str::views::erase(s1, r1);
        static_assert(rng::view<decltype(v1)>);
        BOOST_TEST(v1 == "34");

        str::string_range r2 { s1.begin() + 1, s1.begin() + 3 };
        auto v2 = str::views::erase(s1, r2);
        static_assert(rng::view<decltype(v2)>);
        BOOST_TEST(v2 == "14");

        str::string_range r3 { s1.begin() + 2, s1.begin() + 4 };
        auto v3 = str::views::erase(s1, r3);
        static_assert(rng::view<decltype(v3)>);
        BOOST_TEST(v3 == "12");
    }

BOOST_AUTO_TEST_SUITE_END() // erase_

BOOST_AUTO_TEST_SUITE_END() // view_

} // namespace testing
