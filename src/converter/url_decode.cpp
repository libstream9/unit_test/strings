#include <stream9/strings/converter/url_decode.hpp>
#include <stream9/strings/converter/url_encode.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(url_decode_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto s = str::url_decode("");

        BOOST_TEST(s.empty());
    }

    BOOST_AUTO_TEST_CASE(without_reserved_character_)
    {
        auto s = str::url_decode("foo");

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_reserved_character_)
    {
        auto s = str::url_decode("%24bar%24");

        BOOST_TEST(s == "$bar$");
    }

    BOOST_AUTO_TEST_CASE(bug_fix_)
    {
        auto s = str::url_decode("%2Cbar%2C");

        BOOST_TEST(s == ",bar,");
    }

BOOST_AUTO_TEST_SUITE_END() // url_decode_

BOOST_AUTO_TEST_SUITE(url_encode_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto s = str::url_encode("");

        BOOST_TEST(s.empty());
    }

    BOOST_AUTO_TEST_CASE(no_reserved_characters_)
    {
        auto s = str::url_encode("foo");

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_reserved_characters_)
    {
        auto s = str::url_encode("$foo%");

        BOOST_TEST(s == "%24foo%25");
    }

BOOST_AUTO_TEST_SUITE_END() // url_encode_

} // namespace testing
