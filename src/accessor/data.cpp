#include <stream9/strings/accessor/data.hpp>

#include <stream9/strings/core/string_range.hpp>

#include <concepts>
#include <forward_list>
#include <list>
#include <string>

#include <boost/test/unit_test.hpp>

namespace str = stream9::strings;

namespace testing {

BOOST_AUTO_TEST_SUITE(primitive_)

    template<typename T>
    inline constexpr bool is_valid = requires (T s) {
        str::data(s);
    };

    BOOST_AUTO_TEST_CASE(type_)
    {
        static_assert(is_valid<char(&)[3]>);
        static_assert(is_valid<char*>);
        static_assert(is_valid<std::string>);
        static_assert(!is_valid<std::list<char>>);
        static_assert(!is_valid<std::forward_list<char>>);
    }

    BOOST_AUTO_TEST_SUITE(data_)

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "ABC";
            char* s = arr;

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(empty_pointer_)
        {
            char arr[] = "";
            char* s = arr;

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* s = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(empty_array_)
        {
            char s[] = "";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            std::string s = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_range_)
        {
            std::string const s = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == &s[0]));
        }

        BOOST_AUTO_TEST_CASE(string_range_)
        {
            str::string_range const s = "ABC";

            auto r = str::data(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == &s[0]));
        }

    BOOST_AUTO_TEST_SUITE_END() // data_

    BOOST_AUTO_TEST_SUITE(cdata_)

        BOOST_AUTO_TEST_CASE(pointer_)
        {
            char arr[] = "ABC";
            char* s = arr;

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(empty_pointer_)
        {
            char arr[] = "";
            char* s = arr;

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_pointer_)
        {
            char const* s = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(array_)
        {
            char s[] = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(empty_array_)
        {
            char s[] = "";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_array_)
        {
            char const s[] = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            std::string s = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == s));
        }

        BOOST_AUTO_TEST_CASE(const_range_)
        {
            std::string const s = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == &s[0]));
        }

        BOOST_AUTO_TEST_CASE(subrange_)
        {
            str::string_range const s = "ABC";

            auto r = str::cdata(s);
            static_assert(std::same_as<decltype(r), char const*>);

            BOOST_TEST((r == &s[0]));
        }

    BOOST_AUTO_TEST_SUITE_END() // cdata_

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace testing
