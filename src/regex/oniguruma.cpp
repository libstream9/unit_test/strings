#include <stream9/strings/regex/oniguruma.hpp>

#include <stream9/strings/accessor/begin.hpp>
#include <stream9/strings/accessor/data.hpp>
#include <stream9/strings/accessor/end.hpp>
#include <stream9/strings/query/find_all.hpp>
#include <stream9/strings/query/find_first.hpp>
#include <stream9/strings/query/size.hpp>

#include <boost/test/unit_test.hpp>

#include <oniguruma.h>

namespace testing {

namespace str = stream9::strings;

BOOST_AUTO_TEST_SUITE(oniguruma_)

    BOOST_AUTO_TEST_CASE(c_api_)
    {
        ::OnigEncoding use_encs[] = { ONIG_ENCODING_ASCII };
        ::onig_initialize(use_encs, sizeof(use_encs) / sizeof(use_encs[0]));

        ::regex_t* reg;
        ::OnigErrorInfo einfo;
        auto const pattern = reinterpret_cast<unsigned char const*>("\\d+");

        int rv = ::onig_new(&reg, pattern, pattern + str::length(pattern),
            ONIG_OPTION_DEFAULT, ONIG_ENCODING_ASCII, ONIG_SYNTAX_DEFAULT, &einfo);
        BOOST_TEST(rv == ONIG_NORMAL);

        auto region = ::onig_region_new();
        auto const s = reinterpret_cast<unsigned char const*>("abc1234xyz");
        auto const first = s;
        auto const last = s + str::length(s);

        rv = ::onig_search(reg, s, last, first, last, region, ONIG_OPTION_NONE);

        BOOST_REQUIRE(rv == 3);
        BOOST_REQUIRE(region->num_regs == 1);
        BOOST_TEST(region->beg[0] == 3);
        BOOST_TEST(region->end[0] == 7);

        ::onig_region_free(region, 1);
        ::onig_free(reg);
    }

    class regex
    {
    public:
        template<str::contiguous_string S>
        regex(S&& pattern)
        {
            auto const begin =
                reinterpret_cast<unsigned char const*>(str::data(pattern));
            auto const end = begin + str::length(pattern);

            ::OnigErrorInfo err;

            int rv = ::onig_new(
                &m_re, begin, end,
                ONIG_OPTION_DEFAULT,
                ONIG_ENCODING_ASCII,
                ONIG_SYNTAX_DEFAULT,
                &err
            );
            BOOST_TEST(rv == ONIG_NORMAL);
        }

        ~regex()
        {
            ::onig_free(m_re);
        }

        auto get() const { return m_re; }

    private:
        ::regex_t* m_re;
    };

    BOOST_AUTO_TEST_CASE(regex_adaptor_)
    {
        regex re { "\\d+" };
        auto const s = "abc1234xyz";

        auto const r = str::regex_adaptor<::regex_t*>::find(s, re.get());

        BOOST_TEST(r == "1234");
    }

    BOOST_AUTO_TEST_CASE(find_first_)
    {
        regex re { "b" };

        auto const s = "foobar";

        auto const r = str::find_first(s, re.get());

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_all_)
    {
        std::string s = "foo foo foo";
        regex e { "foo" };

        auto [it, last] = str::find_all(s, e.get());

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // oniguruma_

} // namespace testing
