#include <stream9/strings/accessor/back.hpp>

#include "../namespace.hpp"

#include <stream9/strings/core/string_range.hpp>

#include <concepts>
#include <forward_list>
#include <list>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(back_)

    template<typename T>
    inline constexpr bool is_valid = requires (T s) {
        { str::back(s) } -> std::same_as<str::reference_t<T>>;
    };

    BOOST_AUTO_TEST_CASE(type_)
    {
        static_assert(is_valid<char(&)[3]>);
        static_assert(is_valid<char*>);
        static_assert(is_valid<std::string>);
        static_assert(is_valid<std::list<char>>);
        static_assert(!is_valid<std::forward_list<char>>);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        std::string s1 = "123";

        BOOST_TEST(str::back(s1.data()) == '3');
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        char s1[] = "123";

        BOOST_TEST(str::back(s1) == '3');
    }

    BOOST_AUTO_TEST_CASE(string_)
    {
        std::string s1 = "123";

        BOOST_TEST(str::back(s1) == '3');
    }

    BOOST_AUTO_TEST_CASE(string_range_)
    {
        str::string_range s1 = "123";

        BOOST_TEST(str::back(s1) == '3');
    }

BOOST_AUTO_TEST_SUITE_END() // back_

} // namespace testing
