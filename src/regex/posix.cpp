#include <stream9/strings/regex/posix.hpp>

#include <stream9/strings/query/find_first.hpp>
#include <stream9/strings/query/find_all.hpp>

#include <boost/test/unit_test.hpp>

#include <regex.h>

namespace testing {

BOOST_AUTO_TEST_SUITE(posix_)

    namespace str = stream9::strings;

    class regex
    {
    public:
        regex(std::string s)
            : m_pattern { std::move(s) }
        {
            auto const rc = ::regcomp(&m_re, m_pattern.c_str(), 0);
            BOOST_REQUIRE(rc == 0);
        }

        ~regex() noexcept
        {
            ::regfree(&m_re);
        }

        regex_t const& get() const { return m_re; }

    private:
        std::string m_pattern;
        regex_t m_re;
    };

    BOOST_AUTO_TEST_CASE(regex_adaptor_)
    {
        regex re { "[[:digit:]][[:digit:]]*" };
        auto const s = "abc1234xyz";

        auto const r = str::regex_adaptor<regex_t>::find(s, re.get());

        BOOST_TEST(r == "1234");
    }

    BOOST_AUTO_TEST_CASE(find_first_)
    {
        regex re { "b" };

        auto const s = "foobar";

        auto const r = str::find_first(s, re.get());

        BOOST_REQUIRE(r.size() == 1);
        BOOST_TEST((r.begin() == &s[3]));
    }

    BOOST_AUTO_TEST_CASE(find_all_)
    {
        std::string s = "foo foo foo";
        regex e { "foo" };

        auto [it, last] = str::find_all(s, e.get());

        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[0]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[4]));

        ++it;
        BOOST_REQUIRE((it != last));
        BOOST_REQUIRE((*it).size() == 3);
        BOOST_TEST(((*it)[0] == s[8]));

        ++it;
        BOOST_REQUIRE((it == last));
    }

BOOST_AUTO_TEST_SUITE_END() // posix_


} // namespace testing
